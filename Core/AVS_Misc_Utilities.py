#!/usr/bin/env python3

import json     #for getConfig
import time    #Used by waitForFile() and waitForFileDeletion()
import os

from pathlib import PurePath


import Core.AVSPrimitives as AVSPrimitives    #Used by parseMasterList
import Core.AVSDBErrors as AVSDBErrors
####File I/O#######################################################################################
def waitForFile(fileName, mode="r", delay=0.2, blocking=True, **kwargs):    #why?
    fileName = str(PurePath(fileName))
    while True:
        #os.access() used as it is faster than a try block, plus it also takes into account permissions
        if not os.access(fileName, os.F_OK):
            if not blocking:
                time.sleep(delay)       #delay still used here so that when caller for some reason calls this for a long enough time, it still won't hog resources, e.g. When the blocking behaviour is actually needed, but the caller wanted to print a debug message for each call so blocking was set to false, but the call is still enclosed in a while True loop
                raise FileNotFoundError()

            time.sleep(delay)
            continue

        #The below try block is a safety net for times when the file is deleted in the small gap of time between the call to os.access and open(), in which the program would crash without this try block
        try:
            ###Only allow these as modes?: "w", "a", "bw", "aw"
            fil = open(fileName, mode, **kwargs)
        except FileNotFoundError:
            if not blocking:
                time.sleep(delay)       #delay still used here so that when caller for some reason calls this for a long enough time, it still won't hog resources, e.g. When the blocking behaviour is actually needed, but the caller wanted to print a debug message for each call so blocking was set to false, but the call is still enclosed in a while True loop
                raise

            time.sleep(delay)
            continue
        else:
            return fil

def waitForFileDeletion(fileName, mode="r", delay=0.2, blocking=True, **kwargs):
    fileName = str(PurePath(fileName))
    while True:
        #No need to use a try-open-catch here as race conditions won't matter, We're only checking for a file's existence
        if not os.access(fileName, os.F_OK):
            return True
        else:
            if not blocking:
                time.sleep(delay)       #delay still used here so that when caller for some reason calls this for a long enough time, it still won't hog resources
                return False

            time.sleep(delay)
            continue
##################################################################################################

####Config Translation helper functions############################################################
def getNonPartyKeys(configFile):
    return ["ui_config", "offices"]

def getPartyKeys(configFile):
    nonPartyKeys = getNonPartyKeys(configFile)
    return [key for key in configFile.keys() if key not in nonPartyKeys]

def getOfficeCandidates(configFile):
    parties = getPartyKeys(configFile)

    officeCandidatesMaster = {}
    for party in parties:
        officeCandidatesMaster[party] = configFile[party]

    return officeCandidatesMaster
""""
    officeCandidatesMaster = {}
    for office in configFile[offices"offices"]:
        officeCandidatesMaster[office] = []

    for party in parties:
        print(party)
        currentParty = configFile[party]
        print(currentParty)
        for office in currentParty.keys():
            officeCandidatesMaster[office].append(currentParty[office])
"""
def parseMasterList(offices, masterList):
    masterCandidateList = {}
    for office in offices:
        masterCandidateList[office] = []
    for partyName in sorted(list(masterList.keys())):       #sorted() here makes the order of parties deterministic
        party = masterList[partyName]
        if partyName == "Independent":      #The party key "Independent" (case sensitive) is reserved for independent candidates and takes the form of a list with offices as keys and has a value of a list of independent candidates
            independent_counter = 1     #Numbers are used in the party names for independent candidates. This helps in cases of disambiguation (e.g. Candidates with the same name running for the same office)
            for office in offices:      #Only include offices that are declared in config
                if office not in party:
                    continue
                for candidate in sorted(party[office]):     #sorted() here so that the independent_counter is deterministic
                    candidate = candidate.strip()       #Remove leading and trailing whitespace
                    if not candidate:       #If the candidate name is empty:
                        print("The list of independently running candidates for the", "'" + office + "'", "office contains the invalid name", "'" + candidate + "'", "Skipping.")
                        continue

                    partyName = "Independent #" +  str(independent_counter)
                    masterCandidateList[office].append(AVSPrimitives.Candidate(candidate, office, partyName))
                    independent_counter += 1

            continue

        print("parseMasterList: parsing party", partyName)
        for office in offices:      #Only include offices that are declared in config
            if office not in party:
                continue

            candidate = party[office]
            candidate = candidate.strip()       #Remove leading and trailing whitespace
            if not candidate:       #If the candidate name is empty:
                print("The candidate running for the office", "'" + office + "'", "for the party", "'" + partyName + "'", "has an invalid or empty name. Skipping.")
                continue

            candidateClass  = AVSPrimitives.Candidate(candidate, office, partyName)
            masterCandidateList[office].append(candidateClass)

    #Check for candidates with the same name. If any are found, their identifier is set to Party.Office
    #This mainly for naming conflicts in candidate pictures, as well as disambiguation just in case
    candidateNames = {}
    for office in masterCandidateList:
        for index in range(len(masterCandidateList[office])):
            candidate = masterCandidateList[office][index]
            name = candidate.name.lower()   #Make case insensitive as filenames in Windows are case insensitive
            if name not in candidateNames:
                candidateNames[name] = []

            candidateNames[name].append((office, index))

    for name in candidateNames:
        candidates = candidateNames[name]
        if len(candidates) < 2:     #Name is unique. Do not append identifier
            continue

        for office, index in candidates:
            candidate = masterCandidateList[office][index]
            candidate.identifier = candidate.party +  "." + candidate.office

    return masterCandidateList

def verifyConfig(config):
    required_keys = {
    "ui_config": [
        "assetsFolder",
        "pictureFormat",
        "officeBoxSpacing",
        "maxOfficesPerColumn",
        "includeSummary",
        "legacyMode"
        ],

    "db_config": [
        "votes_db",
        "voter_db",
        "master_db"
        ],

    "server_config": [
        "pre_election",
        "password_hash",
        "serverFolder",
        "maxPCNum"
        ],

    "offices" : []
    }

    missingKeys = []
    for key in required_keys:
        if key not in config:
            missingKeys.append(key)
            continue
        for subkey in required_keys[key]:
            if subkey not in config[key]:
                missingKeys.append(key + ":" + subkey)

    return missingKeys

def getConfig(configFile):
    configFile = str(PurePath(configFile))
    print("Loading config ({})".format(configFile))
    errorMessage = None
    try:
        config = json.load(open(configFile))
    except (FileNotFoundError, PermissionError, ValueError) as error:    #ValueError == json.JSONDecodeError
        errorMessage = AVSDBErrors.AVSConfigError.message + " (" + str(error) + ")"
    else:
        missingKeys = verifyConfig(config)
        if len(missingKeys) > 0:
            errorMessage = "The following keys are missing from the supplied config(" + configFile + "):\n" + "\n".join(missingKeys)

    if errorMessage is not None:
        error = AVSDBErrors.AVSConfigError()
        error.message = errorMessage
        raise error

    return config

def getCandidates(candidatesFile, offices):
    candidatesFile = str(PurePath(candidatesFile))
    print("Loading candidates ({})".format(candidatesFile))
    errorMessage = None
    try:
        candidates = json.load(open(candidatesFile))
    except (FileNotFoundError, PermissionError, ValueError) as error:    #ValueError == json.JSONDecodeError
        errorMessage = AVSDBErrors.AVSCandidatesFileError.message + " (" + str(error) + ")"
    else:
        invalidParties = []
        for party in candidates:
            if not isinstance(candidates[party], dict):
                invalidParties.append(party)

        if len(invalidParties) > 0:
            errorMessage = "The following keys in the candidates file(" + candidatesFile + ") are invalid:\n" + "\n".join(invalidParties)

    if errorMessage is not None:
        error = AVSDBErrors.AVSCandidatesFileError()
        error.message = errorMessage
        raise error

    for key in candidates:
        for office in candidates[key]:
            if office not in offices:
                print("WARNING:", office, "is not in the offices declared in the config. Skipping.")
                continue

    return candidates


##################################################################################################

def getCandidateImageFilename(candidate, pictureFormat=""):
    baseFilename = candidate.name
    identifier = ""
    #when identifier is set by an external handler, it means that this candidate is similar to another
    #  in a way that demands disambiguation between them
    if candidate.identifier:
        #If the candidate is running independently, use the party name instead
        identifier = candidate.party if candidate.isIndependent() else candidate.identifier

    baseFilename += "[" + identifier + "]" if identifier else ""
    #Crudely encode the filename such that invalid characters are eliminated
    substituteChars = ""
    filename = ""
    for char in baseFilename:
        sub = getCandidateImageFilename.invalidFilenameChars.find(char)
        if sub != -1:     #char is in invalidFilenameChars
            filename += "_"
            substituteChars += str(sub)
        else:
            filename += char

    substituteChars = "(SUBCHARS" + substituteChars + ")" if substituteChars else ""

    return filename + substituteChars + pictureFormat
getCandidateImageFilename.invalidFilenameChars = r'"*/:<>?\|'

def centerText(*args):
    max_len = max(len(arg) for arg in args)

    output = []
    for arg in args:
        output.append(arg.center(max_len))

    return tuple(output)


