#!/usr/bin/python3
#TODO
#input sanitizer function, which filters non printable characters, whitespace, and names not in a database(optionally)
#Move Non DB classes to separater class

import sqlite3
import datetime
import getpass
import hashlib
import json
from pathlib import PurePath

from Core import AVSPrimitives
from Core import AVSDBErrors

FileNotFoundError = IOError

class AVSDB(object):
    def __init__(self, dbFile):
        self.dbFile = str(PurePath(dbFile)) if dbFile is not None else None

    def connect(self, *args, **kwargs):
        con = None
        try:
            con = AVSDB.Connection(self.dbFile, *args, **kwargs)
        except sqlite3.Error as e:
            error = AVSDBErrors.DatabaseUnavailableError()
            error.message = '(' + self.dbFile + ')' + error.message + "(" + str(e) + ")"
            raise error

        return con

    ####Wrapper classes so that sqlite3.Error is thrown as AVSDBErrors.DatabaseUnavailableError
    #only the methods used in AVSLogic and AVSDB derivatives are wrapped
    #This is messy and will slow down each query as the wrapper objects for Connection and Cursor will have to
    #   be made each time, but this requires the least changes for now.
    #   TODO:A proper solution should be implemented later
    class Connection(object):
        def __init__(self, dbFile, *args, **kwargs):
            self.dbFile = dbFile
            self.con = sqlite3.connect(self.dbFile, *args, **kwargs)

        def cursor(self, *args, **kwargs):
            result = None
            try:
                result = self.con.cursor(*args, **kwargs)
            except sqlite3.Error as e:
                error = AVSDBErrors.DatabaseUnavailableError()
                error.message = '(' + self.dbFile + ')' + error.message + "(" + str(e) + ")"
                raise error

            return AVSDB.Cursor(self.dbFile, result)

        def execute(self, *args, **kwargs):
            result = None
            try:
                result = self.con.execute(*args, **kwargs)
            except sqlite3.Error as e:
                error = AVSDBErrors.DatabaseUnavailableError()
                error.message = '(' + self.dbFile + ')' + error.message + "(" + str(e) + ")"
                raise error

            return AVSDB.Cursor(self.dbFile, result)

        def commit(self, *args, **kwargs):
            result = None
            try:
                result = self.con.commit(*args, **kwargs)
            except sqlite3.Error as e:
                error = AVSDBErrors.DatabaseUnavailableError()
                error.message = '(' + self.dbFile + ')' + error.message + "(" + str(e) + ")"
                raise error

            return result

        def close(self, *args, **kwargs):
            result = None
            try:
                result = self.con.close(*args, **kwargs)
            except sqlite3.Error as e:
                error = AVSDBErrors.DatabaseUnavailableError()
                error.message = '(' + self.dbFile + ')' + error.message + "(" + str(e) + ")"
                raise error

            return result

    class Cursor(object):
        def __init__(self, dbFile, cur):
            self.dbFile = dbFile
            self.cur = cur

        def execute(self, *args, **kwargs):
            result = None
            try:
                result = self.cur.execute(*args, **kwargs)
            except sqlite3.Error as e:
                error = AVSDBErrors.DatabaseUnavailableError()
                error.message = '(' + self.dbFile + ')' + error.message + "(" + str(e) + ")"
                raise error

            self.cur = result
            return self

        def fetchone(self, *args, **kwargs):
            result = None
            try:
                result = self.cur.fetchone(*args, **kwargs)
            except sqlite3.Error as e:
                error = AVSDBErrors.DatabaseUnavailableError()
                error.message = '(' + self.dbFile + ')' + error.message + "(" + str(e) + ")"
                raise error

            return result

        def fetchall(self, *args, **kwargs):
            result = None
            try:
                result = self.cur.fetchall(*args, **kwargs)
            except sqlite3.Error as e:
                error = AVSDBErrors.DatabaseUnavailableError()
                error.message = '(' + self.dbFile + ')' + error.message + "(" + str(e) + ")"
                raise error

            return result
    ####

class VoterDB(AVSDB):
    def __init__(self, dbFile):     #Handle: What if the voterDB suddenly became invalid?
        AVSDB.__init__(self, dbFile)
        try:
            open(self.dbFile).close()
        except FileNotFoundError:
            print("VoterDB: Initializing the Database ({}).".format(self.dbFile))
            self.clear()

        if not self.isValidDB():
            print("VoterDB: The Database supplied ({}) is invalid.".format(self.dbFile))
        else:
            print("VoterDB: Database ({}) is valid.".format(self.dbFile))

    def clear(self):
        open(self.dbFile, "w").close()
        con = self.connect()
        con.execute("CREATE TABLE Voters (StudentName VARCHAR(255), GradeLevel VARCHAR(2), Section VARCHAR(20), InTime VARCHAR(20), OutTime VARCHAR(20))")
        con.commit()
        con.close()

    def isValidDB(self):
        con = self.connect()
        cur = con.cursor()

        try:
            cur.execute("SELECT StudentName, GradeLevel, Section, Intime, OutTime FROM Voters")
        except AVSDBErrors.AVSDBError:
            return False
        finally:
            con.close()

        return True

    def checkFields(self, voter, *fields, maxNull=True):
        con = self.connect()
        cur = con.cursor()
        arguments = (voter.name, voter.grade, voter.section)
        ####Sanitize field before query
        fields_formatted = ", ".join(fields)
        cur.execute("SELECT " + fields_formatted + " FROM Voters WHERE StudentName = ? AND GradeLevel = ? AND Section = ?", arguments)
        rows = cur.fetchall()
        con.close()

        len_rows = len(rows)
        if len_rows == 0:
            return (None,) * len(fields) + (0,)

        rowIndex = 0
        bestMatch = 0 if maxNull else len(fields)
        matchFunc = (lambda num: num >= bestMatch) if maxNull else (lambda num: num <= bestMatch)
        #For multiple matches (same name, grade, section), prioritize the earliest record with the most NULL
        for i in range(len_rows-1, -1, -1):
            numNull = rows[i].count(None)
            if matchFunc(numNull):
                rowIndex = i
                bestMatch = numNull

        return rows[rowIndex] + (len_rows,)

    def registerVoter(self, voter, masterDB=None):
        inTime, outTime, numRecorded = self.checkFields(voter, "InTime", "OutTime", maxNull=True)
        slots = masterDB.checkVoter(voter.name, voter.grade, voter.section) if masterDB is not None else numRecorded + 1
        if slots == 0:
            raise AVSDBErrors.VoterNotInDatabaseError
        elif numRecorded > slots:
            raise AVSDBErrors.HasAlreadyVotedError  #TODO: Change into a more appropriate exception

        if numRecorded == slots:
            if outTime:
                raise AVSDBErrors.HasAlreadyVotedError
            elif inTime:
                raise AVSDBErrors.HasAlreadyRegisteredError

        con = self.connect()
        dateAndTime = datetime.datetime.now()
        dateAndTime = str(dateAndTime.date()) + " " + str(dateAndTime.time())
        arguments = (voter.name, voter.grade, voter.section, dateAndTime, None)
        con.execute("INSERT INTO Voters (StudentName, GradeLevel, Section, InTime, OutTime) VALUES (?, ?, ?, ?, ?)", arguments)
        con.commit()
        con.close()

        return True

    def registerOutTime(self, voter, masterDB=None):
        inTime, outTime, numRecorded = self.checkFields(voter, "InTime", "OutTime", maxNull=True)
        slots = masterDB.checkVoter(voter.name, voter.grade, voter.section) if masterDB is not None else numRecorded + 1
        if slots == 0:
            raise AVSDBErrors.VoterNotInDatabaseError
        elif numRecorded > slots:
            raise AVSDBErrors.HasAlreadyVotedError  #TODO: Change into a more appropriate exception

        if not inTime:
            raise AVSDBErrors.VoterNotRegisteredError
        elif outTime and (numRecorded == slots):
            raise AVSDBErrors.HasAlreadyVotedError

        con = self.connect()
        dateAndTime = datetime.datetime.now()
        dateAndTime = str(dateAndTime.date()) + " " + str(dateAndTime.time())
        arguments = (dateAndTime, voter.name, voter.grade, voter.section, inTime)
        #print("VoterDB: Recording the following data to the DB:", arguments, dateAndTime)
        con.execute("UPDATE Voters SET OutTime = ? WHERE StudentName = ? AND GradeLevel = ? AND Section = ? AND InTime = ?", arguments)
        con.commit()
        con.close()

class VotesDB(AVSDB):
    def __init__(self, dbFile, masterList=None, clear=False):
        AVSDB.__init__(self, dbFile)
        try:
            open(self.dbFile).close()
        except FileNotFoundError:
            print("VotesDB: Initializing the Database ({}).".format(self.dbFile))
            clear = True

        if self.isValidDB():
            print("VotesDB: Database ({}) is valid.".format(self.dbFile))
        else:
            print("VotesDB: The Database supplied ({}) is invalid. Initializing DB.".format(self.dbFile))

        if clear:
            self.clear()
            if masterList is not None:
                self.initialize(masterList)

    def clear(self):
        open(self.dbFile, "w").close()
        con = self.connect()
        con.execute("CREATE TABLE Candidates (CandidateName VARCHAR(255), Office VARCHAR(25), Party VARCHAR(255), Votes INT)")
        con.commit()
        con.close()

    def checkField(self, field, candidate):
        con = self.connect()
        cur = con.cursor()
        arguments = (candidate.name, candidate.office, candidate.party)
        ####Sanitize field before query
        cur.execute("SELECT " + field + " FROM Candidates WHERE CandidateName = ? AND Office = ? AND Party = ?", arguments)
        row = cur.fetchone()
        con.close()

        return row

    def isValidDB(self):
        con = self.connect()
        cur = con.cursor()

        try:
            cur.execute("SELECT CandidateName, Office, Party, Votes FROM Candidates")
        except AVSDBErrors.AVSDBError:       #Generic catch; was just sqlite3.OperationalError before
            return False
        finally:
            con.close()

        return True

    def initialize(self, masterList):       #Remove prints here
        con = self.connect()
        cur = con.cursor()

        print("VotesDB: Initializing DB ({}) with the following Candidates:".format(self.dbFile))
        for officeName in masterList.keys():
            office = masterList[officeName]
            for candidate in office:
                candidateDataInDB = self.checkField("*", candidate)
                if candidateDataInDB not in (None, (None,)):
                    print("VotesDB: Candidate ({}) already in the database. Skipping".format(candidate))
                    continue

                print(" ", candidate)
                args = (candidate.name, candidate.office, candidate.party, 0)
                cur.execute("INSERT INTO Candidates (CandidateName, Office, Party, Votes) VALUES (?, ?, ?, ?)", args)

        print()
        con.commit()
        con.close()

    def incrementVote(self, candidateList):
        if not candidateList:   #No candidates voted
            return

        con = self.connect()
        cur = con.cursor()

        matches = []
        args = []
        for candidate in candidateList:
            args.extend([candidate.name, candidate.office, candidate.party])
            matches.append("(CandidateName = ? AND Office = ? AND Party = ?)")

        matches = " WHERE " + " OR ".join(matches)
        cur.execute("UPDATE Candidates SET Votes = Votes + 1" + matches, args)

        con.commit()
        con.close()

class MasterDB(AVSDB):
    #Manages a Table of the master list of valid voters with the following format(subject to change):
    #<Table> Grade Level 1
    #   <cells> Name Section
    #<Table> Grade Level 2
    #   <cells> Name Section
    #etc.
    def __init__(self, dbFile=None, masterList=None):
        AVSDB.__init__(self, dbFile)

        #try:
        #    open(self.dbFile, "r")
        #except (FileNotFoundError, IOError):
        #    self.dbFile = None

        if self.dbFile is None:
            print("No db file supplied; assuming masterDB is not needed")
            return
        elif masterList is None:
            print("MasterDB: Database ({}) should be already Valid.".format(self.dbFile))     #ungraceful and not robust; should be just placeholder
            return      #maybe check db for validity?
        elif isinstance(masterList, str):
            masterList = json.load(open(PurePath(masterList), "r"))     ####Check if file exists first
        elif isinstance(masterList, dict):
            pass
        else:
            return      #raise TypeError?

        self.initialize(masterList)
        print("MasterDB: Initialized Database", dbFile)

    def connect(self):
        if self.dbFile is None:
            return sqlite3.connect(":memory:")

        return AVSDB.connect(self)

    def isValidDB(self):
        if self.dbFile is None:
            return True

        con = self.connect()
        cur = con.cursor()

        for gradeLevel in con.execute("SELECT name FROM sqlite_master WHERE type='table'").fetchall():
            gradeLevel = gradeLevel[0]
            if not (gradeLevel.startswith("Grade ") and gradeLevel[6:].isnumeric()):
                print("GRADE OR SECTION", gradeLevel.startswith("Grade "), gradeLevel[6:].isnumeric())
                con.close()
                return False

            try:
                cur.execute("SELECT StudentName, Section FROM '" + gradeLevel + "'")
            except AVSDBErrors.AVSDBError as e:
                print(e)
                con.close()
                return False

        con.close()
        return True

    def initialize(self, masterList):
        #MasterList here should be a json file (subject to change) with the following format:
        #{"Grade Level 1": {
        #       "Section1": <Here goes the names, Sur, First, MI>
        #       "Section2": <Here goes the names, Sur, First, MI>
        #   },
        #"Grade Level 2": { etc
        open(self.dbFile, "w").close()
        con = self.connect()
        cur = con.cursor()
        for gradeLevel in masterList.keys():     #Use sanitize input from dat avsdb base rework first?
            if not gradeLevel.strip():      #If gradeLevel is empty or is just whitespace; Not restricting to just ints(for now) just in case (e.g. Twelve, XII, etc.)
                continue
            cur.execute("CREATE TABLE 'Grade {}' (StudentName VARCHAR(255), Section VARCHAR(20))".format(gradeLevel)) #handle if table already exists
            for section in masterList[gradeLevel].keys():
                if not section.strip():     #If name is empty or is just whitespace
                    continue
                for student in masterList[gradeLevel][section]:
                    if not student.strip():     #If name is empty or is just whitespace
                        continue

                    args = (student, section)
                    cur.execute("INSERT INTO 'Grade {}' (StudentName, Section) VALUES (?, ?)".format(gradeLevel), args)
        con.commit()
        con.close()

    def checkVoter(self, name, grade, section):    #needs more concise name, + take in voter or separate name, grade, section?
        #name = voter.name
        #grade = voter.grade
        #section = voter.section

        if self.dbFile is None:
            return 2000     #Reasonably large number to count as "infinite" considering the school population

        con = self.connect()   #No need to create cur?
        try:
            args = (name, section)
            numVoters = con.execute("SELECT COUNT(*) FROM " + ('"Grade ' + str(grade + '"')) + "WHERE StudentName = ? AND Section = ?", args).fetchone()[0]
        except AVSDBErrors.AVSDBError:
            print("MasterDB: Invalid grade Level Supplied", grade)    #debug for now
            #raise InvalidGradelevelError?
            return 0
        else:
            return numVoters

################################These are only used by AVSComboBoxText for now
    def getStudentInfo(self, name):
        if self.dbFile is None:
            return  (None, None)

        con = self.connect()
        cur = con.cursor()
        gradeLevels = cur.execute("SELECT name FROM sqlite_master WHERE type = 'table'").fetchall()
        gradeLevels = [grade[0] for grade in gradeLevels]

        for grade in gradeLevels:       #does not handle duplicate names for now
            section = cur.execute("SELECT Section FROM '{}' WHERE StudentName = ?".format(grade), (name, )).fetchone()
            if section not in (None, (None, )):
                if grade.startswith("Grade "):
                    grade = grade[6:]   #Remove "Grade " prefix

                return (grade, section[0])

        return (None, None)

    def getAllNames(self):      #hacky and dirty
        if self.dbFile is None:
            return []

        con = self.connect()
        cur = con.cursor()
        gradeLevels = cur.execute("SELECT name FROM sqlite_master WHERE type = 'table'").fetchall()
        gradeLevels = [grade[0] for grade in gradeLevels]

        names = []
        for grade in gradeLevels:
            sections_raw = cur.execute("SELECT Section FROM  '{}' ".format(grade)).fetchall()
            sections = []
            for section in sections_raw:
                if section[0] not in sections:
                    sections.append(section[0])

            for section in sections:
                names_local = cur.execute("SELECT StudentName FROM '{}' WHERE Section = ?".format(grade), (section, )).fetchall()
                names_local = [name[0] for name in names_local if name[0].strip()]
                #print(section, names_local)
                names.extend(names_local)

        return names

    def getAllGrades(self):
        if self.dbFile is None:
            return []

        con = self.connect()
        cur = con.cursor()
        tableNames = cur.execute("SELECT name FROM sqlite_master WHERE type = 'table'").fetchall()
        gradeLevels = []
        for table in tableNames:
            grade = table[0][6:].strip()    #Remove preceding "Grade " and any extra whitespace
            if grade:
                gradeLevels.append(grade)

        return gradeLevels

    def getAllSections(self):
        if self.dbFile is None:
            return []

        con = self.connect()
        cur = con.cursor()
        gradeLevels = cur.execute("SELECT name FROM sqlite_master WHERE type = 'table'").fetchall()
        gradeLevels = [grade[0] for grade in gradeLevels]

        sections = []
        for grade in gradeLevels:
            sections_raw = cur.execute("SELECT Section FROM  '{}' ".format(grade)).fetchall()
            for section in sections_raw:
                if section[0] not in sections and section[0].strip():
                    sections.append(section[0])

        return sections

    def __addVoter(self):       #Password protected; should be for edge cases only
        pass

class AVSExceptionHandler(object):
    #Move these to an enum
    POLICY_WARN = "==WARN=="        #may be uneeded/ there is no apparent way to make this work
    POLICY_BLOCK = "==BLOCK=="
    POLICY_IGNORE = "==IGNORE=="
    POLICY_ASK = "==ASK=="

    def __init__(self, policies=None, default_policy=POLICY_BLOCK):
        print("AVSExceptionHandler: Default Policy:", default_policy)
        if policies is None:
            policies = {}

        self.exception_stack = []
        self.default_policy = default_policy
        self.policies = policies

        ####For UI hook functions
        self.on_append_callbacks = []
        self.on_block_callbacks = []
        self.on_allow_callbacks = []

    def query_policy(self, exception):
        if exception in self.policies.keys():
            return self.policies[exception]
        else:
            return self.default_policy

    def add_to_queue(self, exception, voter, data=None):
        data = {"Voter": voter, "Exception": type(exception), "Data": data}     #type(exception) or just exception?; make a dedicated aehError class?

        self.exception_stack.append(data)
        for callback in self.on_append_callbacks:
            callback(exception)

    def allow_exception(self, exception, voter):        #TODO: Maybe create an AVSExceptionHandled object instead of returning either the data or False
        for item in self.exception_stack[::-1]:
            if item["Voter"] == voter and item["Exception"] == type(exception):
                for callback in self.on_allow_callbacks:
                    callback(exception)
                self.exception_stack.remove(item)

                return item["Data"]

        print("AVSExceptionHandler: Debug: Error! Exception not found in the stack(allow_main)")
        return False

    def block_exception(self, exception, voter):        #almost 1:1 copy of .allow_exception()
        for item in self.exception_stack[::-1]:
            if item["Voter"] == voter and item["Exception"] == type(exception):
                for callback in self.on_block_callbacks:
                    callback(exception)
                self.exception_stack.remove(item)

                return False

        print("AVSExceptionHandler: Debug: Error! Exception not found in the stack(block)")
        return False

    def register_callback(self, callback, signal="append"):
        if callback is None:
            return

        if signal == "append":
            self.on_append_callbacks.append(callback)
        elif signal == "allow":
            self.on_allow_callbacks.append(callback)
        elif signal == "block":
            self.on_block_callbacks.append(callback)

class AVSLogic(object):
    def __init__(self, votesDBFile, voterDBFile, offices, masterCandidateList, AVSexception_handler, password_hash=None, masterDBFile=None, reset=False, pre_election=True):
        self.masterCandidateList = masterCandidateList
        self.AVSexception_handler = AVSexception_handler
        self.votesDB = VotesDB(votesDBFile, masterCandidateList)
        self.voterDB = VoterDB(voterDBFile)
        self.masterDB = MasterDB(masterDBFile)
        self.offices = offices
        self.grade_offset = int(pre_election)    #If it is a pre election, then officers for the next school year are to be voted for, and Grade Representatives have to be one grade higher
        self.password_hash =  hashlib.sha256("E13C7".encode("utf8") + "".encode("utf8")).hexdigest() if password_hash is None else password_hash

        if reset:
            self.clear(masterCandidateList)

        if not self.voterDB.isValidDB():
            error = AVSDBErrors.InvalidVoterDBError()
            error.message = "(" + self.voterDB.dbFile + ")" + error.message
            raise error

        if not self.votesDB.isValidDB():
            error = AVSDBErrors.InvalidVotesDBError()
            error.message = "(" + self.votesDB.dbFile + ")" + error.message
            raise error

        if not self.masterDB.isValidDB():
            error = AVSDBErrors.InvalidMasterDBError()
            error.message = "(" + self.masterDB.dbFile + ")" + error.message
            raise error

    def startElection(self):
        pass        #overload in other cases to make custom setup

    def stopElection(self):
        pass    #overload in other cases to customize the election end

    def clear(self, masterCandidateList):   #Refactor
            self.votesDB.clear()
            self.votersDB.clear()
            self.votesDB(masterCandidateList)

    def newVoterBallot(self, name, grade, section):
        voter = AVSPrimitives.Voter(name, grade, section)
        #TODO: Process offices first (Handle Grade %grade Representative); maybe create a decicated offices class?
        offices = []        #Condense into one line using filter? + any?
        for office in self.offices:     #hardcoded for now
            if office.startswith("Grade") and grade.isnumeric():
                if str(int(grade) + self.grade_offset) in office:
                    offices.append(office)
            else:
                offices.append(office)

        ballot = AVSPrimitives.Ballot(voter, offices, self.masterCandidateList)
        try:
            self.voterDB.registerVoter(voter, self.masterDB)
        except AVSDBErrors.AVSDBError as error:
            self.handleError(error, voter, ballot)

        return ballot

    def processBallot(self, ballot):
        if ballot is None:
            print("AVSLogic: Invalid ballot. Either voter has already voted, or ballot was Corrupted.")
            return      #raise AVSDB.InvalidBallot

        voter = ballot.voter
        offices = ballot.offices
        if voter is None:
            print("AVSLogic: Invalid Ballot (No voter)")
            self.handleError(AVSDBErrors.NoVoterInfo, voter, ballot)

        votedCandidates = []
        for office in offices:
            officeClass = None
            try:
                officeClass = getattr(ballot, office)
            except AttributeError:
                print("AVSLogic: ", office, "not in ballot")      #raise
                continue

            voted = officeClass.votedCandidate
            if voted is None:
                continue
            else:
                votedCandidates.append(voted)

        #Check first if all candidates are in the database.
        for candidate in votedCandidates:
            if self.votesDB.checkField("Votes", candidate) is None:     #Either the VotesDB is malformed, outdated, or a malicious party is somehow trying to cast an invalid vote
                self.handleError(AVSDBErrors.CandidateNotInDatabaseError, voter, candidate)

        try:
            self.voterDB.registerOutTime(voter, self.masterDB)
        except AVSDBErrors.AVSDBError as error:
            self.handleError(error, voter, ballot)
        else:
            self.votesDB.incrementVote(votedCandidates)

    def handleError(self, error, voter, data=None):
        policy =  self.AVSexception_handler.query_policy(type(error))
        error.policy = policy
        error.voter = voter
        if policy == self.AVSexception_handler.POLICY_BLOCK:
            raise error
        elif policy == self.AVSexception_handler.POLICY_IGNORE:
            return
        elif policy == self.AVSexception_handler.POLICY_ASK:
            self.AVSexception_handler.add_to_queue(error, voter, data)
            error.ballot = data     #stop gap for now; migrate soon
            raise error
        else:
            raise error

    def ask_error_handling(self, error, voter, ui_callback=None):
        #handle if error is in stack first?
        if ui_callback is None:
            ui_callback = self.mainServerQueryTUI
            print("NON: ui_callback not specified")
            #return False

        data = None
        password = ui_callback(error, voter)
        if hashlib.sha256("E13C7".encode("utf8") + password.encode("utf8")).hexdigest() == self.password_hash:
            data = self.AVSexception_handler.allow_exception(error, voter)
        else:
            self.AVSexception_handler.block_exception(error, voter)
        return data

    def mainServerQueryTUI(self, error, voter):
        password = getpass.getpass("Voter {} is asking permission to re-entry for the Error: {}\nPassword: ".format(voter, error.message))
        return password


if __name__ == "__main__":
    from Core.AVS_Misc_Utilities import parseMasterList
    masterList = {
        "Sum Party": {
            "President": "Wyan Waminal",
            "Vice President": "Sumwam else",
            "Treasurer": "Austin Dewa Kwuz"
            },

        "Real Party": {
            "President": "Ryan Jaminal",
            "Vice President": "Vincent Dela Cruz",
            "Treasurer": "Austin Dela Cruz"
            },

        "Fake Party": {
            "President": "Ryan Jaminal (Fake)",
            "Treasurer": "Aso"
            }
        }

    AVSexception_handler = AVSExceptionHandler()
    offices = ["President", "Vice President", "Treasurer"]
    masterCandidateList = parseMasterList(offices, masterList)
    print("master list:", masterCandidateList)
    mainAVS = AVSLogic("votesDB.db", "voterDB.db", offices, masterCandidateList,  AVSexception_handler)

    def voteSequence(ballot):
        if ballot is None:
            return

        offices = ballot.offices
        for office in offices:
            officeClass = getattr(ballot, office)
            candidates = officeClass.candidateList
            print(office, ":")
            for index, candidate in enumerate(candidates):
                print(index + 1, candidate.name, " from ", candidate.party)

            inpt = ""
            while not inpt.isnumeric():
                inpt = str(input("Choose the index of your  candidate (0 for none)"))

            inpt = int(inpt)
            if inpt == 0:
                candidate = None
            else:
                candidate = candidates[inpt - 1]

                ballot.vote(candidate.office, candidate)

        mainAVS.processBallot(ballot)

    ballot = mainAVS.newVoterBallot("Jami", "11", "MCL")
    if ballot != None:
        print(ballot.President)
        ballot._spillBallot(offices)
        #ballot.vote(masterCandidateList["President"][0])
        #ballot._spillBallot(offices)
        #ballot.vote(masterCandidateList["President"][1])
        #ballot._spillBallot(offices)
        #ballot.vote(masterCandidateList["Vice President"][0])
        ballot.vote(AVSPrimitives.Candidate("President", "Ryan Jaminal", "President",  "Real Party"))
        ballot._spillBallot(offices)
        ballot.vote(AVSPrimitives.Candidate("President", "Sumwam erusu", "President", "LALa land"))
        ballot._spillBallot(offices)
        mainAVS.processBallot(ballot)
        ballot = mainAVS.newVoterBallot("lul", "11", "MCL")
        ballot._spillBallot(offices)
        mainAVS.processBallot(ballot)

    ballot = mainAVS.newVoterBallot("jp", "10", "Einstein")
    ballot = AVSPrimitives.Ballot(AVSPrimitives.Voter("lel", "11", "rizael"), mainAVS.offices, mainAVS.masterCandidateList)
    voteSequence(ballot)
    name = ""
    while name != "STAHP":
        name = str(input("Name: "))
        ballot = mainAVS.newVoterBallot(name, 10, "Einstein")
        voteSequence(ballot)

    print("\n\n")
    print("----------------------------------------")

"""
    candidates = []
    candidate = Candidate("jaminal", "President", "Turks Party")
    candidates.append(candidate)

    name =""
    while name != "STAHP":
        name = str(input("Candidate Name:"))
        office = str(input("Office: "))
        candidate = Candidate(name, office, "Turks Party")
        candidates.append(candidate)

    votesDB = VotesDB("/mnt/sdcard/votesDB_trial.db", candidates)
    votesDB.incrementVote(candidates)
    for i in ("7 times"):
        votesDB.incrementVote(candidates[:3])
"""
