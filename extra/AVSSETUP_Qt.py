import hashlib
import json
from pathlib import PurePath
import sys

from PySide2.QtWidgets import (QApplication, QAbstractItemView, QLabel, QPushButton,
                               QHBoxLayout, QVBoxLayout, QScrollArea, QWidget, QFormLayout, QGroupBox, QLineEdit, QLayout, QTabWidget, QTreeWidget, QListWidget, QListWidgetItem, QMessageBox, QInputDialog, QFileDialog, QComboBox, QTreeWidgetItem, QHeaderView, QSplitter)
from PySide2.QtCore import Slot, Qt
from PySide2.QtGui import QStandardItemModel, QStandardItem

offices = [
    "President",
    "Vice President",
    "Secretary",
    "Auditor",
    "Treasurer",
    "PIO",
    "Peace Officer",
    "Grade 8 Representative",
    "Grade 9 Representative",
    "Grade 10 Representative",
    "Grade 11 Representative",
    "Grade 12 Representative"
]

parties = [
    "First Party",
    "Second Patty",
    "Third Party Games"
]

#offices = list(map(str, range(100)))

test_candidates = {
    parties[0]: {
        offices[0]: "Test 1"
    },
    parties[1]: {
        offices[1]: "Test 2"
    },
    parties[2]: {
        offices[-1]: "Final Destination"
    },
    "Independent": {
        offices[0]: ["1", "2", "3"],
        offices[1]: ["a", "b"],
        offices[2]: ["11", "11", "1"],
        offices[-1]: ["a", "Uniqlo"]
    }
}

"""
class IndependentCandidatesListWidget(QWidget):
    def __init__(self, office, candidates=None):
        QWidget.__init__(self)
        self.party = "Independent"
        self.office = office
        candidates = candidates or ()

        candidateCreationButton = QPushButton("+")
        candidateCreationButton.clicked.connect(self.GUIaddCandidate)
        candidateRemoveButton = QPushButton("-")
        candidateRemoveButton.clicked.connect(self.GUIremoveSelectedCandidates)

        header = QHBoxLayout()
        header.addWidget(QLabel(self.office))
        header.addStretch(1)
        header.addWidget(candidateCreationButton)
        header.addWidget(candidateRemoveButton)

        self.candidateListWidget = QListWidget()
        self.candidateListWidget.setSortingEnabled(True)
        self.candidateListWidget.setSelectionMode(QAbstractItemView.MultiSelection)

        mainLayout = QVBoxLayout()
        mainLayout.addLayout(header)
        mainLayout.addWidget(self.candidateListWidget)
        self.setLayout(mainLayout)

        self.loadCandidates(candidates, updateOnly=False)

    def GUIaddCandidate(self):
        name, ok = QInputDialog().getText(self.window(), "Add new Independently running candidate", "Enter name of candidate runnig for " + self.office + ": ")
        if ok:
            self.addCandidate(name)

    def GUIremoveSelectedCandidates(self):
        candidates = [candidateItem.text() for candidateItem in self.candidateListWidget.selectedItems()]
        if not candidates:      #No candidates selected. Do nothing
            return

        dialog = QMessageBox()
        dialog.setText("Removing independently running candidates")
        dialog.setInformativeText("The following candidates running for " + self.office + " will be removed:\n" + "\n".join(candidates))
        dialog.setStandardButtons(QMessageBox.Cancel | QMessageBox.Ok)
        
        if dialog.exec() == QMessageBox.Ok:
            #selectedItems() called again here as indices are invalidated each removal
            for candidateItem in self.candidateListWidget.selectedItems():
                self.removeCandidate(self.candidateListWidget.row(candidateItem))

    def addCandidate(self, name):
        name = name.strip()
        if not name:
            return

        candidateListWidgetItem = QListWidgetItem()
        candidateListWidgetItem.setText(name)
        candidateListWidgetItem.setFlags(candidateListWidgetItem.flags() | Qt.ItemIsEditable)
        self.candidateListWidget.insertItem(0, candidateListWidgetItem)
        self.candidateListWidget.sortItems()

    def removeCandidate(self, index):
        #Ensure first that the ListWidgetItem to be removed is inactive as to prevent a crash
        candidateItem = self.candidateListWidget.item(index)
        candidateItem.setSelected(False)
        safe_row = 0 if index else 1
        self.candidateListWidget.setCurrentRow(safe_row)
        self.candidateListWidget.setCurrentItem(self.candidateListWidget.item(safe_row))
        #Finally, delete the ListWidgetItem
        self.candidateListWidget.takeItem(index)

    def loadCandidates(self, candidates, updateOnly=True):
        if not updateOnly:
            for index in range(self.candidateListWidget.count()):
                self.removeCandidate(index)

        for candidate in candidates:
            self.addCandidate(candidate)

    def getCandidates(self):
        #A continuously updated self.candidates is not used here as entries can be edited and workarounds would have to be made in order to map each ListWidget entry to the name in the list 
        candidates = []
        for row in range(self.candidateListWidget.count()):
            candidates.append(self.candidateListWidget.item(row).text().strip())

        return candidates

class IndependentCandidatesConfigWidget(QWidget):
    def __init__(self, offices=()):
        QWidget.__init__(self)
        self.party = "Independent"
        self.offices = []
        self.candidates = dict.fromkeys(offices, [])

        self.mainLayout = QVBoxLayout()
        self.loadOffices(offices, updateOnly=False)

        groupBox = QGroupBox()
        groupBox.setLayout(self.mainLayout)
        scroll = QScrollArea()
        scroll.setWidget(groupBox)
        scroll.setWidgetResizable(True)

        scrollLayout = QVBoxLayout()
        scrollLayout.addWidget(scroll)
        self.setLayout(scrollLayout)

    def addOffice(self, office):
        if office in self.offices:
            return

        self.offices.append(office)
        wrapperLayout = QVBoxLayout()
        officeListWidget = IndependentCandidatesListWidget(office, ("test", "lol", "remove1", "remove2", "wow"))
        wrapperLayout.addWidget(officeListWidget)
        self.mainLayout.addLayout(wrapperLayout)

    def loadOffices(self, offices, updateOnly=True):
        if not updateOnly:
            #Remove currently existing offices not present in the new offices loaded
            for index in range(len(self.offices)-1, -1, -1):    #Traverse in reverse so that the index is not invalidated after each removal
                if self.offices[index] not in offices:
                    wrapperLayout = self.mainLayout.itemAt(index)
                    print(index, self.offices[index], wrapperLayout)
                    hasChild = True
                    while hasChild:
                        hasChild = wrapperLayout.takeAt(0)

                    self.mainLayout.removeItem(wrapperLayout)
                    del self.offices[index]
                ###

        for office in offices:
            self.addOffice(office)

    def getCandidates(self):
        output = {}
        for index in range(self.mainLayout.count()):
            officeListWidget = self.mainLayout.itemAt(index).widget()
            candidates = officeListWidget.getCandidates()
            if any(candidates):     #Only add if the office contains at least one non-empty name
                output[officeListWidget.office] = candidates

        return output
    """

class IndependentCandidatesConfigWidget(QWidget):
    def __init__(self, offices=()):
        QWidget.__init__(self)
        self.party = "Independent"
        self.offices = offices or []

        self.officesModel = QStandardItemModel()
        self.candidatesTreeWidget = QTreeWidget()
        #Columns: Independent candidate Number (useful for disambiguation of candidates with the same name), Office/Position, Candidate Name, sortingKey (Used to deterministically generate the number in the first column)
        self.candidatesTreeWidget.setColumnCount(4)
        self.candidatesTreeWidget.header().hideSection(3)   #sortingKey is not useful by itself and is quite an eyesore
        self.candidatesTreeWidget.setHeaderLabels(("#", "Office", "Name", "_SortingKey"))
        self.candidatesTreeWidget.header().resizeSection(0, 50)
        self.candidatesTreeWidget.header().setSectionResizeMode(1, QHeaderView.ResizeToContents)
        self.candidatesTreeWidget.setSelectionMode(QAbstractItemView.MultiSelection)
        self.loadOffices(offices, updateOnly=False)

        officeComboBox = QComboBox()
        officeComboBox.setModel(self.officesModel)
        officeComboBox.setMaxVisibleItems(12)
        candidateNameEntry = QLineEdit()
        candidateNameEntry.setPlaceholderText("Enter name of independently running candidate")
        candidateCreationButton = QPushButton("+")
        candidateCreationButton.clicked.connect(lambda combobox=officeComboBox, entry=candidateNameEntry: self.addCandidate(combobox.currentText(), entry.text()))
        candidateRemoveButton = QPushButton("-")
        candidateRemoveButton.clicked.connect(self.GUIremoveSelectedCandidates)

        header = QHBoxLayout()
        header.addWidget(officeComboBox)
        header.addWidget(candidateNameEntry)
        header.addWidget(candidateCreationButton)
        header.addWidget(candidateRemoveButton)

        mainLayout = QVBoxLayout()
        mainLayout.addLayout(header)
        mainLayout.addWidget(self.candidatesTreeWidget)
        
        groupBox = QGroupBox()
        groupBox.setLayout(mainLayout)
        scroll = QScrollArea()
        scroll.setWidget(groupBox)
        scroll.setWidgetResizable(True)

        scrollLayout = QVBoxLayout()
        scrollLayout.addWidget(scroll)
        self.setLayout(scrollLayout)

    def GUIremoveSelectedCandidates(self):
        items = self.candidatesTreeWidget.selectedItems()
        print(items)
        for item in items:
            self.candidatesTreeWidget.takeTopLevelItem(self.candidatesTreeWidget.indexOfTopLevelItem(item))

        self.GUIupdateCandidateNumbers()

    def GUIupdateCandidateNumbers(self):
        self.candidatesTreeWidget.sortItems(3, Qt.SortOrder.AscendingOrder)
        for index in range(self.candidatesTreeWidget.topLevelItemCount()):
            candidateTreeWidget = self.candidatesTreeWidget.topLevelItem(index)
            candidateTreeWidget.setText(0, str(index+1))

    def generateSortingKey(self, office, name):
        try:
            officeIndex = self.offices.index(office)
        except ValueError:
            print(office, "not in valid independent offices. Skipping", name)
            return

        #Independent candidates are sorted first by the order of their offices (as in the order they appear in self.offices), then by name
        #As QTabWidget does not expose an easy api to customize sorting with 2 columns, a sortingKey column is added
        #The raw officeIndex can not be just prepended to the name as text is sorted naturally (e.g. "2" > "11", "22" > "10000")
        #Picking a max_num_digits (e.g. 1 -> 001), while a reasonable solution, artificially limits the possible number of offices, and an easy to implement alternative exists
        
        #map the indices to (capital) letters (1-26 -> A-Z), prepending 'Z' as the place value filler (just like 0 in decimal) for indices greater than 26
        #This results to a place value that increases from left to right, perfect for natural sorting
        #',' is used as a delimiter as it has a lower ascii representation to the letters (e.g. "A," < "AZ," as ',' < 'Z')
        filler, end = divmod(officeIndex, 26)            
        officeSortingKey = ("Z" * filler) + chr(65 + end) + ","

        return officeSortingKey + name

    def addOffice(self, office):
        if office in self.offices:
            return

        self.offices.append(office)
        self.officesModel.appendRow(QStandardItem(office))
        #Just appending to list of offices, so no need to update sortingKeys. sortingKeys are not invalidated as previous indices stay the same.

    def loadOffices(self, offices, updateOnly=True):
        if not updateOnly:
            self.officesModel.clear()
            self.offices = []

        if self.offices == offices:
            return

        #?when updateOnly is True, preserve previous office order or adopt new order?
        for office in offices:
            self.addOffice(office)

        #Remove entries for candidates whose offices were removed
        for index in range(self.candidatesTreeWidget.topLevelItemCount()-1, -1, -1):
            candidateTreeWidget = self.candidatesTreeWidget.topLevelItem(index)
            office = candidateTreeWidget.text(1)
            if not self.officesModel.findItems(office):
                self.candidatesTreeWidget.takeTopLevelItem(index)
                continue

            name = candidateTreeWidget.text(2)
            #Order of offices probably changed. Update the sorting keys
            candidateTreeWidget.setText(3, self.generateSortingKey(office, name))

    def addCandidate(self, office, name):
        if office not in self.offices:
            print(office, "not in the valid offices. Skipping", office, name)
            return

        candidateWidgetItem = QTreeWidgetItem( ("#", office, name, self.generateSortingKey(office, name)) )
        self.candidatesTreeWidget.addTopLevelItem(candidateWidgetItem)
        self.GUIupdateCandidateNumbers()

    def loadCandidates(self, candidatesPerOffice, updateOnly=True):
        if not updateOnly:
            for index in range(self.candidatesTreeWidget.topLevelItemCount()-1, -1, -1):
                self.candidatesTreeWidget.takeTopLevelItem(index)

        for office in candidatesPerOffice:
            candidates = candidatesPerOffice[office]
            for candidate in candidates:
                self.addCandidate(office, candidate)

    def getCandidates(self):
        output = {}
        for index in range(self.candidatesTreeWidget.topLevelItemCount()):
            candidateTreeWidget = self.candidatesTreeWidget.topLevelItem(index)
            office = candidateTreeWidget.text(1)
            candidate = candidateTreeWidget.text(2)

            if office not in output:
                output[office] = []

            output[office].append(candidate)

        return output

class PartyCandidatesConfigWidget(QWidget):
    def __init__(self, party, offices=()):
        QWidget.__init__(self)
        self.party = party
        self.offices = []
        self.candidates = dict.fromkeys(offices, "")

        self.mainLayout = QFormLayout()
        self.loadOffices(offices, updateOnly=False)
        
        groupBox = QGroupBox()
        groupBox.setLayout(self.mainLayout)
        scroll = QScrollArea()
        scroll.setWidget(groupBox)
        scroll.setWidgetResizable(True)

        scrollLayout = QVBoxLayout()
        scrollLayout.addWidget(scroll)
        self.setLayout(scrollLayout)

    def updateCandidate(self, office, candidate):
        candidate = candidate.strip()
        self.candidates[office] = candidate
        return
        for office in self.offices: 
            print(office, self.candidates[office])
        print()

    def addOffice(self, office):
        if office in self.offices:
            return

        entry = QLineEdit()
        entry.setPlaceholderText("Type name of candidate or leave blank if there is none")
        entry.textChanged.connect(lambda text: self.updateCandidate(office, text))

        self.mainLayout.addRow(QLabel(office), entry)
        self.offices.append(office)
        self.candidates[office] = ""

    def loadOffices(self, offices, updateOnly=True):
        candidates = {}
        for index in range(len(self.offices)-1, -1, -1):
            office = self.offices[index]
            candidates[office] = self.mainLayout.itemAt(index, QFormLayout.FieldRole).widget().text()      #Save the currently typed candidates first
            self.mainLayout.removeRow(index)

        if updateOnly:
            for office in self.offices:
                if office not in offices:
                    offices.append(office)

        self.offices = []
        self.candidates = {}
        for office in offices:
            self.addOffice(office)

        self.loadCandidates(candidates)
        ###

    def loadCandidates(self, candidates):
        for office in candidates:
            try:
                office_index = self.offices.index(office)
            except ValueError:
            #if office_index == -1:
                print("Party", "'" + self.party + "':", office, "not in the offices. Skipping", candidates[office])
                continue

            self.mainLayout.itemAt(office_index, QFormLayout.FieldRole).widget().setText(candidates[office])

    def getCandidates(self):
        #Filter out empty offices
        output = {}
        for office in self.candidates:
            candidate = self.candidates[office]
            if candidate:
                output[office] =  candidate

        return output

class CandidatesConfigWidget(QWidget):
    defaultOffices = [
        "President",
        "Vice President",
        "Secretary",
        "Auditor",
        "PIO",
        "Peace Officer",
        "Treasurer",
        "Grade 8 Representative",
        "Grade 9 Representative",
        "Grade 10 Representative",
        "Grade 11 Representative",
        "Grade 12 Representative"
        ]

    def __init__(self, offices=defaultOffices, parties=None, label="Candidates"):
        self._officeIndex = 0
        QWidget.__init__(self)
        self.offices = []
        self.parties = []

        addPartyButton = QPushButton("+")
        addPartyButton.clicked.connect(self.GUIaddParty)

        self.partyTabWidget = QTabWidget()
        self.partyTabWidget.setTabsClosable(True)
        self.partyTabWidget.setUsesScrollButtons(True)
        self.partyTabWidget.setMovable(True)
        self.partyTabWidget.setTabShape(QTabWidget.Triangular)
        self.partyTabWidget.tabCloseRequested.connect(self.GUIremovePartyTab)
        self.partyTabWidget.setCornerWidget(addPartyButton)

        self.loadOffices(offices or ())
        for party in (parties or ()):
            self.addParty(party)

        #Add a dummy party and Independent by default, mainly for discoverability
        self.addParty("Sample Party(delete)")
        self.addParty("Independent")

        saveButton = QPushButton("Save to JSON")
        saveButton.clicked.connect(self.getCandidates)
        centeringLayout = QHBoxLayout()
        _addOfficeButton = QPushButton("_addOffice")
        _addOfficeButton.clicked.connect(self._addOffice)
        _loadOfficesButton = QPushButton("_loadOffices")
        _loadOfficesButton.clicked.connect(self._loadOffices) 
        _loadCandidatesButton = QPushButton("_loadCandidates")
        _loadCandidatesButton.clicked.connect(self._loadCandidates)
        centeringLayout.addStretch(1)
        centeringLayout.addWidget(saveButton)
        centeringLayout.addWidget(_addOfficeButton)
        centeringLayout.addWidget(_loadOfficesButton)
        centeringLayout.addWidget(_loadCandidatesButton)
        centeringLayout.addStretch(1)

        mainLayout = QVBoxLayout()
        mainLayout.addWidget(QLabel(label))
        mainLayout.addWidget(self.partyTabWidget)
        mainLayout.addLayout(centeringLayout)
        self.setLayout(mainLayout)

    def GUIaddParty(self):
        party, ok = QInputDialog().getText(self.window(), "Add a new Party", "Party Name:")
        if ok:
            self.addParty(party)

    def GUIremovePartyTab(self, index):
        party = self.partyTabWidget.widget(index).party
        if party == "Independent":
            return

        dialog = QMessageBox()
        dialog.setText("Remove the party '" + party + "'?")
        dialog.setInformativeText("The supplied candidates for the party will also be deleted.")
        dialog.setStandardButtons(QMessageBox.Cancel | QMessageBox.Ok)
        
        if dialog.exec() == QMessageBox.Ok:
            self.removePartyTab(index)

    def addParty(self, party):
        party = party.strip()
        if not party or (party in self.parties):
            return

        partyTab = PartyCandidatesConfigWidget(party, self.offices) if party != "Independent" else IndependentCandidatesConfigWidget(self.offices)
        self.partyTabWidget.addTab(partyTab, party)
        self.parties.append(party)

    def removePartyTab(self, index):
        partyTab = self.partyTabWidget.widget(index)
        safe_index = index-1 if index else index+1      #Prefer the adjacent tabs; Prefer the preceding tab
        safe_index = min(safe_index, self.partyTabWidget.count()-1)    #Since the Independent Tab is sure to be there for now, make it a fallback if there is no adjacent tab
        self.partyTabWidget.setCurrentIndex(safe_index)
        self.partyTabWidget.removeTab(index)

    def addOffice(self, office):
        if office in self.offices:
            return

        self.offices.append(office)
        for index in range(self.partyTabWidget.count()):
            partyTab = self.partyTabWidget.widget(index)
            partyTab.addOffice(office)

    def loadOffices(self, offices, updateOnly=True):
        if not updateOnly:
            self.offices = []

        for index in range(self.partyTabWidget.count()):
            partyTab = self.partyTabWidget.widget(index)
            partyTab.loadOffices(offices, updateOnly)

        self.offices = offices

    def loadCandidates(self, candidatesPerParty, updateOnly=True):
        parties = []
        if not updateOnly:
            for index in range(self.partyTabWidget.count()-1, -1, -1):
                self.removePartyTab(index)
            self.parties = []

        for party in candidatesPerParty:
            self.addParty(party)

        for index in range(self.partyTabWidget.count()):
            partyTab = self.partyTabWidget.widget(index)
            party = partyTab.party
            partyTab.loadCandidates(candidatesPerParty[party])
            ###

    def getCandidates(self):
        output = {}
        for index in range(self.partyTabWidget.count()):
            partyTab = self.partyTabWidget.widget(index)
            output[partyTab.party] = partyTab.getCandidates()
        
        print(output)
        file = open("candidates.json", "w")
        json.dump(output, file, indent=4)
        file.close()

        return output

    def _addOffice(self):
        office = offices[self._officeIndex]
        self.addOffice(office)
        self._officeIndex = (self._officeIndex + 1) % len(offices) 

    def _loadOffices(self):
        new_offices = offices[:self._officeIndex]
        new_offices.sort()
        self.loadOffices(new_offices, updateOnly=False)
        self._officeIndex = (self._officeIndex + 1) % len(offices) 

    def _loadCandidates(self):
        self.loadCandidates(test_candidates, False)




class FilePathEntryAndButton(QWidget):
    def __init__(self):
        QWidget.__init__(self)

class ElectionConfigWidget(QWidget):
    def __init__(self, label="Config"):
        QWidget.__init__(self)
        configWidgetLayouts = []

        ####Start of officesWidgetLayout
        officeCreateButton = QPushButton("+")
        officeCreateButton.clicked.connect(self.GUIaddOffice)
        officeRemoveButton = QPushButton("-")
        officeRemoveButton.clicked.connect(self.GUIremoveSelectedOffices)

        header = QHBoxLayout()
        instructions = QLabel("Offices" and "Use the buttons to add and remove offices.\nDrag and drop to reorder. Hold Ctrl or Shift for Multiselect")
        instructions.setAlignment(Qt.AlignHCenter)
        header.addWidget(instructions)
        header.addStretch(1)
        header.addWidget(officeCreateButton)
        header.addWidget(officeRemoveButton)

        self.officeListWidget = QListWidget()
        self.officeListWidget.setSortingEnabled(False)
        self.officeListWidget.setSelectionMode(QAbstractItemView.ExtendedSelection)
        self.officeListWidget.setDragDropMode(QAbstractItemView.InternalMove)
        self.officeListWidget.setDragEnabled(True)
        self.officeListWidget.itemChanged.connect(lambda item: self.getOffices())       #Debug

        scroll = QScrollArea()
        scroll.setWidget(self.officeListWidget)
        scroll.setWidgetResizable(True)
        officesWidgetLayout = QVBoxLayout()
        officesWidgetLayout.addLayout(header)
        officesWidgetLayout.addWidget(scroll)
        
        configWidgetLayouts.append( ("Offices", officesWidgetLayout) )
        #@@@End of officesWidgetLayout

        ####Start of mainConfigLayout
        self.passwordHashEntry = QLineEdit()
        self.passwordHashEntry.setPlaceholderText("Password HASH using sha256 and the salt E13C7")
        setPasswordButton = QPushButton("Set Password")
        setPasswordButton.clicked.connect(self.GUIsetPassword)

        passwordHashLayout = QHBoxLayout()
        passwordHashLayout.addWidget(QLabel("Password HASH: "))
        passwordHashLayout.addWidget(self.passwordHashEntry)
        passwordHashLayout.addWidget(setPasswordButton)


        fileConfigsLayout = QVBoxLayout()
        for fileConfig in ("Pictures Folder", "Votes DB", "Voter DB", "MasterDB"):
            if "folder" in fileConfig.lower():
                fileDialog = QFileDialog.getExistingDirectory
            else:
                fileDialog = lambda *args, **kwargs: QFileDialog.getSaveFileName(*args, **kwargs)[0]

            pathEntry = QLineEdit()
            pathEntry.setPlaceholderText("Manually enter path or use the button")
            pathChooserButton = QPushButton("Choose")
            if "folder" in fileConfig.lower():
                pathChooserButton.clicked.connect(lambda checked, entry=pathEntry: entry.setText(str(PurePath(Q(self.window(), "Select " + fileConfig)))))
            else:
                #TODO: Warn user
                pass
        configWidgetLayouts.append( ("Password", passwordHashLayout) )
        #@@@End of mainConfigLayout

        configTabWidget = QTabWidget()
        configTabWidget.setTabsClosable(False)
        configTabWidget.setUsesScrollButtons(True)
        configTabWidget.setTabShape(QTabWidget.Triangular)
        for name, layout in configWidgetLayouts:
            tabWidget = QWidget()
            tabWidget.setLayout(layout)
            configTabWidget.addTab(tabWidget, name)

        mainLayout = QVBoxLayout()
        mainLayout.addWidget(QLabel(label))
        mainLayout.addWidget(configTabWidget)
        self.setLayout(mainLayout)

    def GUIaddOffice(self):
        office, ok = QInputDialog().getText(self.window(), "Add an Office/Position", "Type Office/Position:")
        if ok:
            self.addOffice(office)

    def GUIremoveSelectedOffices(self):
        for officeItem in self.officeListWidget.selectedItems():
            self.officeListWidget.takeItem(self.officeListWidget.row(officeItem))

    def GUIsetPassword(self):
        password, ok = QInputDialog().getText(self.window(), "Set Password", "Enter new COMELEC password:")
        if ok:
            self.passwordHashEntry.setText(hashlib.sha256("E13C7".encode("utf8") + password.encode("utf8")).hexdigest())

    def addOffice(self, office):
        candidateListWidgetItem = QListWidgetItem()
        candidateListWidgetItem.setText(office)
        candidateListWidgetItem.setFlags(candidateListWidgetItem.flags() | Qt.ItemIsEditable)
        self.officeListWidget.addItem(candidateListWidgetItem)

    def loadOffices(self, offices):
        for index in range(self.officialListWidget.count()-1, -1, -1):
            self.officeListWidget.takeItem(index)

        for office in offices:
            self.addOffice(office)

    def getOffices(self):
        offices = []
        for index in range(self.officeListWidget.count()):
            officeItem = self.officeListWidget.item(index)
            offices.append(officeItem.text())

        print(offices)
        return offices

class AVSVoterInfoWidget(QWidget):      ####
    def __init__(self):
        QWidget.__init__(self)
        entryGroupBox = QGroupBox()
        entryGroupBox.setTitle("Voter Info")
        verticalLayout = QVBoxLayout()
        
        self.fieldEntries = []
        for field in ("Name", "Grade", "Section"):
            entry = QLineEdit()
            entry.setPlaceholderText("Type " + field)

            fieldLayout = QVBoxLayout()
            fieldLayout.setSizeConstraint(QLayout.SetMinimumSize)
            fieldLayout.addWidget(QLabel(field + ":"))            
            fieldLayout.addWidget(entry)

            verticalLayout.addLayout(fieldLayout)
            
            self.fieldEntries.append(entry)

        submitButton = QPushButton("Submit")
        submitButton.clicked.connect(self.submitVoterInfo)

        verticalLayout.addWidget(submitButton)
        entryGroupBox.setLayout(verticalLayout)
        mainLayout = QVBoxLayout()
        mainLayout.addStretch(1)
        mainLayout.addWidget(entryGroupBox)
        mainLayout.addStretch(1)
        centeringLayout = QHBoxLayout()
        centeringLayout.addStretch(1)
        centeringLayout.addLayout(mainLayout)
        centeringLayout.addStretch(1)
        self.setLayout(centeringLayout)

    def submitVoterInfo(self):
        for entry in self.fieldEntries:
            print(entry.placeholderText(), entry.text())

        print()

if __name__ == "__main__":
    app = QApplication(sys.argv)
    widget = CandidatesConfigWidget()
    scroll1 = QScrollArea()
    scroll1.setWidget(widget)
    scroll1.setWidgetResizable(True)

    widget2 = ElectionConfigWidget()
    scroll2 = QScrollArea()
    scroll2.setWidget(widget2)
    scroll2.setWidgetResizable(True)

    splitview = QSplitter()
    splitview.addWidget(scroll1)
    splitview.addWidget(scroll2)
    splitview.show()
    sys.exit(app.exec_())

