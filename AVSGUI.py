#!/usr/bin/env python3
#TODO: add fallback photo for when photo is not supplied for candidate

from datetime import datetime
from pathlib import PurePath
import os
import threading

import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GdkPixbuf, GObject#, Pango

import Core.AVSDB as AVSDB    #For Main avs logic
from Core.AVS_Misc_Utilities import centerText, getCandidateImageFilename


def loadGtkImage(filename, width=None, height=None, id=""):    ####TODO: Move this to a more appropriate location
    filename = str(PurePath(filename))
    identifier = filename + "_" + id + "_" + str(width) + "x" + str(height)

    loadGtkImage.lock.acquire()
    if identifier in loadGtkImage.cache:
        return loadGtkImage.cache[identifier]
    loadGtkImage.lock.release()

    image = Gtk.Image.new_from_file(filename)
    pixbuf = image.get_pixbuf()
    if pixbuf is None:      #Picture pointed to by filename either does not exist or is invalid
        return image

    if (width is not None) and (height is not None):
        pixbuf = image.get_pixbuf()
        #Scale using best scaling algorithm available as it will only be ran just once
        pixbuf = pixbuf.scale_simple(width, height, GdkPixbuf.InterpType.HYPER)
        image = Gtk.Image.new_from_pixbuf(pixbuf)

    del pixbuf      #Just ensuring that the pixbuf is deleted as it might take up space
    loadGtkImage.lock.acquire()
    if image is not None:
        loadGtkImage.cache[identifier] = image
    loadGtkImage.lock.release()

    return loadGtkImage.cache[identifier]
loadGtkImage.cache = {}
loadGtkImage.lock = threading.RLock()

def preloadCandidateImages(candidates, pictureFormat, picturesFolder, referenceScreen=None):
    referenceScreen = referenceScreen or Gtk.Box().get_screen()
    pictureSize = referenceScreen.get_height() / 5.5
    for candidate in candidates:
        filename = PurePath(picturesFolder + getCandidateImageFilename(candidate, pictureFormat))
        loadGtkImage(filename, pictureSize, pictureSize)

####Legacy UI Code
class LegacyOfficeBox(Gtk.Box):
    def __init__(self, officeClass, *args, **kwargs):
        Gtk.Box.__init__(self, *args, **kwargs)
        self.office = officeClass

        officeLabel = Gtk.Label()
        officeLabel.set_markup("<b>" + officeClass.officeName + "</b>")
        officeLabel.set_max_width_chars(25)
        officeLabel.set_ellipsize(3)     #Pango.EllipsizeMode.END
        officeLabel.set_tooltip_text(officeClass.officeName)

        self.candidatesComboBox = AVSLegacyComboBox(sorted(officeClass.candidateList))
        self.candidatesComboBox.connect("changed", self.candidateChosen)
        self.candidatesComboBox.set_size_request(self.get_screen().get_width()*(19/30), -1)      #Hardcoded combobox width scale for now
        clearButton = Gtk.Button.new_from_icon_name("edit-clear", Gtk.IconSize.BUTTON)
        clearButton.connect("clicked", self.clearVoted)

        padding = self.get_screen().get_width()/20
        self.pack_start(Gtk.Label(), False, False, padding)
        self.pack_start(officeLabel, False, False, 0)
        self.pack_end(Gtk.Label(), False, False, padding)
        self.pack_end(clearButton, False, False, 0)
        self.pack_end(self.candidatesComboBox, False, False, 0)

    def candidateChosen(self, candidatesComboBox):
        voted = candidatesComboBox.get_voted_candidate()
        self.office.voteCandidate(voted)

    def clearVoted(self, clearButton):
        self.office.voteCandidate(None)
        self.candidatesComboBox.set_active(-1)

class AVSLegacyComboBox(Gtk.ComboBoxText):
    def __init__(self, candidates, *args, **kwargs):
        Gtk.ComboBoxText.__init__(self, *args, **kwargs)
        self.candidates = [None]    #The First choice is none of the candidates, i.e. abstain
        self.candidates.extend(candidates)

        for candidate in self.candidates:
            if candidate is None:
                self.append_text("")
            else:
                self.append_text(candidate.name)

    def get_voted_candidate(self):
        votedIndex = self.get_active()     #Relies on the fact that the order of self.candidates is the same order the candidate's name is appended to the combobox
        if votedIndex == -1:        #Special cased as Selecting none in the combobox returns -1, which is still a valid index in python
            votedIndex = None

        votedCandidate = None
        if votedIndex is not None:
            votedCandidate = self.candidates[votedIndex]

        return votedCandidate

class AVSLegacyMainVotingScreen(Gtk.Box):
    def __init__(self, config, *args, **kwargs):
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.VERTICAL)
        self.config = config

    def run(self, ballot, mainAVS, orderedStack):
        self.ballot = ballot
        ballotFlowBox = Gtk.FlowBox()
        ballotFlowBox.set_orientation(Gtk.Orientation.VERTICAL)
        ballotFlowBox.set_min_children_per_line(8)
        ballotFlowBox.set_max_children_per_line(self.config["maxOfficesPerColumn"])  #8
        ballotFlowBox.set_selection_mode(Gtk.SelectionMode.NONE)
        ballotFlowBox.set_column_spacing(self.config["officeBoxSpacing"])    #50
        ballotFlowBox.set_row_spacing(self.config["officeBoxSpacing"])    #20

        offices = ballot.offices
        for office in offices:
            officeClass = getattr(self.ballot, office)
            if len(officeClass.candidateList) < 1:
                #Skip offices with no candidates
                continue

            ballotFlowBox.add(LegacyOfficeBox(officeClass))

        navigationControls = Gtk.Box()
        backButton = Gtk.Button(label="Back")
        backButton.connect("clicked", self.goBack, orderedStack)
        submitButton = Gtk.Button(label="Continue")
        submitButton.connect("clicked", self.submitBallot, mainAVS, orderedStack)
        navigationControls.pack_start(backButton, False, False, 0)
        navigationControls.pack_start(submitButton, False, False, 10)
        navigationControls.set_halign(Gtk.Align.CENTER)


        scroll = Gtk.ScrolledWindow()
        scroll.add(ballotFlowBox)
        mainContent = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        mainContent.pack_start(scroll, True, True, 50)
        mainContent.pack_end(navigationControls, False, False, 10)

        logo = loadGtkImage("logo.png", 250, 250)
        #Put logo on the bottom-right
        logo.set_halign(Gtk.Align.END)
        logo.set_valign(Gtk.Align.END)

        overlay = Gtk.Overlay()
        overlay.add_overlay(logo)
        overlay.add_overlay(mainContent)
        self.pack_start(overlay, True, True, 0)
        self.show_all()

    def reset(self):
        for child in self.get_children():
            self.remove(child)

        self.ballot = None

    def goBack(self, backButton, orderedStack):
        confirmationDialog = Gtk.MessageDialog(parent=self.get_toplevel(),
            message_type=Gtk.MessageType.QUESTION,
            buttons=Gtk.ButtonsType.OK_CANCEL,
            text="Are you sure you want to discard this ballot?\nYour votes will NOT be recorded.")

        response = confirmationDialog.run()
        confirmationDialog.destroy()
        if response == Gtk.ResponseType.OK:
            orderedStack.previous(reset=True)

    def submitBallot(self, submitButton, mainAVS, orderedStack):
        orderedStack.next(self.ballot, mainAVS, orderedStack, reset=True)

class CandidatePortrait(Gtk.Box):
    def __init__(self, candidate, picture_filename, width, height, add_label=True, picture_identifier=""):     #TODO: Find a way to eliminate the need for picture_identifier
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.VERTICAL)
        candidatePicture = loadGtkImage(picture_filename, width, height, picture_identifier)
        #Do not append party if candidate is independently running and has a unique name (no identifier set)
        candidateIdentifier = "[{}]".format(candidate.party) if ( (not candidate.isIndependent()) or candidate.identifier) else ""

        self.label = candidate.name + candidateIdentifier
        candidateLabel = Gtk.Label(label=self.label)
        candidateLabel.set_max_width_chars(10)
        candidateLabel.set_ellipsize(3)     #Pango.EllipsizeMode.END

        self.set_tooltip_text(self.label)
        self.pack_start(candidatePicture, False, False, 0)
        self.pack_end(candidateLabel, False, False, 0)

class CandidateToggleButton(Gtk.ToggleButton):
    def __init__(self, candidate, picture_filename):
        Gtk.Button.__init__(self)
        self.candidate = candidate
        pictureSize = self.get_screen().get_height() / 5.5
        candidatePortrait = CandidatePortrait(candidate, picture_filename, pictureSize, pictureSize)

        self.add(candidatePortrait)
        #Also set tooltip here to widen area that activates tooltip, not just the image
        self.set_tooltip_text(candidatePortrait.label)
        self.set_size_request(pictureSize, pictureSize)

class OfficeBox(Gtk.Box):
    def __init__(self, office, picturesFolder, pictureFormat=".jpg"):
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.VERTICAL)
        self.office = office

        candidateButtonsBox = Gtk.FlowBox()
        candidateButtonsBox.set_min_children_per_line(2)
        candidateButtonsBox.set_max_children_per_line(3)     #Pango.EllipsizeMode.END

        candidateButtonsBox.set_homogeneous(False)
        self.candidateButtons = []
        for candidate in sorted(self.office.candidateList):
            if candidate.office != self.office.officeName:
                print("Office mismatch. Skipping candidate", candidate.name)
                continue

            pictureFilename = PurePath(picturesFolder, getCandidateImageFilename(candidate, pictureFormat))
            candidateButton = CandidateToggleButton(candidate, pictureFilename)
            candidateButton.connect("toggled", self.toggleCandidate)
            self.candidateButtons.append(candidateButton)
            candidateButtonsBox.add(candidateButton)#pack_start(candidateButton, False, False, 0)

        candidateButtonsBox.set_property("margin", 9)
        candidateButtonsBox.set_halign(Gtk.Align.START)
        candidateButtonsBox.set_valign(Gtk.Align.START)

        officeLabel = Gtk.Label()
        officeLabel.set_markup("<b>" + self.office.officeName + "</b>")
        officeLabel.set_max_width_chars(25)
        officeLabel.set_ellipsize(3)     #Pango.EllipsizeMode.END
        officeLabel.set_line_wrap(True)
        officeLabel.set_tooltip_text(self.office.officeName)

        frame = Gtk.Frame()
        frame.add(candidateButtonsBox)
        frame.set_label_widget(officeLabel)
        frame.set_label_align(0.5, 0.5)

        self.add(frame)

    def toggleCandidate(self, clickedButton):
        candidate = clickedButton.candidate
        state = clickedButton.props.active
        otherCandidateButtons = [candidateButton for candidateButton in self.candidateButtons if candidateButton != clickedButton]
        if state:
            for candidateButton in otherCandidateButtons:
                candidateButton.set_active(False)
            self.office.voteCandidate(candidate)
        else:
            self.office.voteCandidate(None)

class AVSInfoEntry(Gtk.Entry):
    def __init__(self, entries, *args, placeholder_text=None, match_func=None, **kwargs):
        Gtk.Entry.__init__(self, *args, **kwargs)
        if entries is None:
            entries = []

        entriesModel = Gtk.ListStore(str)
        entries.sort()
        for entry in entries:
            entriesModel.append((entry, ))

        completion = Gtk.EntryCompletion()
        completion.set_text_column(0)
        completion.set_minimum_key_length(0)
        completion.set_model(entriesModel)
        if match_func is not None:
            completion.set_match_func(match_func)

        self.set_placeholder_text(placeholder_text)
        self.set_completion(completion)
################

class AVSVoterInfoScreen(Gtk.Box):
    def __init__(self, mainAVS, orderedStack, *args, **kwargs):
        Gtk.Box.__init__(self, *args, **kwargs)
        names, grades, sections = (None, None ,None)
        masterDB = mainAVS.masterDB
        names = masterDB.getAllNames()
        grades = masterDB.getAllGrades()
        sections = masterDB.getAllSections()

        header = Gtk.Label()
        header.set_markup("Voter Info")

        self.nameEntry = AVSInfoEntry(names, placeholder_text="Type Surname, Firstname. M.I.", match_func=self.isInName)
        self.gradeEntry = AVSInfoEntry(grades, placeholder_text="Grade")       #make this only accept ints
        self.sectionEntry = AVSInfoEntry(sections, placeholder_text="Section")
        self.submitButton = Gtk.Button(label="Vote")

        #Make the placeholder text center
        self.gradeEntry.set_max_length(2)
        self.submitButton.connect("clicked", self.submitInfo, mainAVS, orderedStack)
        #self.nameEntry.connect("changed", self.updateOtherEntries, mainAVS.masterDB) #<<Uncomment to enable automatic grade and section deduction based on name

        entriesBox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        entriesBox.pack_start(self.nameEntry, False, False, 10)
        entriesBox.pack_start(self.gradeEntry, False, False, 10)
        entriesBox.pack_start(self.sectionEntry, False, False, 10)

        mainBox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        mainBox.pack_start(header, True, True, 0)
        mainBox.pack_start(entriesBox, False, False, 0)
        mainBox.pack_start(self.submitButton, False, False, 0)
        self.submitButton.set_margin_top(50)

        frame = Gtk.Frame()
        frame.set_label("")
        frame.get_label_widget().set_markup("<big><b>SSG Election " + str(datetime.now().year) + "</b></big>")
        frame.set_label_align(0.5, 0.5)
        #frame.set_shadow_type(Gtk.ShadowType.IN)

        frame.add(mainBox)
        mainBox.set_margin_top(10)
        mainBox.set_margin_bottom(20)
        mainBox.set_margin_left(10)
        mainBox.set_margin_right(10)
        mainBox = frame
        ####


        #Centering for mainBox
        vbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=0, hexpand=True)
        vbox.pack_start(mainBox, False, False, 0)
        box = Gtk.Box(vexpand=True)
        box.pack_start(vbox, False, False, 0)
        vbox2 = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, hexpand=True)
        vbox2.pack_start(box, False, False, 0)

        screen = self.get_toplevel().get_screen()
        screen_width = screen.get_width()
        screen_height = screen.get_height()

        bg = loadGtkImage("background.png", screen_width, screen_height)
        logo = loadGtkImage("logo.png", screen_height/3.5, screen_height/3.5)

        #Centering for logo
        vbox3 = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        vbox3.pack_start(logo, False, False, 0)
        hbox = Gtk.Box()
        hbox.pack_end(vbox3, True, False, 0)

        overlay = Gtk.Overlay()
        overlay.add_overlay(bg)
        overlay.add_overlay(hbox)
        overlay.add_overlay(loadGtkImage("visibility.png"))
        overlay.add_overlay(vbox2)

        self.pack_start(overlay, True, True, 0)

    def run(self):
        pass

    def reset(self):
        self.nameEntry.set_text("")
        self.gradeEntry.set_text("")
        self.sectionEntry.set_text("")

    def isInName(self, completion, typedName, nameIter, *user_data):        #maybe move this somewhere more general
        typedName = typedName.lower()
        name = completion.get_model().get_value(nameIter, 0).lower()
        #if ñ not in the typedName, convert all ñ in the completion name to n
        name = name.replace("ñ", "n") if "ñ" not in typedName else name
        if typedName in name:
            return True     #Exact match
        else:
            for part in typedName.split():
                if part.strip(",") not in name:
                    return False
            else:
                return True     #Names match, just in different order

    def updateOtherEntries(self, nameEntry, masterDB):
        if masterDB is None:
            return
        student = nameEntry.get_text().strip()
        grade, section = masterDB.getStudentInfo(student)

        if grade and section:
            self.gradeEntry.set_text(grade)
            self.sectionEntry.set_text(section)

    def submitInfo(self, submitButton, mainAVS, orderedStack):
        main_GUI = self.get_toplevel()
        #?The title and strip calls here limit the valid name, grade, and section?
        name = self.nameEntry.get_text().strip()
        grade = self.gradeEntry.get_text().strip()
        section = self.sectionEntry.get_text().strip().title()

        for field in (name, grade, section):
            if not field:
                errorDialog = Gtk.MessageDialog(parent=main_GUI,
                    message_type=Gtk.MessageType.ERROR,
                    buttons=Gtk.ButtonsType.OK,
                    text="Please fill in all fields")

                response = errorDialog.run()
                errorDialog.destroy()
                return

        ballot = None
        try:
            ballot = mainAVS.newVoterBallot(name, grade, section)
        except AVSDB.AVSDBErrors.AVSError as error:       #Move to handleError for reuse?
            if error.policy == AVSDB.AVSExceptionHandler.POLICY_ASK:
                ballot = main_GUI.askMainServer(mainAVS, error, name, grade, section)
                if ballot is not None:
                    orderedStack.next(ballot, mainAVS, orderedStack, reset=True)
                    return

                errorMessage = "The server denied reentry."
            else:
                errorMessage = error.message
                if isinstance(error, AVSDB.AVSDBErrors.VoterNotInDatabaseError):
                    errorMessage += "\nMake sure to select all fields from the dropdown list."

            errorDialog = Gtk.MessageDialog(parent=main_GUI,
                message_type=Gtk.MessageType.ERROR,
                buttons=Gtk.ButtonsType.OK,
                text=errorMessage)

            response = errorDialog.run()
            errorDialog.destroy()
        else:
            orderedStack.next(ballot, mainAVS, orderedStack, reset=True)

class AVSMainVotingScreen(Gtk.Box):
    def __init__(self, config, *args, **kwargs):
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.VERTICAL)
        self.config = config

    def run(self, ballot, mainAVS, orderedStack):
        self.ballot = ballot    #Should the ballot even be owned? The only addition to accessible data is the voter's info
        mainBox = Gtk.FlowBox()
        mainBox.set_orientation(Gtk.Orientation.VERTICAL)
        mainBox.set_min_children_per_line(3)
        mainBox.set_max_children_per_line(self.config["maxOfficesPerColumn"])
        mainBox.set_selection_mode(Gtk.SelectionMode.NONE)
        mainBox.set_column_spacing(self.config["officeBoxSpacing"])

        offices = ballot.offices
        for office in offices:
            officeClass = getattr(self.ballot, office)
            if len(officeClass.candidateList) < 1:
                continue

            officeBox = OfficeBox(officeClass, self.config["assetsFolder"], self.config["pictureFormat"])
            box = Gtk.Box()
            box.pack_start(officeBox, True, False, 0)
            box.set_valign(Gtk.Align.START)
            mainBox.add(box)

        hbox = Gtk.Box()
        backButton = Gtk.Button(label="Back")
        backButton.connect("clicked", self.goBack, orderedStack)
        submitButton = Gtk.Button(label="Continue")
        submitButton.connect("clicked", self.submitBallot, self.ballot, mainAVS, orderedStack)
        hbox.pack_start(backButton, False, False, 0)
        hbox.pack_start(submitButton, False, False, 10)

        box = Gtk.Box()
        box.pack_start(hbox, True, False, 0)

        frame = Gtk.Frame()
        vbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        vbox.pack_start(mainBox, True, True, 0)
        vbox.pack_end(box, False, False, 20)
        frame.add(vbox)
        scroll = Gtk.ScrolledWindow()
        scroll.add(frame)
        scroll.set_overlay_scrolling(False)

        overlay = Gtk.Overlay()
        logo = loadGtkImage("logo.png", 250, 250)

        #Centering for logo
        vbox3 = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        vbox3.pack_end(logo, False, False, 0)
        hbox = Gtk.Box()
        hbox.pack_end(vbox3, False, False, 0)

        overlay.add_overlay(hbox)
        overlay.add_overlay(scroll)

        self.pack_start(overlay, True, True, 0)
        self.show_all()


    def reset(self):
        for child in self.get_children():
            self.remove(child)

        self.ballot = None

    def goBack(self, backButton, orderedStack):
        confirmationDialog = Gtk.MessageDialog(parent=self.get_toplevel(),
            message_type=Gtk.MessageType.QUESTION,
            buttons=Gtk.ButtonsType.OK_CANCEL,
            text="Are you sure you want to discard this ballot?")

        response = confirmationDialog.run()
        confirmationDialog.destroy()
        if response == Gtk.ResponseType.OK:
            orderedStack.previous(reset=True)

    def submitBallot(self, submitButton, ballot, mainAVS, orderedStack):
        orderedStack.next(ballot, mainAVS, orderedStack, reset=True)

class AVSConfirmationScreen(Gtk.Box):
    def __init__(self, config, *args, **kwargs):
        Gtk.Box.__init__(self, *args, orientation=Gtk.Orientation.VERTICAL, **kwargs)
        self.config = config

    def run(self, ballot, mainAVS, orderedStack):
        mainBox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        candidatesBox = Gtk.FlowBox()
        candidatesBox.set_orientation(Gtk.Orientation.VERTICAL)
        candidatesBox.set_min_children_per_line(1)
        candidatesBox.set_max_children_per_line(3)
        candidatesBox.set_selection_mode(Gtk.SelectionMode.NONE)
        candidatesBox.set_column_spacing(20)

        if self.config["legacyMode"]:
            candidatesBox.set_min_children_per_line(8)
            candidatesBox.set_max_children_per_line(12)
            candidatesBox.set_selection_mode(Gtk.SelectionMode.NONE)
            candidatesBox.set_column_spacing(20)
            candidatesBox.set_homogeneous(False)


        votedCandidates = []
        abstainedOffices = []
        pictureSize = self.get_screen().get_height() / 5
        for office in ballot.offices:       #reused code here so maybe make it modular?
            officeClass = getattr(ballot, office)
            candidate = officeClass.votedCandidate

            if candidate == None:       #add "You abstained from voting in:" instead
                abstainedOffices.append(office)
                continue
            elif self.config["legacyMode"]:
                votedCandidates.append(Gtk.Label(label=office + ": " + candidate.name))
            else:
                picture_filename = PurePath(self.config["assetsFolder"], getCandidateImageFilename(candidate, self.config["pictureFormat"]))
                portrait = CandidatePortrait(candidate, picture_filename, pictureSize, pictureSize, picture_identifier="summary")
                officeLabel = Gtk.Label(label=office)
                officeLabel.set_max_width_chars(25)
                officeLabel.set_ellipsize(3)
                portrait.pack_start(officeLabel, False, False, 0)
                votedCandidates.append(portrait)

        if len(votedCandidates) == 0:
            notice = Gtk.Label()
            notice.set_markup("<big><b>It seems like You didn't vote for anyone!</b></big>")
            mainBox.pack_start(notice, True, False, 0)
        else:
            mainBox.pack_start(Gtk.Label(label="In summary, here are the candidates you have chosen:"), False, False, 0)
            for candidate in votedCandidates:
                box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
                box.pack_start(candidate, False, False, 0)
                candidatesBox.add(box)

            if len(abstainedOffices) > 0:
                abstainText = "You didn't vote a candidate\nfor the following offices:\n"
                abstainText += "\n".join("<b>" + office+ "</b>" for office in abstainedOffices)
                abstainLabel = Gtk.Label()
                abstainLabel.set_markup(abstainText)
                #abstainLabel.set_justify(Gtk.Justification.CENTER)
                box = Gtk.Box()
                box.pack_start(abstainLabel, True, False, 0)

                if self.config["legacyMode"]:
                    candidatesBox.add(box)
                else:
                    scroll = Gtk.ScrolledWindow()
                    scroll.add(box)
                    candidatesBox.add(scroll)

            frame = Gtk.Frame()
            frame.add(candidatesBox)
            scroll = Gtk.ScrolledWindow()
            scroll.add(frame)
            scroll.set_overlay_scrolling(False)
            mainBox.pack_start(scroll, True, True, 0)
            #candidatesBox.set_margin_top(50)

        backButton = Gtk.Button(label="Back")
        backButton.connect("clicked", self.goBack, orderedStack)
        submitButton = Gtk.Button(label="Submit")
        submitButton.connect("clicked", self.submitBallot, ballot, mainAVS, orderedStack)
        box = Gtk.Box()
        hbox = Gtk.Box()
        hbox.pack_start(backButton, False, False, 0)
        hbox.pack_start(submitButton, False, False, 10)
        box.pack_start(hbox, True, False, 0)
        mainBox.pack_end(box, False, False, 20)

        overlay = Gtk.Overlay()
        logo = loadGtkImage("logo.png", 250, 250, "summary")

        #Centering for logo
        vbox3 = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        vbox3.pack_end(logo, False, False, 0)
        hbox = Gtk.Box()
        hbox.pack_end(vbox3, False, False, 0)

        overlay.add_overlay(hbox)
        overlay.add_overlay(mainBox)

        box = Gtk.Box()
        box.pack_start(overlay, True, True, 0)
        self.pack_start(box, True, True, 0)
        self.show_all()

    def submitBallot(self, submitButton, ballot, mainAVS, orderedStack):
        orderedStack.next(ballot, mainAVS, orderedStack, run=True, reset=True)        #only ballot should be in here but meh

    def reset(self):
        for child in self.get_children():
            self.remove(child)

    def goBack(self, backButton, orderedStack):
        orderedStack.previous(run=False)


class AVSBallotSubmitScreen(Gtk.Box):
    def __init__(self):
        Gtk.Box.__init__(self)
        self.show_all()

    def reset(self):
        self.show_all()

    def run(self, ballot, mainAVS, orderedStack):
        #An empty box for now
        self.show_all()

        main_GUI = self.get_toplevel()
        errorMessage = None
        try:
            mainAVS.processBallot(ballot)
        except AVSDB.AVSDBErrors.AVSError as error:
            voter = ballot.voter
            if error.policy == AVSDB.AVSExceptionHandler.POLICY_ASK:
                ballot = main_GUI.askMainServer(mainAVS, error, voter.name, voter.grade, voter.section)
                if ballot is not None:
                    mainAVS.processBallot(ballot)
                    return

                errorMessage = "The server denied the request."
            else:
                errorMessage = error.message

            errorMessage += "\n\nVotes were NOT recorded."

        infoMessage = errorMessage or "Votes recorded. Thank you for voting."
        infoDialog = Gtk.MessageDialog(parent=main_GUI,
            message_type=Gtk.MessageType.INFO,
            buttons=Gtk.ButtonsType.OK,
            text=infoMessage)

        if errorMessage is None:    #Only close the dialog automatically when there were no errors
            GObject.timeout_add(3000, infoDialog.destroy)   #Automatically close after 3s so that the confirmation screen won't be left open, possibly revealing the votes of the previous voter
        response = infoDialog.run()
        infoDialog.destroy()    #ensure that the dialog is destroyed when OK is pressed

        if errorMessage is not None:
            orderedStack.previous(ballot, mainAVS, orderedStack, run=False, reset=False)
        else:   #No errors when submitting ballot. Return to voter info screen for the next voter.
            orderedStack.go_to_page(1, reset=True)

class OrderedStackChild(Gtk.Box):
    def __init__(self, widget, *args, **kwargs):
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.VERTICAL, *args, **kwargs)
        self.wrapped_child = widget
        scroll = Gtk.ScrolledWindow()
        scroll.add(widget)
        scroll.set_overlay_scrolling(False)

        self.pack_start(scroll, True, True, 0)

    ####Forward all OrderedStack related methods
    def run(self, *data, **kwargs):
        return self.wrapped_child.run(*data, **kwargs)

    def reset(self, *data, **kwargs):
        return self.wrapped_child.reset(*data, **kwargs)

class OrderedStack(Gtk.Stack):      #cleanup the visible_child_index situation; REDESIGN
    def __init__(self, *args, **kwargs):
        Gtk.Stack.__init__(self, *args, **kwargs)
        self.children = []
        self.visible_child_index = 0

    def add_named(self, widget, name):
        widget = OrderedStackChild(widget)
        Gtk.Stack.add_named(self, widget, name)
        self.children.append(widget)

    def set_visible_child(self, widget, *data, **kwargs):     #Also overload it's _full and _named cousins; accept transitionType param for more flexibility
        Gtk.Stack.set_visible_child(self, widget)

    def next(self, *data, run=True, reset=False, **kwargs):       #make a base function for next, previous, and go_to_page?
        self.visible_child_index += 1
        if self.visible_child_index >= len(self.children):
            print("No next children")
            return
        if reset:
            self.children[self.visible_child_index].reset()
        if run:
            self.children[self.visible_child_index].run(*data, **kwargs)
        self.set_visible_child(self.children[self.visible_child_index], *data, **kwargs)

    def previous(self, *data, run=True, reset=False, **kwargs):
        self.visible_child_index -= 1
        if self.visible_child_index < 0:
            print("No previous child")
            return
        if reset:
            self.children[self.visible_child_index].reset()
        if run:
            self.children[self.visible_child_index].run(*data, **kwargs)
        self.set_visible_child(self.children[self.visible_child_index], *data, **kwargs)

    def go_to_page(self, page, *data, run=True, reset=False, **kwargs):
        if page > len(self.children) or page < 0:
            print("Page(",  page, ")must be between 1 and", len(self.children))
            return
        page -= 1
        if reset:
            self.children[page].reset()
        if run:
            self.children[page].run(*data, **kwargs)
        self.set_visible_child(self.children[page], *data, **kwargs)
        self.visible_child_index = page

class ElectronicBallotWindow(Gtk.Window):
    def __init__(self, mainAVS, config):
        Gtk.Window.__init__(self, title="Saytek Electronic Ballot Voting System", border_width=0, deletable=False, window_position=Gtk.WindowPosition.CENTER)
        self.mainAVS = mainAVS
        self.config = config

        self.set_keep_above(True) #Window manager hint; currently only works on compliant linux window managers
        self.fullscreen()

        self.mainStack = OrderedStack()
        self.mainStack.set_transition_type(Gtk.StackTransitionType.SLIDE_LEFT_RIGHT)
        self.mainStack.set_transition_duration(1000)

        voterInfoScreen = AVSVoterInfoScreen(self.mainAVS, self.mainStack)
        mainVotingScreen = (AVSLegacyMainVotingScreen if config["legacyMode"] else AVSMainVotingScreen)(self.config)
        confirmationScreen = AVSConfirmationScreen(self.config)

        self.mainStack.add_named(voterInfoScreen, "Voter Info")
        self.mainStack.add_named(mainVotingScreen, "Main Ballot")
        if config["includeSummary"]:
            self.mainStack.add_named(confirmationScreen, "Confirmation Screen")
        self.mainStack.add_named(AVSBallotSubmitScreen(), "Ballot Submit Screen")
        self.mainStack.set_homogeneous(True)

        self.add(self.mainStack)

    ####These are to be used by the class itself, or by its children
    def askMainServer(self, mainAVS, error, name, grade, section):
        def reissue_ballot_dialog(error, voter):
            dialogWindow = Gtk.MessageDialog(parent=self,
                modal=True, destroy_with_parent=True,
                message_type=Gtk.MessageType.QUESTION,
                buttons=Gtk.ButtonsType.OK_CANCEL,
                text=error.message + "\n" + "Enter the COMELEC password:" + "\n" + "(Voters, ASK for ASSISTANCE)")

            dialogWindow.set_title("Voter already Registered.")

            dialogBox = dialogWindow.get_content_area()
            secondary_message_label = dialogWindow.get_message_area().get_children()[0]      #No API to get the label for the secondary message
            secondary_message_label.set_justify(Gtk.Justification.CENTER)
            passwordEntry = Gtk.Entry()
            passwordEntry.set_visibility(False)
            passwordEntry.set_invisible_char("*")
            passwordEntry.set_size_request(250,0)
            dialogBox.pack_end(passwordEntry, False, False, 0)

            dialogWindow.show_all()
            response = dialogWindow.run()
            text = passwordEntry.get_text()
            dialogWindow.destroy()
            if (response == Gtk.ResponseType.OK):
                return text
            else:
                return ""

        voter = AVSDB.AVSPrimitives.Voter(name, grade, section)
        ballot = mainAVS.ask_error_handling(error, voter, ui_callback=reissue_ballot_dialog)

        return ballot

if __name__ == "__main__":
    try:
        import json
        import argparse
        from Core.AVS_Misc_Utilities import getOfficeCandidates, getPartyKeys, getNonPartyKeys, parseMasterList, getConfig, getCandidates
        parser = argparse.ArgumentParser(description="Make an instance of the Electric Ballot",prog="EAVS")
        parser.add_argument("candidates", default="candidates.json", nargs="?", help="a JSON file that holds the candidate list for the election(default:candidates.json)")
        parser.add_argument("config", default="config.json", nargs='?', help="a JSON file used as config for the program. See Documentation for details(default:config.json)")
        args = parser.parse_args()

        config = getConfig(args.config)
        ui_config= config["ui_config"]
        offices = config["offices"]
        pre_election = config["server_config"]["pre_election"]
        password_hash = config["server_config"]["password_hash"]
        candidates = getCandidates(args.candidates, offices)
        officeCandidatesMaster = getOfficeCandidates(candidates)
        dbConfig = config["db_config"]

        ####End of Config Setup###########################################################################
        masterCandidateList = parseMasterList(offices, officeCandidatesMaster)
        AVSexception_handler = AVSDB.AVSExceptionHandler({AVSDB.AVSDBErrors.HasAlreadyRegisteredError: AVSDB.AVSExceptionHandler.POLICY_ASK})
        #Preload images on another thread
        preloadImagesThread = threading.Thread(target=
            lambda: tuple(preloadCandidateImages(candidates, ui_config["pictureFormat"], ui_config["assetsFolder"])
                    for office, candidates in masterCandidateList.items())
                and print("Done preloading candidate images"))
        preloadImagesThread.start()

        mainAVS = AVSDB.AVSLogic(dbConfig["votes_db"], dbConfig["voter_db"], offices, masterCandidateList, AVSexception_handler, password_hash=password_hash, masterDBFile=dbConfig["master_db"], pre_election=pre_election)
        electionBallotMainWindow = ElectronicBallotWindow(mainAVS, ui_config)
        electionBallotMainWindow.show_all()
        electionBallotMainWindow.set_focus(None)
        electionBallotMainWindow.connect("delete-event", Gtk.main_quit)

        Gtk.main()
        preloadImagesThread.join()
    except Exception as error:
        errorDialog = Gtk.MessageDialog(parent=None,
        message_type=Gtk.MessageType.ERROR,
        buttons=Gtk.ButtonsType.OK,
        text="AVS Failed with the error:\n" + str(type(error))[8:-2] + ": " + str(error))

        response = errorDialog.run()
        errorDialog.destroy()
        raise error     #re-raise exception so that the traceback is also printed to the terminal
