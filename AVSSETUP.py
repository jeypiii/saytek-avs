import hashlib
import json
from pathlib import PurePath
import os

import gi
gi.require_version("Gtk", "3.0")
gi.require_version("GtkSource", "3.0")
from gi.repository import Gtk, Gdk, GdkPixbuf, GtkSource

from Core.AVSDB import MasterDB, VoterDB, VotesDB
from Helpers.get_election_results import consolidateVoterDB, consolidateVotesDB, getElectionResults, getNumExpectedVoters, getVotesBreakdown, saveResults, saveResultsToExcel, getNumElectionVoters


try:
    from Core.AVS_Misc_Utilities import parseMasterList, getCandidateImageFilename, getCandidates, getConfig
    from Core.AVSPrimitives import Candidate
    from Helpers.make_masterList import parseVotersPerSectionFile, make_masterList
except ImportError:
    print("Modules not found lul")
#####################Placeholder
    class Candidate(object):
        def __init__(self, name, office, party):
            self.name = name
            self.office = office
            self.party = party
            self.identifier = ""

        def isIndependent(self):
            return self.party.startswith["Independent #"] and self.party[13:].isnumeric()

    def parseMasterList(allowed_offices, candidates):
        out = []
        independent_counter = 1
        for party, offices in candidates.items():
            for office, candidates in offices.items():
                if party == "Independent":
                    for candidate in candidates:
                        out.append(Candidate(candidate, office, str(independent_counter)))
                        independent_counter += 1
                    continue

                out.append(Candidate(candidates, office, party))

        return out

    def getCandidateImageFilename(candidate, pictureFormat=".jpg"):
        output = ""
        for char in candidate.name + "[" + candidate.party + "." + candidate.office + "]":
            if char not in "?*/\|:<>\\\"'":
                output += char

        return output
    ####################

class CandidatesConfigWidget(Gtk.Box):
    #These should be in an enum, but this will do for now
    TYPES = (int, str, str, GdkPixbuf.Pixbuf, GdkPixbuf.Pixbuf, str, str)
    NUM, OFFICE, NAME, PICTURE_PREVIEW, PICTURE, SORTKEY, TOOLTIPTEXT = tuple(range(len(TYPES)))

    def __init__(self, party, offices):
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.VERTICAL)
        self.party = party
        self.offices = offices
        self.numCandidates = {}
        self.previousImageDirectoryURI = "." #For GUIselectCandidatePicture
        self.numHasPicture = 0

        self.officesModel = Gtk.ListStore(str)
        self.officeCombobox = Gtk.ComboBox.new_with_model(self.officesModel)
        renderer = Gtk.CellRendererText()
        renderer.set_property("width_chars", 25)
        renderer.set_property("max_width_chars", 25)
        renderer.set_property("ellipsize", 3)   #Pango.EllipsizeMode.END
        self.officeCombobox.pack_start(renderer, True)
        self.officeCombobox.add_attribute(renderer, "text", 0)
        #TODO: add an indicator on the combobox when office already has a candidate?

        configControls = Gtk.Box()
        self.candidateNameEntry = Gtk.Entry()
        self.candidateNameEntry.set_placeholder_text("Type the name of the candidate (case sensitive)")
        addCandidateButton = Gtk.Button(label="+")
        addCandidateButton.set_tooltip_text("Add candidate")
        addCandidateButton.connect("clicked", lambda button: self.GUIaddCandidate())
        removeCandidateButton = Gtk.Button(label="-")
        removeCandidateButton.set_tooltip_text("Remove selected candidates")
        removeCandidateButton.connect("clicked", lambda button: self.GUIremoveSelectedCandidates())

        configControls.pack_start(Gtk.Label(label="Office:"), False, False, 0)
        configControls.pack_start(self.officeCombobox, False, False, 0)
        configControls.pack_start(Gtk.Label(label="Name:"), False, False, 0)
        configControls.pack_start(self.candidateNameEntry, True, True, 0)
        configControls.pack_end(removeCandidateButton, False, False, 0)
        configControls.pack_end(addCandidateButton, False, False, 5)

        ####Start of candidatesConfig
        self.candidatesModel = Gtk.ListStore(*self.TYPES)
        self.candidatesModel.set_sort_column_id(self.SORTKEY, Gtk.SortType.ASCENDING)

        candidatesConfig = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        self.candidatesTreeView  = Gtk.TreeView.new_with_model(self.candidatesModel)
        self.candidatesTreeView.set_tooltip_column(self.TOOLTIPTEXT)
        self.candidatesTreeView.set_hover_expand(True)
        self.candidatesTreeView.get_selection().set_mode(Gtk.SelectionMode.MULTIPLE)
        self.candidatesTreeView.connect("row-activated", self.GUIselectCandidatePicture)

        textColumns =  ("#", "Office", "Name")
        maxChars = (3, 25, 70)
        cellFuncs = (self.GUIupdateNums, self.GUIwarnInvalidOffice, self.GUIwarnInvalidName)
        for i in range(len(textColumns)):
            column = textColumns[i] 
            renderer = Gtk.CellRendererText()
            if i in (self.OFFICE, self.NAME):     #Only names and offices are editable
                renderer.set_property("editable", True)
                renderer.connect("edited", self.updateCandidatesModel, i)
            renderer.set_property("width_chars", maxChars[i])
            renderer.set_property("max_width_chars", maxChars[i])
            renderer.set_property("ellipsize", 3)   #Pango.EllipsizeMode.END
            treeviewColumn = Gtk.TreeViewColumn(column, renderer, text=i)
            treeviewColumn.set_resizable(True)

            treeviewColumn.set_cell_data_func(renderer, cellFuncs[i])
            if i == self.NAME:      #Make the name column take up extra space
                treeviewColumn.set_expand(True)

            self.candidatesTreeView.append_column(treeviewColumn)

        rendererImage = Gtk.CellRendererPixbuf()
        imageColumn = Gtk.TreeViewColumn("Picture", rendererImage)
        imageColumn.set_cell_data_func(rendererImage, self.GUIsetCandidatePicture)
        self.candidatesTreeView.append_column(imageColumn)

        """
        #Show these columns for debug
        textColumns =  ("_sortingKey", "tooltipText")
        maxChars = (20, 20)
        for i in (self.SORTKEY, self.TOOLTIPTEXT):
            column = textColumns[i-5] 
            renderer = Gtk.CellRendererText()
            renderer.set_property("width_chars", maxChars[i-5])
            renderer.set_property("max_width_chars", maxChars[i-5])
            renderer.set_property("ellipsize", 3)   #Pango.EllipsizeMode.END

            treeviewColumn = Gtk.TreeViewColumn(column, renderer, text=i)
            treeviewColumn.set_resizable(True)
            self.candidatesTreeView.append_column(treeviewColumn)
        """

        scroll = Gtk.ScrolledWindow()
        scroll.add(self.candidatesTreeView)
        scroll.set_overlay_scrolling(False)

        candidatesConfig.pack_start(configControls, False, False, 0)
        candidatesConfig.pack_start(scroll, True, True, 0)
        #@@@End of candidatesConfig


        self.pack_start(candidatesConfig, True, True, 0)
        self.loadOffices(offices)

    def GUIaddCandidate(self):
        activeIter = self.officeCombobox.get_active_iter()
        if activeIter is None:
            errorMessage = "Choose an office from the dropdown combobox"
            office = None
        else:
            office = self.officesModel.get_value(activeIter, 0)
            if (self.numCandidates.get(office)):   #The party already has a candidate for this party
                errorMessage = "The '" + office + "' for the party '" + self.party + "' has already been chosen."
            else:
                errorMessage = None

        if errorMessage is not None:
            errorDialog = Gtk.MessageDialog(parent=self.get_toplevel(),
                modal=True, destroy_with_parent=True,
                message_type=Gtk.MessageType.ERROR,
                buttons=Gtk.ButtonsType.OK,
                text=errorMessage)

            errorDialog.set_title("Invalid office")
            response = errorDialog.run()
            errorDialog.destroy()
            return

        self.addCandidate(self.candidateNameEntry.get_text(), office)

    def GUIremoveSelectedCandidates(self):
        selectedRows = self.candidatesTreeView.get_selection().get_selected_rows()[1]
        candidates = [self.candidatesModel[path][self.OFFICE] + ": " + self.candidatesModel[path][self.NAME] for path in selectedRows]
        if len(candidates) > 1:
            confirmationDialog = Gtk.MessageDialog(parent=self.get_toplevel(),
                modal=True, destroy_with_parent=True,
                message_type=Gtk.MessageType.QUESTION,
                buttons=Gtk.ButtonsType.OK_CANCEL,
                text="The Following candidates will be removed from the party '" + self.party + "':\n\n" + "\n".join(candidates))

            confirmationDialog.set_title("Removing candidates")
            response = confirmationDialog.run()
            confirmationDialog.destroy()
            if response != Gtk.ResponseType.OK:
                return

        for path in reversed(selectedRows):     #Reversed so that iters are not invalidated per removal
            self.removeCandidate(path)

    def GUIupdateNums(self, treeviewColumn, renderer, candidatesModel, modelIter, *data):
        path = candidatesModel.get_path(modelIter)
        renderer.set_property("text", str(path.get_indices()[self.NUM] + 1))

    def GUIwarnInvalidName(self, treeviewColumn, renderer, candidatesModel, modelIter, *data):
        name = candidatesModel.get_value(modelIter, self.NAME)
        if not name.strip():    #If the name is empty or is just whitespace
            renderer.set_property("cell-background", "#B31412")
        else:
            renderer.set_property("cell-background-set", False)

    def GUIwarnInvalidOffice(self, treeviewColumn, renderer, candidatesModel, modelIter, *data):
        office = candidatesModel.get_value(modelIter, self.OFFICE)
        if office not in self.offices:
            renderer.set_property("cell-background", "#B31412")     #Red
        elif self.numCandidates[office] > 1:
            renderer.set_property("cell-background", "#F57C00")     #Orange
        else:
            renderer.set_property("cell-background-set", False)

    def GUIsetCandidatePicture(self, treeviewColumn, rendererImage, candidatesModel, modelIter, *data):
        rendererImage.set_property("pixbuf", candidatesModel.get_value(modelIter, self.PICTURE_PREVIEW))
        if rendererImage.get_property("pixbuf") is None:
            rendererImage.set_property("cell-background", "#FFFFFF")
        else:
            rendererImage.set_property("cell-background-set", False)

    def GUIselectCandidatePicture(self, candidatesTreeview, path, column):
        if column.get_property("title") != "Picture":
            #print("Column clicked is not Picture. Skipping.")
            return

        rendererImage = column.get_cells()[0]
        fileDialog = Gtk.FileChooserDialog("Pick Image", self.get_toplevel(), Gtk.FileChooserAction.OPEN,
            ("Remove current Image", Gtk.ResponseType.NONE,
            "Cancel", Gtk.ResponseType.CANCEL, 
            "Select", Gtk.ResponseType.OK)
        )
        fileDialog.set_current_folder_uri(self.previousImageDirectoryURI)

        response = fileDialog.run()
        filename = str(PurePath(fileDialog.get_filename()))
        directoryURI = fileDialog.get_current_folder_uri()
        fileDialog.destroy()
        if response in (Gtk.ResponseType.CANCEL, Gtk.ResponseType.DELETE_EVENT):
            return

        pixbuf = None
        if response == Gtk.ResponseType.OK:     #Only prompt for invalid file when a file is actually picked
            try:
                pixbuf = GdkPixbuf.Pixbuf.new_from_file(filename)
            except:
                errorText = "The Image you selected is invalid: '" + filename + "'."
                if self.candidatesModel[path][self.PICTURE] is not None:
                    errorText += "\n\nPress OK to remove the current image."
                confirmationDialog = Gtk.MessageDialog(parent=self.get_toplevel(),
                    modal=True, destroy_with_parent=True,
                    message_type=Gtk.MessageType.WARNING,
                    buttons=Gtk.ButtonsType.OK_CANCEL,
                    text=errorText)

                confirmationDialog.set_title("Invalid Image")
                response = confirmationDialog.run()
                confirmationDialog.destroy()
                if response != Gtk.ResponseType.OK:
                    return

        previousPixbuf = self.candidatesModel[path][self.PICTURE]
        self.candidatesModel[path][self.PICTURE] = pixbuf
        if pixbuf is not None:
            pictureSize = self.get_screen().get_height() / 5.5
            pixbuf = pixbuf.scale_simple(pictureSize, pictureSize, GdkPixbuf.InterpType.HYPER)
            self.previousImageDirectoryURI = directoryURI or self.previousImageDirectoryURI

        self.candidatesModel[path][self.PICTURE_PREVIEW] = pixbuf

        #Update self.numHasPicture
        if (pixbuf is None) and (previousPixbuf is None):   #No image previously set and no image chosen
            pass
        elif pixbuf is None:                                #An image was previously set but is now removed
            self.numHasPicture -= 1
        elif previousPixbuf is None:                        #No image before and an image is now set
            self.numHasPicture += 1
        else:                                               #Image was previously set, just changed
            pass

    #def updateSortAndTooltip(self, candidatesModel, path, modelIter):
    def updateSortAndTooltip(self, path):
        office, name = (self.candidatesModel[path][column] for column in (self.OFFICE, self.NAME))
        self.candidatesModel[path][self.TOOLTIPTEXT] = self.generateTooltipText(name, office)
        self.candidatesModel[path][self.SORTKEY] = self.generateSortingKey(name, office)

    def generateSortingKey(self, name, office):
        #Candidates are sorted first by the order of their offices (as in the order they appear in self.offices), then by name
        #The raw office index can not be just prepended to the name as text is sorted naturally (e.g. "2" > "11", "22" > "10000")
        #Picking a max_num_digits (e.g. 1 -> 001), while a reasonable solution, artificially limits the possible number of offices, and an easy to implement alternative exists
        
        #map the indices to (capital) letters (1-26 -> A-Z), prepending 'Z' as the place value filler (just like 0 in decimal) for indices greater than 26
        #This results to a place value that increases from left to right, perfect for natural sorting
        #',' is used as a delimiter as it has a lower ascii representation to the letters (e.g. "A," < "AZ," as ',' < 'Z')

        if not name:    #If the name is empty, sort it just below candidates with invalid office
            name += chr(1)

        if office not in self.offices:      #Bring invalid candidates to the top so they will be easily noticed
            officeIndex = 0
            name = chr(0) + name
        else:
            officeIndex = self.offices.index(office)

        filler, end = divmod(officeIndex, 26)            
        officeSortingKey = ("Z" * filler) + chr(65 + end) + ","

        return officeSortingKey + name

    def generateTooltipText(self, name, office):
        tooltipText = ""
        #Escape special markup characters
        #This could be improved by using a list and indexing it instead of a bunch of if-elif
        for char in (name + "[" + office + "]"):
            if char == "<":
                tooltipText += "&lt;"
            elif char == ">":
                tooltipText += "&gt;"
            elif char == "&":
                tooltipText += "&amp;"
            elif char == '"':
                tooltipText += "&quot;"
            elif char == "'":
                tooltipText += "&apos;"
            else:
                tooltipText += char

        return tooltipText

    def updateCandidatesModel(self, treeview, path, text, column):
        previousText = self.candidatesModel[path][column]
        if previousText == text:      #No changes made. Do nothing
            return

        errorMessage = None
        if text.strip() == "":
            errorMessage = "Empty or whitespace-only values are invalid."

        if (column == self.OFFICE):   #When the office is edited, do some checks
            if (text not in self.offices):
                errorMessage = "The office must be in the valid offices (i.e. Those found on the dropdown combobox)."
            elif self.numCandidates[text]:    #The party already has a candidate for this party
                errorMessage = "A '" + text + "' for the party '" + self.party + "' has already been chosen."

        if errorMessage is not None:
            errorMessage += "\n\nContinue anyway?"
            errorDialog = Gtk.MessageDialog(parent=self.get_toplevel(),
                modal=True, destroy_with_parent=True,
                message_type=Gtk.MessageType.ERROR,
                buttons=Gtk.ButtonsType.OK_CANCEL,
                text=errorMessage)

            errorDialog.set_title("Invalid value")
            response = errorDialog.run()
            errorDialog.destroy()

            if response != Gtk.ResponseType.OK:            
                return

        if self.party != "Independent":
            #Update the recorded number of candidates. Decrement the previous office and increment the new office
            self.numCandidates[previousText] = self.numCandidates[previousText]-1 if previousText in self.numCandidates else 0
            self.numCandidates[text] = self.numCandidates[text]+1 if text in self.numCandidates else 0

        self.candidatesModel[path][column] = text
        if column in (self.OFFICE, self.NAME):    #Name or office (or both) changed. Update sortingKey and Tooltip text
            self.updateSortAndTooltip(path)

    def loadCandidates(self, candidates):
        #?should old candidates not in the loaded candidates be removed? 
        if self.party == "Independent":     #The candidates format for Indepent candidates is a list
            for office, candidateList in candidates.items():
                for candidate in candidateList:
                    self.addCandidate(candidate, office)
            return

        for office, candidate in candidates.items():
            self.addCandidate(candidate, office)

    def addCandidate(self, name, office):
        """
        if (name is None) or (office not in self.offices) or (office in self.full_offices):
            print("invalid candidate or office:", name, office)
            return
        """

        #Num in the model is just -1. The View/renderer is responsible for displaying the actual num
        self.candidatesModel.append( (-1, office, name, None, None,
            self.generateSortingKey(name, office), self.generateTooltipText(name, office)) )

        if office not in self.numCandidates:
            self.numCandidates[office] = 0
        elif (self.party != "Independent"):
            #When the number of candidates is greater than 1, it is interpreted as having too
            #  many candidates for that office
            #For simplicity, do not ever increment the number of candidates for the independent "party"
            #  as more than one candidate per office is allowed

            self.numCandidates[office] += 1
        
    def removeCandidate(self, path):
        if (self.party != "Independent") and (self.candidatesModel[path][self.OFFICE] in self.numCandidates):
            self.numCandidates[self.candidatesModel[path][self.OFFICE]] -= 1
        self.candidatesModel.remove(self.candidatesModel.get_iter(path))

    def getCandidates(self):
        #?Check if offices are in self.offices first?
        output = {}
        modelIter = self.candidatesModel.get_iter_first()
        while modelIter:
            office, name = (self.candidatesModel.get_value(modelIter, col) for col in [self.OFFICE, self.NAME])
            if self.party == "Independent":     #The output format for Indepent candidates is specialized
                if office not in output:
                    output[office] = []

                output[office].append(name)
            else:
                output[office] = name   #In the case of multiple candidates for an office, the last specified candidate is chosen
            modelIter = self.candidatesModel.iter_next(modelIter)

        return output

    def loadOffices(self, offices):
        previousOffices = self.offices

        self.offices = []
        self.officesModel.clear()

        for office in offices:
            self.addOffice(office)

        changedOffices = set()
        len_offices = len(self.offices)
        len_previousOffices = len(previousOffices)
        #An office is only considered unchanged when its index is the same on previousOffices and offices
        #  otherwise, the office is either removed, added or reordered (changed index/position)
        #When one of previousOffices and offices is longer, the extra offices are automaticaly considered changed

        if len_previousOffices < len_offices:
            len_min = len_previousOffices
            for i in range(len_previousOffices, len_offices):
                changedOffices.add(offices[i])
        else:
            len_min = len_offices
            for i in range(len_offices, len_previousOffices):
                changedOffices.add(previousOffices[i])        

        for i in range(len_min):
            office = self.offices[i]
            previousOffice = previousOffices[i]
            if office != previousOffice:
                changedOffices.add(office)
                changedOffices.add(previousOffice)

        modelIter = self.candidatesModel.get_iter_first()
        #Disable dynamic sorting first by setting the nums column as sort column (all values are -1)
        #Even in the chance that the sorting is not stable, at least the model is not changed
        #  while it is being looped through.
        self.candidatesModel.set_sort_column_id(self.NUM, Gtk.SortType.ASCENDING)
        _count = 0
        while modelIter:
            office = self.candidatesModel.get_value(modelIter, self.OFFICE)
            if office in changedOffices:
                self.updateSortAndTooltip(self.candidatesModel.get_path(modelIter))
                _count += 1
            modelIter = self.candidatesModel.iter_next(modelIter)
        self.candidatesModel.set_sort_column_id(self.SORTKEY, Gtk.SortType.ASCENDING)    #Go back to proper sorting
        print(" " + self.party, "Updated rows:", _count)

    def addOffice(self, office):
        self.officesModel.append((office, ))
        self.offices.append(office)
        if office not in self.numCandidates:
            self.numCandidates[office] = 0

    def getOffices(self):
        offices = []
        self.officesModel = self.officesModel or self.officesModel
        modelIter = self.officesModel.get_iter_first()
        while modelIter:
            offices.append(self.officesModel.get_value(modelIter, 0))
            modelIter = self.officesModel.iter_next(modelIter)

        if self.offices != offices:
            print("     ERROR: Model and class offices mismatch.", offices, self.offices)

        return offices

    def savePictures(self, directory, pictureFormat, filenameDict=None):
        if pictureFormat not in  ("jpeg", "png", "tiff", "ico", "bmp"):
            print("Picture format must be “jpeg”, “png”, “tiff”, “ico” or “bmp”")
            return

        #filenameDict is a key-value pair of candidate info and filename, respectively.
        #The candidate info is a tuple containing the candidate party, office, and name in that order
        if not filenameDict:    #filenameDict is empty. Generate one with the known info within the party
            filenameDict = {}
            for office, candidates in parseMasterList(self.getOffices(), {self.party: self.getCandidates()}).items():
                for candidate in candidates:
                    filenameKey = (candidate.party, candidate.office, candidate.name)
                    print("  ", filenameKey)
                    filenameDict[filenameKey] =  getCandidateImageFilename(candidate, ".jpg")

        modelIter = self.candidatesModel.get_iter_first()
        candidatesWithError = {}
        independent_counter = 1
        while modelIter:
            office, name, pixbuf = (self.candidatesModel.get_value(modelIter, col) for col in (self.OFFICE, self.NAME, self.PICTURE))
            modelIter = self.candidatesModel.iter_next(modelIter)

            filenameKey = (self.party, office, name)
            if (filenameKey not in filenameDict) and (self.party == "Independent"):
                filenameKey = ("Independent #" + str(independent_counter), office, name)

            #Since the sortingKey follows the ordering scheme in parseMasterList, the independent candidates are already in order
            #We just have to not increment the counter when we encounter invalid candidates (e.g. those with invalid offices)
            if office in self.offices:
                independent_counter += 1
            else:
                candidatesWithError[filenameKey] = "({}, {}, {})".format(*filenameKey) + ": Invalid Office"
                continue

            print("\n  ", self.party, filenameDict, "\n")
            if filenameKey not in filenameDict:
                error =  "({}, {}, {})".format(*filenameKey)
                error = ": No filename specified"
                #print(error, "Skipping.")
                #print("Generating a default filename (using getCandidateImageFilename)")
                candidatesWithError[filenameKey] = error
                continue

            filename = str(PurePath(directory, filenameDict[filenameKey]))
            if pixbuf is None:
                continue
 
            try:
                pixbuf.savev(filename, pictureFormat, [None], [])
            except Exception as e:#GLib.Error:
                candidatesWithError[filenameKey] = e.message

        #TODO: Inform user on fail or success via GUI
        print(self.party, "Errors:", candidatesWithError) if candidatesWithError else None
        return candidatesWithError

class FilePickerEntry(Gtk.Box):
    def __init__(self, fileLabel, folderMode=False, hasRelative=False):
        Gtk.Box.__init__(self)
        self.fileLabel = fileLabel

        pathControls = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        self.pathEntry = Gtk.Entry()
        self.pathEntry.set_placeholder_text("Use the file picker or type filename/directory of " + self.fileLabel)
        self.relativePathCheckButton = Gtk.CheckButton.new_with_label("Relative path")
        self.relativePathCheckButton.connect("toggled", lambda button: self.toggleRelative())
        self.relativePathCheckButton.set_active(True)
        self.relativePathCheckButton.set_halign(Gtk.Align.END)
        pathControls.pack_start(self.pathEntry, False, False, 0)
        if hasRelative:
            pathControls.pack_start(self.relativePathCheckButton, False, False, 0)

        fileChooserButton = Gtk.Button(label="Choose")
        fileChooserButton.connect("clicked", lambda button: self.chooseFile(folderMode))

        self.pack_start(pathControls, True, True, 10)
        self.pack_end(fileChooserButton, False, False, 0)

    def chooseFile(self, folderMode):
        pathDialog = Gtk.FileChooserDialog("Select " + self.fileLabel, self.get_toplevel(), 
            Gtk.FileChooserAction.SELECT_FOLDER if folderMode else Gtk.FileChooserAction.SAVE, 
            ("Cancel", Gtk.ResponseType.CANCEL,
            "Select", Gtk.ResponseType.OK) )
        pathDialog.unselect_all()

        response = pathDialog.run()
        path = pathDialog.get_filename()
        #directoryURI = directoryDialog.get_current_folder_uri()
        pathDialog.destroy()
        if response != Gtk.ResponseType.OK:
            return

        if self.relativePathCheckButton.get_active():
            path = os.path.relpath(path)

        self.pathEntry.set_text(str(PurePath(path)))

    def toggleRelative(self):
        path = self.pathEntry.get_text() or "."
        if self.relativePathCheckButton.get_active():
            path = os.path.relpath(path)
        else:
            path = os.path.abspath(path)

        self.pathEntry.set_text(str(PurePath(path)))

    def getPath(self, absolute=False):
        path = self.pathEntry.get_text()
        if not path:
            return None

        if absolute and path:
            path = os.path.abspath(path)

        return path

    def setPath(self, path):
        if path:
            path = PurePath(path)
            self.relativePathCheckButton.set_active(not path.is_absolute())

        self.pathEntry.set_text(str(path))

class SetupConfigWidget(Gtk.Box):
    def __init__(self): 
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.VERTICAL, spacing=10)
        self.passwordHashEntry = Gtk.Entry()
        self.passwordHashEntry.set_placeholder_text("Password HASH using SHA256 and the salt E13C7")
        setPasswordButton = Gtk.Button(label="Set Password")
        setPasswordButton.connect("clicked", self.GUIsetPassword)

        passwordConfig = Gtk.Box()
        passwordConfig.pack_start(Gtk.Label(label="Password HASH: "), False, False, 0)
        passwordConfig.pack_start(self.passwordHashEntry, True, True, 10)
        passwordConfig.pack_start(setPasswordButton, False, False, 0)
        passwordConfig.set_tooltip_text("The password HASH to use (NOT the password itself).\nUse the \"Set Password\" button to actually set a password.\nThe password is hashed using SHA256 and the salt E13C7.\nMAKE SURE TO SET A PASSWORD HASH\nSet a new and unpredictable password each election")

        self.preElectionCheckButton = Gtk.CheckButton.new_with_label("Election for the next School Year (pre-election)")
        self.preElectionCheckButton.set_active(True)
        self.preElectionCheckButton.set_tooltip_text("Pre-election: The election is held near the end of the year school year.\nOfficers for the next school year are voted for.\nGrade 7 students vote for the Grade 8 Representative, etc.")

        self.legacyModeCheckButton = Gtk.CheckButton.new_with_label("Use dropdown combobox UI (legacy mode)")
        self.legacyModeCheckButton.set_tooltip_text("Use dropdown comboboxes for the ballot instead of clickable pictures.\nThis can be useful when pictures can not be provided or there are too many candidates to fit in one screen.")

        self.includeSummaryCheckButton = Gtk.CheckButton.new_with_label("Include vote summary/final confirmation screen")
        self.includeSummaryCheckButton.set_active(True)
        self.includeSummaryCheckButton.set_tooltip_text("Include a final page that shows all the chosen candidates and the offices where no candidates are chosen.\nThis should be enabled in most cases, but it can slow down voting time so it can be disabled if the election needs to be quicker.")

        pictureFormatConfig = Gtk.Box()
        self.PICFORMAT, self.FORMATNAME, self.EXTENSION = range(3)
        self.pictureFormatsModel = Gtk.ListStore(str, str, str)
        picformats = (
            ("jpeg", ".jpg"),
            ("png", ".png"),
            ("tiff", ".tiff"),
            ("ico", ".ico"),
            ("bmp", ".bmp")
        )
        for picformat, extension in picformats:
            formatText = picformat + " (" + extension + ")"
            self.pictureFormatsModel.append( (formatText, picformat, extension) )

        self.pictureFormatsCombobox = Gtk.ComboBox.new_with_model(self.pictureFormatsModel)
        renderer = Gtk.CellRendererText()
        renderer.set_property("width_chars", 15)
        renderer.set_property("max_width_chars", 15)
        renderer.set_property("ellipsize", 3)   #Pango.EllipsizeMode.END
        self.pictureFormatsCombobox.pack_start(renderer, True)
        self.pictureFormatsCombobox.add_attribute(renderer, "text", 0)
        self.pictureFormatsCombobox.set_id_column(self.PICFORMAT)
        self.pictureFormatsCombobox.set_active(0)   #Choose jpeg as default 
        pictureFormatConfig.pack_start(Gtk.Label(label="Picture Format:"), False, False, 0)
        pictureFormatConfig.pack_start(self.pictureFormatsCombobox, False, False, 10)
        pictureFormatConfig.set_tooltip_text("The picture format of the candidate images to be loaded.\nThe format chosen here is also used for saving the images chosen in the <Candidates> Tab.")

        picturesFolderConfig = Gtk.Box()
        self.picturesFolderPicker = FilePickerEntry("Pictures Folder", folderMode=True, hasRelative=True)
        self.picturesFolderPicker.setPath("Candidate Pictures")
        picturesFolderConfig.pack_start(Gtk.Label(label="Pictures Folder:", valign=Gtk.Align.START), False, False, 0)
        picturesFolderConfig.pack_end(self.picturesFolderPicker, True, True, 0)
        picturesFolderConfig.set_tooltip_text("The Folder where images are to be loaded from.")

        votesDBConfig = Gtk.Box()
        self.votesDBPicker = FilePickerEntry("Votes Database", hasRelative=True)
        self.votesDBPicker.setPath("votesDB.db")
        votesDBConfig.pack_start(Gtk.Label(label="Votes Database:", valign=Gtk.Align.START), False, False, 0)
        votesDBConfig.pack_end(self.votesDBPicker, True, True, 0)
        votesDBConfig.set_tooltip_text("The database where votes are recorded.\nIf the database does not yet exist, the AVS will \ngenerate one at first run using the filename set here.\nMake sure it is DELETED before every NEW election.")

        voterDBConfig = Gtk.Box()
        self.voterDBPicker = FilePickerEntry("Voter Database", hasRelative=True)
        self.voterDBPicker.setPath("voterDB.db")
        voterDBConfig.pack_start(Gtk.Label(label="Voter Database:", valign=Gtk.Align.START), False, False, 0)
        voterDBConfig.pack_end(self.voterDBPicker, True, True, 0)
        voterDBConfig.set_tooltip_text("The database containing registration and ballot submission time.\nMake sure it is DELETED before every NEW election.")

        masterDBConfig = Gtk.Box()
        self.masterDBPicker = FilePickerEntry("Student Database", hasRelative=True)
        self.masterDBPicker.setPath("masterDB.db")
        masterDBConfig.pack_start(Gtk.Label(label="Student Database:", valign=Gtk.Align.START), False, False, 0)
        masterDBConfig.pack_end(self.masterDBPicker, True, True, 0)
        masterDBConfig.set_tooltip_text("The database containing eligible voters.\nSet to empty if not needed (NO VOTER VERIFICATION)\nMake sure it is DELETED before every NEW election.\nOne can be generated using the <Master list> Tab.")

        db_config = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        db_config.pack_start(votesDBConfig, False, False, 0)
        db_config.pack_start(voterDBConfig, False, False, 0)
        db_config.pack_start(masterDBConfig, False, False, 0)

        ui_config = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        ui_config.pack_start(picturesFolderConfig, False, False, 0)
        ui_config.pack_start(pictureFormatConfig, False, False, 0)
        ui_config.pack_start(self.legacyModeCheckButton, False, False, 0)
        ui_config.pack_start(self.includeSummaryCheckButton, False, False, 0)

        server_config = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        server_config.pack_start(passwordConfig, False, False, 0)
        server_config.pack_start(self.preElectionCheckButton , False, False, 0)


        for label, config in (("AVS Server" ,server_config), ("Database", db_config), ("UI", ui_config)):
            frame = Gtk.Frame()
            frame.add(config)
            frame.set_label(label)
            frame.set_label_align(0.05, 0.5)

            self.pack_start(frame, False, False, 0)

    def GUIsetPassword(self, button):
        passwordDialog = Gtk.MessageDialog(parent=self.get_toplevel(),
            modal=True, destroy_with_parent=True,
            message_type=Gtk.MessageType.QUESTION,
            buttons=Gtk.ButtonsType.OK_CANCEL,
            text="Enter new COMELEC password:\n(Make sure to REMEMBER it as only the hash will be saved)")

        passwordDialog.set_title("Set COMELEC Pasword")

        dialogBox = passwordDialog.get_content_area()
        messageLabel = passwordDialog.get_message_area().get_children()[0]      #No API to get the label for the secondary message
        messageLabel.set_justify(Gtk.Justification.CENTER)
        passwordEntry = Gtk.Entry()
        passwordEntry.set_size_request(250,0)
        dialogBox.pack_end(Gtk.Label(label="The hash produced is a sha256 sum with E13C7 as salt."), False, False, 0)
        dialogBox.pack_end(passwordEntry, False, False, 0)

        passwordDialog.show_all()
        response = passwordDialog.run()
        password = passwordEntry.get_text()
        passwordDialog.destroy()
        if response != Gtk.ResponseType.OK:
            return

        passwordHash = hashlib.sha256("E13C7".encode("utf8") + password.encode("utf8")).hexdigest()
        self.passwordHashEntry.set_text(passwordHash)

    def getPictureFormat(self):
        modelIter = self.pictureFormatsCombobox.get_active_iter()
        if modelIter is None:
            return ("", "")

        formatExtension, formatName = (self.pictureFormatsModel.get_value(modelIter, col) for col in (self.EXTENSION, self.FORMATNAME))

        return (formatExtension, formatName)

    def getMasterDBFilename(self, absolute=True):
        return self.masterDBPicker.getPath(absolute)

    def getPicturesFolder(self, absolute=True):
        return self.picturesFolderPicker.getPath(absolute)

    def loadConfig(self, config):
        ui_config = config["ui_config"]
        assetsFolder = ui_config["assetsFolder"]
        pictureFormat = ui_config["pictureFormat"]
        legacyMode = ui_config["legacyMode"]
        includeSummary = ui_config["includeSummary"]

        server_config = config["server_config"]
        passwordHash = server_config["password_hash"]
        preElection = server_config["pre_election"]

        self.picturesFolderPicker.setPath(assetsFolder)
        self.legacyModeCheckButton.set_active(legacyMode)
        self.includeSummaryCheckButton.set_active(includeSummary)

        self.pictureFormatsCombobox.set_active_iter(None)
        formatIter = self.pictureFormatsModel.get_iter_first()
        while formatIter:
            if pictureFormat == self.pictureFormatsModel.get_value(formatIter, self.EXTENSION):
                self.pictureFormatsCombobox.set_active_iter(formatIter)
                break
            formatIter = self.pictureFormatsModel.iter_next(formatIter)

        self.passwordHashEntry.set_text(passwordHash)
        self.preElectionCheckButton.set_active(preElection)

        db_config = config["db_config"]
        self.votesDBPicker.setPath(db_config["votes_db"])
        self.voterDBPicker.setPath(db_config["voter_db"])
        self.masterDBPicker.setPath(db_config["master_db"])

    def getConfig(self):
        #ui_config
        assetsFolder = self.picturesFolderPicker.getPath()
        pictureFormat = self.pictureFormatsModel.get_value(self.pictureFormatsCombobox.get_active_iter(), self.EXTENSION)
        legacyMode = self.legacyModeCheckButton.get_active()
        includeSummary = self.includeSummaryCheckButton.get_active()

        #server_config
        passwordHash = self.passwordHashEntry.get_text()
        preElection = self.preElectionCheckButton.get_active()

        ui_config = {
            "assetsFolder": assetsFolder,
            "pictureFormat": pictureFormat,
            "officeBoxSpacing": 30,
            "maxOfficesPerColumn": 12,
            "legacyMode": legacyMode,
            "includeSummary": includeSummary
        }

        db_config = {
            "votes_db": self.votesDBPicker.getPath(),
            "voter_db": self.voterDBPicker.getPath(),
            "master_db": self.masterDBPicker.getPath(),
        }

        server_config = {
        "pre_election": preElection,
        "password_hash": passwordHash,
        "serverFolder": "serverFolder/",
        "maxPCNum": 9
        }

        return {"ui_config": ui_config, "db_config": db_config, "server_config": server_config}

class MasterListConfigWidget(Gtk.Box):
    def __init__(self):
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.VERTICAL, hexpand=True, vexpand=True)
        self.sections = {}
        self.previousGrade = 7  #Keep track of the grade level of the last section added so that adding all sections for a grade is not repetitive
        self.previousDirectoryURI = "."  #Keep track of the last directory where a voters file is loaded. This is useful for when voter files are in one folder
        self.sectionsNotebook = Gtk.Notebook()
        addSectionButton = Gtk.Button(label="+")
        addSectionButton.set_tooltip_text("Add a new section")
        addSectionButton.connect("clicked", lambda button: self.GUIaddSection())
        addSectionButton.show_all()
        self.sectionsNotebook.set_action_widget(addSectionButton, Gtk.PackType.START)
        self.sectionsNotebook.set_scrollable(True)

        instructions = "AFGHGFGDSE TODO"     
        self.pack_start(Gtk.Label(label="Enter the voter names (one name per line) in each section."), False, False, 0)
        sectionsNtbk = self.sectionsNotebook
        self.pack_start(sectionsNtbk, True, True, 0)
        #self.pack_end(fileControls, False, False, 10)
        #self.set_tooltip_text(instructions)
        self.show_all()
        #self.pack_end(Gtk.Label(label="Enter the voter names (one name per line) in each section."), False, False, 0)
        self.addSection("10- Example")
        self.addVoters("10-Example", ["Juan De la Cruz", "Rizal, Jose", "Leon Arcillas", "\n", "Maria Makiling", "Silang, Gabriela", "Arlene Arcillas"])

    def GUIaddSection(self):
        sectionDialog = Gtk.MessageDialog(parent=self.get_toplevel(),
            modal=True, destroy_with_parent=True,
            message_type=Gtk.MessageType.QUESTION,
            buttons=Gtk.ButtonsType.OK_CANCEL,
            text="Enter new section:")
        sectionDialog.set_title("Adding a new section")

        dialogBox = sectionDialog.get_content_area()
        gradeInput = Gtk.Box()
        gradeEntry = Gtk.SpinButton()#None, 10.0, 0)
        gradeEntry = Gtk.SpinButton(adjustment=Gtk.Adjustment(0, 0, 100, 1, 0, 0), climb_rate=1, digits=0)
        gradeEntry.set_value(self.previousGrade)
        gradeEntry.set_numeric(True)
        gradeEntry.set_placeholder_text("Enter Grade (positive integer)")
        gradeInput.pack_start(Gtk.Label(label="Grade: "), False, False, 0)
        gradeInput.pack_start(gradeEntry, False, False, 0)

        sectionInput = Gtk.Box()
        sectionEntry = Gtk.Entry()
        sectionEntry.set_placeholder_text("Enter the section name (case sensitive)")
        sectionEntry.set_size_request(250,0)
        sectionInput.pack_start(Gtk.Label(label="Section: "), False, False, 0)
        sectionInput.pack_start(sectionEntry, False, False, 0)
        dialogBox.pack_start(gradeInput, False, False, 0)
        dialogBox.pack_start(sectionInput, False, False, 0)

        sectionDialog.show_all()
        sectionEntry.grab_focus()
        response = sectionDialog.run()
        grade = str(gradeEntry.get_value_as_int())
        section = sectionEntry.get_text().strip()
        sectionDialog.destroy()
        if response != Gtk.ResponseType.OK:
            return

        gradeAndSection = grade + "-" + section
        errorMessage = None
        if not grade.isnumeric():
            errorMessage = "Grade must be a positive integer"
        elif len(section) == 0:
            errorMessage = "The section cannot be empty"
        elif gradeAndSection in self.sections:
            errorMessage = "Section already exists."

        if errorMessage is not None:
            errorDialog = Gtk.MessageDialog(parent=self.get_toplevel(),
                modal=True, destroy_with_parent=True,
                message_type=Gtk.MessageType.ERROR,
                buttons=Gtk.ButtonsType.OK,
                text=errorMessage)

            errorDialog.set_title("Error adding section")
            response = errorDialog.run()
            errorDialog.destroy()
            return

        self.addSection(gradeAndSection)
        self.previousGrade = int(grade)

    def addSection(self, gradeAndSection):
        gradeAndSection = tuple(map(str.strip, gradeAndSection.split("-", 1)))
        gradeAndSection = gradeAndSection[0] + "-" + gradeAndSection[1]
        if gradeAndSection in self.sections:
            print("Section already exists")
            return

        gradeAndSection = gradeAndSection.split("-", 1)
        if len(gradeAndSection) < 2:
            print(gradeAndSection, "must be in the fomat {grade}-{section}")
            return

        gradeAndSection = gradeAndSection[0].strip() + "-" + gradeAndSection[1].strip()
        sectionWidget = GtkSource.View()
        sectionWidget.set_show_line_numbers(True)
        sectionWidget.show_all()
        sectionWidget.gradeAndSection = gradeAndSection

        scroll = Gtk.ScrolledWindow()
        scroll.add(sectionWidget)
        scroll.gradeAndSection = sectionWidget.gradeAndSection
        scroll.get_buffer = sectionWidget.get_buffer
        scroll.show_all()
        sectionWidget = scroll

        sectionTabLabel = Gtk.Box()
        sectionLabel = Gtk.Label(label=gradeAndSection)
        sectionLabel.set_tooltip_text(gradeAndSection)
        sectionLabel.set_width_chars(20)
        sectionLabel.set_max_width_chars(20)
        sectionLabel.set_ellipsize(3)     #Pango.EllipsizeMode.END

        editSectionButton = Gtk.Button(label="*")
        editSectionButton.set_tooltip_text("Edit section name")
        editSectionButton.connect("clicked", lambda button, section=sectionWidget: self.GUIeditSectionName(section.gradeAndSection))
        removeSectionButton = Gtk.Button(label="-")
        removeSectionButton.set_tooltip_text("Remove section")
        removeSectionButton.connect("clicked", lambda button, section=sectionWidget: self.GUIremoveSection(section.gradeAndSection))

        sectionTabLabel.pack_start(sectionLabel, True, True, 0)
        sectionTabLabel.pack_end(removeSectionButton, False, False, 0)
        sectionTabLabel.pack_end(editSectionButton, False, False, 0)
        sectionTabLabel.show_all()

        self.sectionsNotebook.append_page(scroll, sectionTabLabel)
        self.sections[gradeAndSection] = self.sectionsNotebook.page_num(scroll)
        self.sectionsNotebook.next_page()

    def GUIeditSectionName(self, gradeAndSection):
        grade, section = gradeAndSection.split("-")
        sectionDialog = Gtk.MessageDialog(parent=self.get_toplevel(),
            modal=True, destroy_with_parent=True,
            message_type=Gtk.MessageType.QUESTION,
            buttons=Gtk.ButtonsType.OK_CANCEL,
            text="Enter new section:")
        sectionDialog.set_title("Enter new section info")

        dialogBox = sectionDialog.get_content_area()
        gradeInput = Gtk.Box()
        gradeEntry = Gtk.SpinButton()#None, 10.0, 0)
        gradeEntry = Gtk.SpinButton(adjustment=Gtk.Adjustment(0, 0, 100, 1, 0, 0), climb_rate=1, digits=0)
        gradeEntry.set_numeric(True)
        gradeEntry.set_value(int(grade))
        gradeEntry.set_placeholder_text("Enter Grade (positive integer)")
        gradeInput.pack_start(Gtk.Label(label="Grade: "), False, False, 0)
        gradeInput.pack_start(gradeEntry, False, False, 0)

        sectionInput = Gtk.Box()
        sectionEntry = Gtk.Entry()
        sectionEntry.set_placeholder_text("Enter the section name (case sensitive)")
        sectionEntry.set_size_request(250,0)
        sectionEntry.set_text(section)
        sectionInput.pack_start(Gtk.Label(label="Section: "), False, False, 0)
        sectionInput.pack_start(sectionEntry, False, False, 0)
        dialogBox.pack_start(gradeInput, False, False, 0)
        dialogBox.pack_start(sectionInput, False, False, 0)

        sectionDialog.show_all()
        sectionEntry.grab_focus()
        response = sectionDialog.run()
        grade = str(gradeEntry.get_value_as_int())
        section = sectionEntry.get_text()
        sectionDialog.destroy()
        if response != Gtk.ResponseType.OK:
            return

        newName = grade + "-" + section
    #def changeSectionName(self, gradeAndSection, newName):
        if gradeAndSection == newName:
            return

        errorMessage = None
        if not grade.isnumeric():
            errorMessage = "Grade must be a positive integer"
        elif len(section) == 0:
            errorMessage = "The section cannot be empty"
        elif newName in self.sections:
            errorMessage = "Section already exists."

        if errorMessage is not None:
            errorDialog = Gtk.MessageDialog(parent=self.get_toplevel(),
                modal=True, destroy_with_parent=True,
                message_type=Gtk.MessageType.ERROR,
                buttons=Gtk.ButtonsType.OK,
                text=errorMessage)

            errorDialog.set_title("Error editing section info")
            response = errorDialog.run()
            errorDialog.destroy()
            return

        sectionWidget = self.sectionsNotebook.get_nth_page(self.sections[gradeAndSection])
        sectionWidget.gradeAndSection = newName
        sectionTabLabel = self.sectionsNotebook.get_tab_label(sectionWidget)
        sectionTabLabel = sectionTabLabel.get_children()[0]     #The label is packed first
        sectionTabLabel.set_text(newName)

        #Reassign pageNum to newName and remove old section name (dict.pop pops the value and deletes the key)
        self.sections[newName] = self.sections.pop(gradeAndSection)

    def GUIremoveSection(self, gradeAndSection):
        confirmationDialog = Gtk.MessageDialog(parent=self.get_toplevel(),
            modal=True, destroy_with_parent=True,
            message_type=Gtk.MessageType.WARNING,
            buttons=Gtk.ButtonsType.OK_CANCEL,
            text="The section '" + gradeAndSection + "' will be removed.\n\nContinue?")

        confirmationDialog.set_title("Removing section")
        response = confirmationDialog.run()
        confirmationDialog.destroy()
        if response != Gtk.ResponseType.OK:
            return

        self.removeSections([gradeAndSection])

    def removeSections(self, sections):
        pageNums = []
        for gradeAndSection in sections:
            if gradeAndSection not in self.sections:
                print(gradeAndSection, "not in the current sections")
                continue

            pageNums.append(self.sections.pop(gradeAndSection))

        #sort in reverse order so that each pageNum is not invalidated per iteration
        pageNums.sort()
        pageNums.reverse()
        for pageNum in pageNums:
            self.sectionsNotebook.remove_page(pageNum)

        #update the invalidated pageNums in self.sections
        self.updateSectionPageNums()

    def updateSectionPageNums(self, start=0):
        pageNum = start
        sectionWidget = self.sectionsNotebook.get_nth_page(pageNum)
        while sectionWidget:
            self.sections[sectionWidget.gradeAndSection] = self.sectionsNotebook.page_num(sectionWidget)
            pageNum += 1
            sectionWidget = self.sectionsNotebook.get_nth_page(pageNum)

    def addVoters(self, gradeAndSection, voters):
        gradeAndSection = tuple(map(str.strip, gradeAndSection.split("-", 1)))
        gradeAndSection = gradeAndSection[0] + "-" + gradeAndSection[1]
        if gradeAndSection not in self.sections:
            print(gradeAndSection, "not a valid section.", self.sections)
            return

        sectionWidget = self.sectionsNotebook.get_nth_page(self.sections[gradeAndSection])
        textBuffer = sectionWidget.get_buffer()
        for voter in voters:
            endIter = textBuffer.get_end_iter()
            endIter.backward_char()
            voter = voter if endIter.ends_line() else "\n" + voter
            textBuffer.insert(textBuffer.get_end_iter(), voter)

    def loadVoters(self, votersPerSection):
       for gradeAndSection, voters in sorted(votersPerSection.items()):
            if gradeAndSection not in self.sections:
                self.addSection(gradeAndSection)

            self.addVoters(gradeAndSection, voters)

    def getVoters(self, sections=None):
        sections = sections or self.sections.keys()
        masterList = {}
        for gradeAndSection, pageNum in self.sections.items():
            if gradeAndSection not in sections:
                continue

            sectionWidget = self.sectionsNotebook.get_nth_page(pageNum)
            textBuffer = sectionWidget.get_buffer()
            voters = textBuffer.get_text(textBuffer.get_start_iter(), textBuffer.get_end_iter(), False)
            voters = [voter for voter in map(str.strip, voters.splitlines()) if voter]
            masterList[gradeAndSection] = voters

        return make_masterList(masterList)

    def loadVotersFromFile(self):
        current_votersPerSection = self.getVoters(self.sections.keys())
        #Check first if there are pre-existing voters
        votersPresent = False
        for grade, sections in current_votersPerSection.items():
            if not sections:     #If there are no sections
                continue

            for section, voters in sections.items():
                if voters:
                    votersPresent = True
                    break

            if votersPresent:
                break

        if votersPresent:
            confirmationDialog = Gtk.MessageDialog(parent=self.get_toplevel(),
                modal=True, destroy_with_parent=True,
                message_type=Gtk.MessageType.QUESTION,
                buttons=Gtk.ButtonsType.YES_NO,
                text="Should the pre-existing voters and sections be deleted?")

            confirmationDialog.set_title("Loading voters: Pre-existing voters found")
            response = confirmationDialog.run()
            confirmationDialog.destroy()
            if response == Gtk.ResponseType.YES:
                self.removeSections(tuple(self.sections.keys()))

        fileDialog = Gtk.FileChooserDialog("Load candidates from file", self.get_toplevel(), 
            Gtk.FileChooserAction.OPEN, ("Cancel", Gtk.ResponseType.CANCEL,
            "Select voter file(s)/masterlist", Gtk.ResponseType.OK) )
        fileDialog.set_do_overwrite_confirmation(True)
        fileDialog.set_select_multiple(True)
        fileDialog.unselect_all()
        fileDialog.set_current_folder_uri(self.previousDirectoryURI)

        response = fileDialog.run()
        filenames = fileDialog.get_filenames()
        votersFileDirectoryURI = fileDialog.get_current_folder_uri()
        fileDialog.destroy()
        if response != Gtk.ResponseType.OK:
            return
        filenames = map(lambda filename: str(PurePath(filename)), filenames)

        errorMessages = {}
        votersPerSection = {}
        for filename in filenames:
            votersFile = None
            try:
                votersFile = open(filename)
                contents = votersFile.read()
            except Exception as error:      #Generic catch as no special handling is done, just informing via GUI
                errorMessages[filename] = str(type(error))[8:-2] + ": " + str(error)
                continue

            voters = None
            gradeAndSection = None
            errorMessage = None
            #Check first if it is a masterList JSON
            try:
                masterList = json.loads(contents)
            except ValueError:    #json.JSONDecodeError
                #Not a valid masterList. Treat as a newline-delimited list of voter names
                directory, file = os.path.split(filename)
                gradeAndSection, voters = tuple(parseVotersPerSectionFile(file, directory).items())[0]
                tuple_gradeAndSection = gradeAndSection.split("-", 1)
                print(gradeAndSection, tuple_gradeAndSection)
                if (len(tuple_gradeAndSection) < 2) or (not tuple_gradeAndSection[0].strip().isnumeric()):
                    errorMessage = "The filename must conform to the format {grade}-{section}."

                if gradeAndSection not in votersPerSection:
                    votersPerSection[gradeAndSection] = []
                votersPerSection[gradeAndSection].extend(voter.strip() for voter in voters)
            else:
                if not isinstance(masterList, dict):
                    errorMessages[filename] = "Invalid masterList. It must be a JSON of the format {Grade level : {Section : [names]}}"
                else:
                    for grade, sections in sorted(masterList.items()):
                        if not isinstance(sections, dict):
                            errorMessages[filename] = "Invalid masterList. (Section for grade" + grade + " must be in the format {Section: [names]})"
                            continue

                        for section, names in sorted(sections.items()):
                            gradeAndSection = grade.strip() + "-" + section.strip()
                            if not isinstance(names, list):
                                errorMessages[filename] = "Invalid masterList. (Names for section " + gradeAndSection + " must be a list)"
                                continue

                            voters = tuple(name.strip() for name in names)
                            if gradeAndSection not in votersPerSection:
                                votersPerSection[gradeAndSection] = []
                            votersPerSection[gradeAndSection].extend(voter.strip() for voter in voters)

        if errorMessages:   #Errors were encountered while parsing files
            errorDialog = Gtk.MessageDialog(parent=self.get_toplevel(),
                modal=True, destroy_with_parent=True,
                message_type=Gtk.MessageType.ERROR,
                buttons=Gtk.ButtonsType.OK_CANCEL,
                text="The following errors were encountered:\n " + "\n  ".join(filename + ": " + error for filename, error in errorMessages.items()) + "\n\nValid files, if any, will still be loaded.") 

            errorDialog.set_title("Failed loading files")
            response = errorDialog.run()
            errorDialog.destroy()
            if response == Gtk.ResponseType.CANCEL:
                return

        self.loadVoters(votersPerSection)
        self.previousDirectoryURI = votersFileDirectoryURI or self.previousDirectoryURI

    def GUIgenerateMasterDB(self, defaultDbFilename=None):
        #self.previousDirectoryURI is shared for now
        fileDialog = Gtk.FileChooserDialog("Generate MasterDB", self.get_toplevel(), 
            Gtk.FileChooserAction.SAVE, ("Cancel", Gtk.ResponseType.CANCEL,"OK", Gtk.ResponseType.OK) )
        fileDialog.set_do_overwrite_confirmation(True)
        fileDialog.unselect_all()
        fileDialog.set_current_folder_uri(self.previousDirectoryURI)
        fileDialog.set_current_name(os.path.relpath(defaultDbFilename))

        response = fileDialog.run()
        dbFilename = fileDialog.get_filename()
        masterDBDirectoryURI = fileDialog.get_current_folder_uri()
        fileDialog.destroy()
        if response != Gtk.ResponseType.OK:
            return

        try:
            MasterDB(dbFilename, self.getVoters())
        except Exception as error:
            errorDialog = Gtk.MessageDialog(parent=self.get_toplevel(),
                modal=True, destroy_with_parent=True,
                message_type=Gtk.MessageType.ERROR,
                buttons=Gtk.ButtonsType.OK_CANCEL,
                text="The following error was encountered when Generating the MasterDB:\n" + str(error)) 

            errorDialog.set_title("Could not Generate MasterDB")
            response = errorDialog.run()
            errorDialog.destroy()

        self.previousDirectoryURI = masterDBDirectoryURI or self.previousDirectoryURI

class ResultsDBChooser(Gtk.Box):
    def __init__(self):
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.VERTICAL)
        self.previousDirectoryURI = "."

        masterDBLabel = Gtk.Label(label="MasterDB")
        masterDBLabel.set_halign(Gtk.Align.START)
        self.masterDBPicker = FilePickerEntry("MasterDB")
        self.masterDBPicker.setPath("")
        self.masterDBPicker.set_tooltip_text("Make sure the MasterDB used here is the most up-to-date.\nTo make things easier, make voters that were not in the original MasterDB vote on the same single server so that only one MasterDB needs to be modified.")

        #VoterDB
        voterDBPicker = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        self.voterDBFilenameListModel = Gtk.ListStore(str)
        self.voterDBFilenameListWidget = Gtk.TreeView.new_with_model(self.voterDBFilenameListModel)
        header = Gtk.Box()
        header.pack_start(Gtk.Label(label="VoterDB"), False, False, 0)
        addVoterDBFileButton = Gtk.Button(label="Add VoterDB")
        addVoterDBFileButton.connect("clicked", lambda button: self.addDBFilename(self.voterDBFilenameListModel, "VoterDB"))
        removeVoterDBFileButton = Gtk.Button(label="-")
        removeVoterDBFileButton.connect("clicked", lambda button: self.removeSelectedDBFilenames(self.voterDBFilenameListWidget, self.voterDBFilenameListModel))

        header.pack_end(removeVoterDBFileButton, False, False, 10)
        header.pack_end(addVoterDBFileButton, False, False, 0)

        self.voterDBFilenameListWidget.set_reorderable(False)
        self.voterDBFilenameListWidget.set_activate_on_single_click(False)
        self.voterDBFilenameListWidget.get_selection().set_mode(Gtk.SelectionMode.MULTIPLE)
        #self.voterDBFilenameListWidget.set_tooltip_text("")

        renderer = Gtk.CellRendererText()
        renderer.set_property("width_chars", 70)
        renderer.set_property("max_width_chars", 70)
        renderer.set_property("ellipsize", 3)   #Pango.EllipsizeMode.END
        self.voterDBFilenameListWidget.append_column(Gtk.TreeViewColumn("DB Filename", renderer, text=0))
        scroll = Gtk.ScrolledWindow()
        scroll.add(self.voterDBFilenameListWidget)

        voterDBPicker.pack_start(header, False, False, 0)
        voterDBPicker.pack_start(scroll, True, True, 0)
        #@@@

        #VotesDB
        votesDBPicker = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        self.votesDBFilenameListModel = Gtk.ListStore(str)
        self.votesDBFilenameListWidget = Gtk.TreeView.new_with_model(self.votesDBFilenameListModel)
        header = Gtk.Box()
        header.pack_start(Gtk.Label(label="VotesDB"), False, False, 0)
        addVotesDBFileButton = Gtk.Button(label="Add VotesDB")
        addVotesDBFileButton.connect("clicked", lambda button: self.addDBFilename(self.votesDBFilenameListModel, "VotesDB"))
        removeVotesDBFileButton = Gtk.Button(label="-")
        removeVotesDBFileButton.connect("clicked", lambda button: self.removeSelectedDBFilenames(self.votesDBFilenameListWidget, self.votesDBFilenameListModel))

        header.pack_end(removeVotesDBFileButton, False, False, 10)
        header.pack_end(addVotesDBFileButton, False, False, 0)

        self.votesDBFilenameListWidget.set_reorderable(False)
        self.votesDBFilenameListWidget.set_activate_on_single_click(False)
        self.votesDBFilenameListWidget.get_selection().set_mode(Gtk.SelectionMode.MULTIPLE)
        #self.votesDBFilenameListWidget.set_tooltip_text("")

        renderer = Gtk.CellRendererText()
        renderer.set_property("width_chars", 70)
        renderer.set_property("max_width_chars", 70)
        renderer.set_property("ellipsize", 3)   #Pango.EllipsizeMode.END
        self.votesDBFilenameListWidget.append_column(Gtk.TreeViewColumn("DB Filename", renderer, text=0))
        scroll = Gtk.ScrolledWindow()
        scroll.add(self.votesDBFilenameListWidget)

        votesDBPicker.pack_start(header, False, False, 0)
        votesDBPicker.pack_start(scroll, True, True, 0)
        #@@@

        self.pack_start(masterDBLabel, False, False, 0)
        self.pack_start(self.masterDBPicker, False, False, 10)
        self.pack_start(voterDBPicker, True, True, 10)
        self.pack_start(votesDBPicker, True, True, 10)

    def addDBFilename(self, model, DBTypeName="DB"):
        fileDialog = Gtk.FileChooserDialog("Select " + DBTypeName + " File(s)", self.get_toplevel(),
            Gtk.FileChooserAction.OPEN, ("Cancel", Gtk.ResponseType.CANCEL,
            "Select", Gtk.ResponseType.OK) )
        fileDialog.set_select_multiple(True)
        fileDialog.unselect_all()
        fileDialog.set_current_folder_uri(self.previousDirectoryURI)

        response = fileDialog.run()
        filenames = fileDialog.get_filenames()
        votersFileDirectoryURI = fileDialog.get_current_folder_uri()
        fileDialog.destroy()
        if response != Gtk.ResponseType.OK:
            return

        for filename in filenames:
            model.append((os.path.relpath(filename), ))

    def removeSelectedDBFilenames(self, widget, model):
        selectedRows = widget.get_selection().get_selected_rows()[1]
        for path in reversed(selectedRows):     #Reversed so that iters are not invalidated per removal
            model.remove(model.get_iter(path))

    def getVoterDBFilenames(self):
        filenames = []
        modelIter = self.voterDBFilenameListModel.get_iter_first()
        while modelIter:
            filenames.append(self.voterDBFilenameListModel.get_value(modelIter, 0))
            modelIter = self.voterDBFilenameListModel.iter_next(modelIter)

        return filenames

    def getVotesDBFilenames(self):
        filenames = []
        modelIter = self.votesDBFilenameListModel.get_iter_first()
        while modelIter:
            filenames.append(self.votesDBFilenameListModel.get_value(modelIter, 0))
            modelIter = self.votesDBFilenameListModel.iter_next(modelIter)

        return filenames

    def getMasterDBFilename(self):
        return self.masterDBPicker.getPath()

class ElectionConfigWidget(Gtk.Box):
    defaultOffices = [
        "President",
        "Vice President",
        "Secretary",
        "Auditor",
        "PIO",
        "Peace Officer",
        "Treasurer",
        "Grade 8 Representative",
        "Grade 9 Representative",
        "Grade 10 Representative",
        "Grade 11 Representative",
        "Grade 12 Representative"
        ]

    def __init__(self, offices=None, label="Config"):
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.VERTICAL, border_width=10)
        self.previousConfigFile = "config.json"
        self.previousCandidatesFile = "candidates.json"

        self.configsNotebook = Gtk.Notebook()
        self.configsNotebook.set_scrollable(True)
        self.parties = {}
        offices = ElectionConfigWidget.defaultOffices if offices is None else offices
        offices = offices or []

        ####Start of officesConfig
        officeControls = Gtk.Box()
        self.officeEntry = Gtk.Entry()
        self.officeEntry.set_placeholder_text("Type name of new office (case sensitive)")
        addOfficeButton = Gtk.Button(label="+")
        addOfficeButton.set_tooltip_text("Add a new office")
        addOfficeButton.connect("clicked", lambda button: self.GUIaddOffice())
        removeOfficeButton = Gtk.Button(label="-")
        removeOfficeButton.set_tooltip_text("Remove selected offices")
        removeOfficeButton.connect("clicked", self.GUIremoveSelectedOffices)

        officeControls.pack_start(Gtk.Label(label="Office: "), False, False, 0)
        officeControls.pack_start(self.officeEntry, True, True, 20)
        officeControls.pack_end(removeOfficeButton, False, False, 0)
        officeControls.pack_end(addOfficeButton, False, False, 5)

        header = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        instructionsLabel = Gtk.Label(label="Use the buttons to add and remove offices. Drag and drop to reorder. \nDouble click to edit. Use Ctrl or Shift for multiselect.\nHover on items for more information.")
        instructionsLabel.set_justify(Gtk.Justification.CENTER)
        instructionsLabel.set_max_width_chars(10)
        instructionsLabel.set_ellipsize(3)  #Pango.EllipsizeMode.END

        header.set_vexpand(False)
        header.set_valign(Gtk.Align.START)
        header.pack_start(instructionsLabel, True, True, 0)
        header.pack_start(officeControls, False, False, 0)

        self.offices = set()    #only for easy checking of duplicate offices; the officesModel holds the actual offices
        self.officesModel = Gtk.ListStore(str)
        self.deletedViaGUI = False   #When delete is not via GUI, it is interpreted as a delete from reordering
        self.officesModel.connect("row-deleted", lambda *args, this=self: self.loadOffices() if not this.deletedViaGUI else None)

        officesConfig = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        self.officeListWidget = Gtk.TreeView.new_with_model(self.officesModel)
        self.officeListWidget.set_reorderable(True)
        self.officeListWidget.set_activate_on_single_click(False)
        self.officeListWidget.get_selection().set_mode(Gtk.SelectionMode.MULTIPLE)
        self.officeListWidget.set_tooltip_text("The order of offices here will be the order they appear in the ballot.\nAny changes made to the list are propagated to each party.\nFor Grade Representatives, make sure that the office name is \"Grade n Representative\".")

        renderer = Gtk.CellRendererText()
        renderer.set_property("editable", True)
        renderer.set_property("width_chars", 70)
        renderer.set_property("max_width_chars", 70)
        renderer.set_property("ellipsize", 3)   #Pango.EllipsizeMode.END
        renderer.connect("edited", self.updateOfficesModel)
        self.officeListWidget.append_column(Gtk.TreeViewColumn("Offices", renderer, text=0))

        scroll = Gtk.ScrolledWindow()
        scroll.add(self.officeListWidget)
        scroll.set_overlay_scrolling(False)

        officesConfig.pack_start(header, False, False, 0)
        officesConfig.pack_start(scroll, True, True, 0)
        #@@@End of officesConfig

        ####Start of setupConfig
        setupConfig = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        pane = Gtk.Paned()
        configBox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        self.setupConfigWidget = SetupConfigWidget()
        configBox.pack_start(Gtk.Label(label="After saving, don't forget to restart the AVS.\nDon't forget to delete any leftover database."), False, False, 0)
        configBox.pack_start(self.setupConfigWidget, True, True, 0)
        
        pane.pack1(officesConfig, True, True)
        pane.pack2(configBox, True, True)

        fileControls = Gtk.Box()
        loadButton = Gtk.Button(label="Load Config")
        loadButton.connect("clicked", lambda button: self.loadConfigFile())
        loadButton.set_tooltip_text("Load settings from a config file")
        saveButton = Gtk.Button(label="Save Config")
        saveButton.connect("clicked", lambda button: self.saveConfigFile())
        saveButton.set_tooltip_text("Save settings to a config file")
        fileControls.pack_start(loadButton, False, False, 10)
        fileControls.pack_start(saveButton, False, False, 0)
        fileControls.set_halign(Gtk.Align.CENTER)

        scroll = Gtk.ScrolledWindow()
        scroll.add(pane)
        setupConfig.pack_start(scroll, True, True, 0)
        setupConfig.pack_end(fileControls, False, False, 10)
        #@@@End of setupConfig

        ####Start of candidatesConfig
        candidatesConfig = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        self.partiesNotebook = Gtk.Notebook()
        addPartyButton = Gtk.Button(label="+")
        addPartyButton.set_tooltip_text("Add a new party")
        addPartyButton.connect("clicked", lambda button: self.GUIaddParty())
        addPartyButton.show_all()
        self.partiesNotebook.set_action_widget(addPartyButton, Gtk.PackType.START)
        self.partiesNotebook.set_scrollable(True)

        fileControls = Gtk.Box()
        configButtons = Gtk.Box()
        loadButton = Gtk.Button(label="Load Candidates")
        loadButton.connect("clicked", lambda button: self.loadCandidatesFile())
        loadButton.set_tooltip_text("Import candidates from file")
        saveButton = Gtk.Button(label="Save Candidates")
        saveButton.connect("clicked", lambda button: self.saveCandidatesFile())
        saveButton.set_tooltip_text("Save candidates to file")
        configButtons.pack_start(loadButton, False, False, 10)
        configButtons.pack_start(saveButton, False, False, 0)
        configButtons.set_halign(Gtk.Align.CENTER)

        savePicturesButton = Gtk.Button(label="Save Pictures")
        savePicturesButton.connect("clicked", lambda button: self.GUIsavePictures())
        savePicturesButton.set_halign(Gtk.Align.END)
        savePicturesButton.set_tooltip_text("Save images of valid candidates ")

        fileControls.set_center_widget(configButtons)
        fileControls.pack_end(savePicturesButton, False, False, 10)

        instructions = "To edit a party's name, press The * button. Double click to change names or offices.\nRED means that a candidate is invalid because of the highlighted field. ORANGE means there are too many candidates for the office."
        #candidatesConfig.pack_start(Gtk.Label("Click on any column to edit. Don't forget to reset the AVS after saving config."), False, False, 0)
        
        candidatesConfig.pack_start(Gtk.Label(label="Double click to edit name or office and to select pictures."), False, False, 0)
        candidatesConfig.pack_start(self.partiesNotebook, True, True, 0)
        candidatesConfig.pack_end(fileControls, False, False, 10)
        #candidatesConfig.pack_end(fileControls, False, False, 20)
        candidatesConfig.set_tooltip_text(instructions)
        #@@@End of candidatesConfig

        ####Start of masterListConfig
        masterListConfig = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        self.masterListConfigWidget = MasterListConfigWidget()        
        fileControls = Gtk.Box()
        loadButton = Gtk.Button(label="Load voters/masterlist")
        loadButton.connect("clicked", lambda button: self.masterListConfigWidget.loadVotersFromFile())
        loadButton.set_tooltip_text("Load voters from a file.\nThis can be a newline-delimited list of names\nor a JSON masterlist file.")
        saveButton = Gtk.Button(label="Generate MasterDB")
        saveButton.connect("clicked", lambda button: self.masterListConfigWidget.GUIgenerateMasterDB(self.setupConfigWidget.getMasterDBFilename()))
        #saveButton.connect("clicked", lambda button: print(self.masterListConfigWidget.getVoters()))
        saveButton.set_tooltip_text("Generate Student Database (MasterDB)")
        fileControls.pack_start(loadButton, True, False, 10)
        fileControls.pack_start(saveButton, True, False, 0)
        fileControls.set_halign(Gtk.Align.CENTER)

        masterListConfig.pack_start(self.masterListConfigWidget, True, True, 0)
        masterListConfig.pack_end(fileControls, False, False, 10)
        #@@@End of masterListConfig

        #####Start of resultsConfig
        resultsConfig = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        pane = Gtk.Paned()
        self.dbPicker = ResultsDBChooser()

        generateElectionSummaryButton = Gtk.Button(label="Merge Databases and generate Election Summary")
        generateElectionSummaryButton.set_hexpand(False)
        generateElectionSummaryButton.set_halign(Gtk.Align.CENTER)
        generateElectionSummaryButton.connect("clicked", lambda button: self.generateElectionSummary())

        self.resultsTextView = Gtk.TextView()
        self.resultsTextView.get_buffer().set_text("The election summary will be displayed here.\nThe summary will also be saved as summary.txt and results.xlsx (formatted).\nMerged Databases will also be saved.\nThe reported count of votes and abstains in the summary are only as accurate as the databases (MasterDB, VoterDb, VotesDB) supplied.")
        self.resultsTextView.set_editable(False)
        self.resultsTextView.set_monospace(True)
        scroll = Gtk.ScrolledWindow()
        scroll.add(self.resultsTextView)

        pane.pack1(self.dbPicker, False, False)
        pane.pack2(scroll, True, True)
        resultsConfig.pack_start(pane, True, True, 0)
        resultsConfig.pack_end(generateElectionSummaryButton, False, False, 10)
        #@@@End of resultsConfig


        #self.configsNotebook.append_page(officesConfig, Gtk.Label(label="Offices"))
        self.configsNotebook.append_page(setupConfig, Gtk.Label(label="Setup Config"))
        self.configsNotebook.append_page(candidatesConfig, Gtk.Label(label="Candidates"))
        self.configsNotebook.append_page(masterListConfig, Gtk.Label(label="Masterlist"))
        self.configsNotebook.append_page(resultsConfig, Gtk.Label(label="Results"))

        self.pack_start(self.configsNotebook, True, True, 0)
        self.loadOffices(offices)

    def GUIaddOffice(self):
        office = self.officeEntry.get_text()
        if not office.strip():      #If the office entered is empty or is just whitespace
            confirmationDialog = Gtk.MessageDialog(parent=self.get_toplevel(),
                modal=True, destroy_with_parent=True,
                message_type=Gtk.MessageType.WARNING,
                buttons=Gtk.ButtonsType.OK_CANCEL,
                text="The Office you are about is either empty or just whitespace. Add it anyway?")

            messageLabel = confirmationDialog.get_message_area().get_children()[0]      #No API to get the label for the secondary message
            messageLabel.set_justify(Gtk.Justification.CENTER)
            confirmationDialog.set_title("Empty office")
            response = confirmationDialog.run()
            confirmationDialog.destroy()
            if response != Gtk.ResponseType.OK:
                return

        if office in self.offices:
            errorDialog = Gtk.MessageDialog(parent=self.get_toplevel(),
                modal=True, destroy_with_parent=True,
                message_type=Gtk.MessageType.ERROR,
                buttons=Gtk.ButtonsType.OK,
                text="The office '" + office + "' already exists.")

            errorDialog.set_title("Invalid office")
            response = errorDialog.run()
            errorDialog.destroy()
            return

        self.addOffice(office)
        self.loadOffices()

    def addOffice(self, office):
        if office in self.offices:
            print("Office", office, "already present")
            return

        self.officesModel.append((office, ))
        self.offices.add(office)

    def updateOfficesModel(self, treeview, path, text):
        #print(" #############Upd8 offices model called")
        self.officesModel[path][0] = text
        self.loadOffices()

    def GUIremoveSelectedOffices(self, button):
        selectedRows = self.officeListWidget.get_selection().get_selected_rows()[1]
        offices = [self.officesModel[path][0] for path in selectedRows]
        if len(offices) > 1:
            confirmationDialog = Gtk.MessageDialog(parent=self.get_toplevel(),
                modal=True, destroy_with_parent=True,
                message_type=Gtk.MessageType.WARNING,
                buttons=Gtk.ButtonsType.OK_CANCEL,
                text="The Following offices will be removed:\n" + "\n".join(offices))

            messageLabel = confirmationDialog.get_message_area().get_children()[0]      #No API to get the label for the secondary message
            messageLabel.set_justify(Gtk.Justification.CENTER)
            confirmationDialog.set_title("Removing Offices")
            response = confirmationDialog.run()
            confirmationDialog.destroy()
            if response != Gtk.ResponseType.OK:
                return

        self.deletedViaGUI = True
        for path in reversed(selectedRows):     #Reversed so that iters are not invalidated per removal
            self.officesModel.remove(self.officesModel.get_iter(path))
        self.deletedViaGUI = False
 
        self.loadOffices()

    def loadOffices(self, offices=None):
        #If offices is None, the call is interpreted as a request to reload the current offices
        #This is so that basic changes such as add, delete, edit, and reorder are propagated to each party
        if offices is not None:
            self.offices = set()
            self.officesModel.clear()
            for office in offices:
                self.addOffice(office)
        else:
            offices = self.getOffices()

        for party, pageNum in self.parties.items():
            partyWidget = self.partiesNotebook.get_nth_page(pageNum)
            partyWidget.loadOffices(offices)

        #print("Main: loadOffices called", offices)
        print()

    def getOffices(self):
        offices = []
        modelIter = self.officesModel.get_iter_first()
        while modelIter:
            offices.append(self.officesModel.get_value(modelIter, 0))
            modelIter = self.officesModel.iter_next(modelIter)

        return offices

    def GUIaddParty(self):
        partyDialog = Gtk.MessageDialog(parent=self.get_toplevel(),
            modal=True, destroy_with_parent=True,
            message_type=Gtk.MessageType.QUESTION,
            buttons=Gtk.ButtonsType.OK_CANCEL,
            text="Enter the party name:")

        partyDialog.set_title("Adding a Party")
        dialogBox = partyDialog.get_content_area()
        partyEntry = Gtk.Entry()
        partyEntry.set_placeholder_text("Enter party name (case sensitive)")
        partyEntry.set_size_request(250,0)
        dialogBox.pack_start(partyEntry, False, False, 0)
        partyDialog.show_all()
        response = partyDialog.run()
        party = partyEntry.get_text()
        partyDialog.destroy()
        if response != Gtk.ResponseType.OK:
            return

        if party in self.parties:
            errorDialog = Gtk.MessageDialog(parent=self.get_toplevel(),
                modal=True, destroy_with_parent=True,
                message_type=Gtk.MessageType.ERROR,
                buttons=Gtk.ButtonsType.OK,
                text="The party '" + party + "' already exists.")

            errorDialog.set_title("Party already exists")
            response = errorDialog.run()
            errorDialog.destroy()
            return

        self.addParty(party)

    def addParty(self, party):
        if party in self.parties:
            print("Party already exists")
            return

        partyWidget = CandidatesConfigWidget(party, self.getOffices())
        partyWidget.show_all()        

        partyTabLabel = Gtk.Box()
        partyLabel = Gtk.Label(label=party)
        partyLabel.set_tooltip_text(party)
        partyLabel.set_width_chars(20)
        partyLabel.set_max_width_chars(20)
        partyLabel.set_ellipsize(3)     #Pango.EllipsizeMode.END

        editPartyButton = Gtk.Button(label="*")
        editPartyButton.set_tooltip_text("Edit party name")
        editPartyButton.connect("clicked", lambda button, party=partyWidget: self.GUIeditPartyName(partyWidget.party))
        removePartyButton = Gtk.Button(label="-")
        removePartyButton.set_tooltip_text("Remove party")
        removePartyButton.connect("clicked", lambda button, party=partyWidget: self.GUIremoveParty(partyWidget.party))

        partyTabLabel.pack_start(partyLabel, True, True, 0)
        partyTabLabel.pack_end(removePartyButton, False, False, 0)
        partyTabLabel.pack_end(editPartyButton, False, False, 0)
        partyTabLabel.show_all()

        self.partiesNotebook.append_page(partyWidget, partyTabLabel)
        self.parties[party] = self.partiesNotebook.page_num(partyWidget)
        self.partiesNotebook.next_page()

    def GUIeditPartyName(self, party):
        partyDialog = Gtk.MessageDialog(parent=self.get_toplevel(),
            modal=True, destroy_with_parent=True,
            message_type=Gtk.MessageType.QUESTION,
            buttons=Gtk.ButtonsType.OK_CANCEL,
            text="Enter the new name for the party:")

        partyDialog.set_title("Edit Party name")
        dialogBox = partyDialog.get_content_area()
        partyEntry = Gtk.Entry()
        partyEntry.set_text(party)
        partyEntry.set_size_request(250,0)
        dialogBox.pack_start(partyEntry, False, False, 0)
        partyDialog.show_all()
        response = partyDialog.run()
        newName = partyEntry.get_text()
        partyDialog.destroy()
        if response != Gtk.ResponseType.OK:
            return

    #def changePartyName(self, party, newName):
        if party == newName:
            return

        if newName in self.parties:
            errorDialog = Gtk.MessageDialog(parent=self.get_toplevel(),
                modal=True, destroy_with_parent=True,
                message_type=Gtk.MessageType.ERROR,
                buttons=Gtk.ButtonsType.OK,
                text="The party '" + newName + "' already exists.")

            errorDialog.set_title("Failed renaming party")
            response = errorDialog.run()
            errorDialog.destroy()
            return

        partyWidget = self.partiesNotebook.get_nth_page(self.parties[party])
        partyWidget.party = newName
        partyTabLabel = self.partiesNotebook.get_tab_label(partyWidget)
        partyLabel = partyTabLabel.get_children()[0]     #The label is packed first
        partyLabel.set_text(newName)

        #Reassign pageNum to newName and remove old party name (dict.pop pops the value and deletes the key)
        self.parties[newName] = self.parties.pop(party)

    def GUIremoveParty(self, party):
        confirmationDialog = Gtk.MessageDialog(parent=self.get_toplevel(),
            modal=True, destroy_with_parent=True,
            message_type=Gtk.MessageType.WARNING,
            buttons=Gtk.ButtonsType.OK_CANCEL,
            text="The party '" + party + "' will be removed.\n\nContinue?")

        confirmationDialog.set_title("Removing Party")
        response = confirmationDialog.run()
        confirmationDialog.destroy()
        if response != Gtk.ResponseType.OK:
            return

        self.removeParties([party])

    def removeParties(self, parties):
        pageNums = []
        for party in parties:
            if party not in self.parties:
                print(party, "not in the current parties")
                continue

            pageNums.append(self.parties.pop(party))

        #sort in reverse order so that each pageNum is not invalidated per iteration
        pageNums.sort()
        pageNums.reverse()
        for pageNum in pageNums:
            self.partiesNotebook.remove_page(pageNum)

        #update the invalidated pageNums in self.parties
        self.updatePartyPageNums()

    def updatePartyPageNums(self, start=0):
        pageNum = start
        partyWidget = self.partiesNotebook.get_nth_page(pageNum)
        while partyWidget:
            self.parties[partyWidget.party] = self.partiesNotebook.page_num(partyWidget)
            pageNum += 1
            partyWidget = self.partiesNotebook.get_nth_page(pageNum)

    def loadCandidates(self, candidatesPerParty):
        for party, candidates in candidatesPerParty.items():
            if party not in self.parties:
                self.addParty(party)

            partyWidget = self.partiesNotebook.get_nth_page(self.parties[party])
            partyWidget.loadCandidates(candidates)

        #?Should pre-existing parties not in the loaded config be removed?
        self.removeParties(list(party for party in self.parties if party not in candidatesPerParty))

    def getCandidates(self):
        output = {}
        for party, pageNum in self.parties.items():
            partyWidget = self.partiesNotebook.get_nth_page(pageNum)
            output[party] = partyWidget.getCandidates()

        return output

    def loadConfig(self, config):
        offices = config.pop("offices")
        self.loadOffices(offices)
        self.setupConfigWidget.loadConfig(config)

    def getConfig(self):
        office = self.getOffices()
        config = self.setupConfigWidget.getConfig()
        config.update({"offices": self.getOffices()})

        return config

    def loadConfigFile(self):
        errorMessage = None
        configFile = self.previousConfigFile
        try:
            configFile = self.GUIpickFile("Select config file", defaultFile=configFile)
            if configFile is None:
                return

            config = getConfig(configFile)
        except Exception as error:      #Generic catch as no special handling is done, just informing via GUI
            errorMessage = str(type(error))[8:-2] + ": " + str(error)

        if errorMessage is not None:
            errorDialog = Gtk.MessageDialog(parent=self.get_toplevel(),
                modal=True, destroy_with_parent=True,
                message_type=Gtk.MessageType.ERROR,
                buttons=Gtk.ButtonsType.OK,
                text="Loading config failed with the following error:\n" + errorMessage)

            errorDialog.set_title("Failed loading config")
            response = errorDialog.run()
            errorDialog.destroy()
            return

        self.loadConfig(config)
        self.previousConfigFile = configFile or self.previousConfigFile

    def saveConfigFile(self):
        #TODO: warn on unset
        emptyConfig = []
        noMasterDB = False
        config = self.getConfig()
        for category, subConfig in config.items():
            if category == "offices":
                if len(subConfig) == 0:
                    emptyConfig.append(category)

                continue

            for configKey, value in subConfig.items():
                if (not value) and (value is not False):
                    if configKey == "master_db":
                        #The empty MasterDB is handled differently below
                        noMasterDB = True
                        continue

                    emptyConfig.append(configKey)

        if noMasterDB:
            confirmationDialog = Gtk.MessageDialog(parent=self.get_toplevel(),
                modal=True, destroy_with_parent=True,
                message_type=Gtk.MessageType.QUESTION,
                buttons=Gtk.ButtonsType.YES_NO,
                text="No MasterDB is set, which means NO VOTER VERIFICATION will be done and all voters will be allowed.\n\nIs a MasterDB NEEDED?")

            confirmationDialog.set_title("Saving config: No MasterDB")
            response = confirmationDialog.run()
            confirmationDialog.destroy()
            if response != Gtk.ResponseType.NO:
                emptyConfig.append("master_db")

        if len(emptyConfig) > 0:
            errorDialog = Gtk.MessageDialog(parent=self.get_toplevel(),
                modal=True, destroy_with_parent=True,
                message_type=Gtk.MessageType.ERROR,
                buttons=Gtk.ButtonsType.OK,
                text="Please set a value for the following:\n" + "\n".join(emptyConfig))

            errorDialog.set_title("Failed saving config")
            response = errorDialog.run()
            errorDialog.destroy()
            return

        errorMessage = None
        configFile = self.previousConfigFile
        try:
            configFile = self.GUIpickFile("Select where to save config file", saveMode=True, defaultFile=configFile)
            if configFile is None:
                return

            file = open(configFile, "w")
            json.dump(config, file, indent=4)
            file.close()
        except Exception as error:      #Generic catch as no special handling is done, just informing via GUI
            errorMessage = str(type(error))[8:-2] + ": " + str(error)

        if errorMessage is not None:
            errorDialog = Gtk.MessageDialog(parent=self.get_toplevel(),
                modal=True, destroy_with_parent=True,
                message_type=Gtk.MessageType.ERROR,
                buttons=Gtk.ButtonsType.OK,
                text="Failed to save config:\n" + errorMessage)

            errorDialog.set_title("Failed to save config")
            response = errorDialog.run()
            errorDialog.destroy()
            return

        self.previousConfigFile = configFile or self.previousConfigFile

    def loadCandidatesFile(self):
        current_candidatesPerParty = self.getCandidates()
        #Check first if there are pre-existing candidates and parties
        candidatesPresent = False
        for party, offices in current_candidatesPerParty.items():
            if not offices:     #If there are no offices/party is empty
                continue

            for office, candidates in offices.items():
                if candidates:  #If the candidate(s) for that office is not empty
                    candidatesPresent = True
                    break

            if candidatesPresent:
                break

        if candidatesPresent:
            confirmationDialog = Gtk.MessageDialog(parent=self.get_toplevel(),
                modal=True, destroy_with_parent=True,
                message_type=Gtk.MessageType.QUESTION,
                buttons=Gtk.ButtonsType.YES_NO,
                text="Should the pre-existing parties and candidates be deleted?")

            confirmationDialog.set_title("Loading candidates: Pre-existing candidates found")
            response = confirmationDialog.run()
            confirmationDialog.destroy()
            if response == Gtk.ResponseType.YES:
                self.removeParties(tuple(self.parties.keys()))

        errorMessage = None
        candidatesFile = self.previousCandidatesFile
        try:
            candidatesFile = self.GUIpickFile("Select candidates file", defaultFile=candidatesFile)
            if candidatesFile is None:
                return

            candidatesPerParty = getCandidates(candidatesFile, self.getOffices())
        except Exception as error:      #Generic catch as no special handling is done, just informing via GUI
            errorMessage = str(type(error))[8:-2] + ": " + str(error)

        if errorMessage is not None:
            errorDialog = Gtk.MessageDialog(parent=self.get_toplevel(),
                modal=True, destroy_with_parent=True,
                message_type=Gtk.MessageType.ERROR,
                buttons=Gtk.ButtonsType.OK,
                text="Loading candidates failed with the following error:\n" + errorMessage)

            errorDialog.set_title("Failed loading candidates")
            response = errorDialog.run()
            errorDialog.destroy()
            return

        #candidatesPerParty = json.load(open("load.json"))
        self.loadCandidates(candidatesPerParty)
        #print("Getting candidates")
        #print("AFter load:", self.getCandidates())
        self.previousCandidatesFile = candidatesFile or self.previousCandidatesFile
        print("Done geting candidates")

    def saveCandidatesFile(self):
        #TODO: warn on empty parties
        errorMessage = None
        candidatesFile = self.previousCandidatesFile
        try:
            candidatesFile = self.GUIpickFile("Export candidates to file", saveMode=True, defaultFile=candidatesFile)
            if candidatesFile is None:
                return

            file = open(candidatesFile, "w")
            json.dump(self.getCandidates(), file, indent=2, ensure_ascii=False)
            file.close()
        except Exception as error:      #Generic catch as no special handling is done, just informing via GUI
            errorMessage = str(type(error))[8:-2] + ": " + str(error)

        if errorMessage is not None:
            errorDialog = Gtk.MessageDialog(parent=self.get_toplevel(),
                modal=True, destroy_with_parent=True,
                message_type=Gtk.MessageType.ERROR,
                buttons=Gtk.ButtonsType.OK,
                text="Failed exporting candidates:\n" + errorMessage)

            errorDialog.set_title("Failed exporting candidates")
            response = errorDialog.run()
            errorDialog.destroy()
            return

        self.previousCandidatesFile = candidatesFile or self.previousCandidatesFile

    def _save(self):
        file = open("save.json", "w")
        json.dump(self.getCandidates(), file, indent=2, ensure_ascii=False)
        file.close()

    def GUIsavePictures(self):
        errorMessage = None
        partyWidgets = []
        #Only save pictures with parties where pictures are actually set
        for party, pageNum in self.parties.items():
            partyWidget = self.partiesNotebook.get_nth_page(pageNum)
            if partyWidget.numHasPicture >= 1:
                partyWidgets.append(partyWidget)

    #def savePictures(self, directory, pictureFormat, filenameDict=None)
        #TODO:Conversion table/if-elifs for file format
        pictureExtension, pictureFormat = self.setupConfigWidget.getPictureFormat()
        if not pictureFormat:
            errorMessage = "No picture format chosen."

        filenameDict = {}
        for office, candidates in parseMasterList(self.getOffices(), self.getCandidates()).items():
            for candidate in candidates:
                party = candidate.party
                if candidate.isIndependent():
                    party = "Independent"

                if party not in filenameDict:
                    filenameDict[party] = {}

                filenameKey = (candidate.party, candidate.office, candidate.name)
                filenameDict[party][filenameKey] = getCandidateImageFilename(candidate, pictureExtension)

        if (len(partyWidgets) == 0) or (len(filenameDict) == 0):    #No valid candidate images to save.
            errorMessage = "No Images for valid candidates are set."

        if errorMessage is not None:
            errorDialog = Gtk.MessageDialog(parent=self.get_toplevel(),
                modal=True, destroy_with_parent=True,
                message_type=Gtk.MessageType.ERROR,
                buttons=Gtk.ButtonsType.OK,
                text=errorMessage)

            errorDialog.set_title("Saving pictures failed")
            response = errorDialog.run()
            errorDialog.destroy()
            return

        directoryDialog = Gtk.FileChooserDialog("Select where to save Pictures", self.get_toplevel(), 
            Gtk.FileChooserAction.SELECT_FOLDER, ("Cancel", Gtk.ResponseType.CANCEL,
            "Select", Gtk.ResponseType.OK) )
        picturesFolder = self.setupConfigWidget.getPicturesFolder(absolute=True)
        if picturesFolder:
            #If the images directory does not exist, automatically generate it
            if not os.access(picturesFolder, os.F_OK):
                os.mkdir(picturesFolder)
            directoryDialog.set_current_folder(picturesFolder)

        directoryDialog.unselect_all()


        response = directoryDialog.run()
        directory = directoryDialog.get_filename()
        #directoryURI = directoryDialog.get_current_folder_uri()
        directoryDialog.destroy()
        if response != Gtk.ResponseType.OK:
            return
        directory = str(PurePath(directory))

        candidatesWithError = []
        for partyWidget in partyWidgets:
            errors = partyWidget.savePictures(directory, pictureFormat, filenameDict[partyWidget.party])
            candidatesWithError.extend(errors)

        message = "Pictures saved to " + os.path.abspath(directory)
        #Display errors via GUI
        if len(candidatesWithError) >= 1:
            message += ".\nThe image for the following candidates couldn't be saved:\n" + "\n".join(map(str, candidatesWithError))

        messageDialog = Gtk.MessageDialog(parent=self.get_toplevel(),
            modal=True, destroy_with_parent=True,
            message_type=Gtk.MessageType.INFO,
            buttons=Gtk.ButtonsType.OK,
            text=message)

        messageDialog.set_title("Saving candidate Pctures")
        messageDialog.run()
        messageDialog.destroy()

    def GUIpickFile(self, title, saveMode=False, defaultFile=None):
        action = Gtk.FileChooserAction.OPEN if not saveMode else Gtk.FileChooserAction.SAVE
        fileDialog = Gtk.FileChooserDialog(title, self.get_toplevel(), 
            action, ("Cancel", Gtk.ResponseType.CANCEL,
            "Select", Gtk.ResponseType.OK) )
        fileDialog.set_do_overwrite_confirmation(True)
        fileDialog.unselect_all()
        if defaultFile:
            fileDialog.set_current_name(os.path.relpath(defaultFile))

        response = fileDialog.run()
        filename = fileDialog.get_filename()
        fileDialog.destroy()
        if response != Gtk.ResponseType.OK:
            return
        filename = str(PurePath(filename))

        return filename

    def generateElectionSummary(self, outputBasename="results"):
        errorMessage = None
        masterDBFilename = self.dbPicker.getMasterDBFilename()
        voterDBFilenames = self.dbPicker.getVoterDBFilenames()
        votesDBFilenames = self.dbPicker.getVotesDBFilenames()

        if masterDBFilename is None:
            confirmationDialog = Gtk.MessageDialog(parent=self.get_toplevel(),
                modal=True, destroy_with_parent=True,
                message_type=Gtk.MessageType.QUESTION,
                buttons=Gtk.ButtonsType.YES_NO,
                text="No MasterDB is set, which means NO VOTER VERIFICATION will be done and the voter breakdown will not accurate.\n\nContinue WITHOUT a MasterDB?")

            confirmationDialog.set_title("No MasterDB")
            response = confirmationDialog.run()
            confirmationDialog.destroy()
            if response != Gtk.ResponseType.YES:
                return

        if not MasterDB(masterDBFilename).isValidDB():
            errorMessage = "'" + masterDBFilename + "' is not a valid MasterDB."

        if len(voterDBFilenames) == 0 or len(votesDBFilenames) == 0:
            errorMessage = "Please provide all the necessary Databases."

        for dbFilename in voterDBFilenames:
            if not VoterDB(dbFilename).isValidDB():
                errorMessage = "'" + dbFilename + "' is not a valid VoterDB."
                break

        for dbFilename in votesDBFilenames:
            if not VotesDB(dbFilename).isValidDB():
                errorMessage = "'" + dbFilename + "' is not a valid VotesDB."
                break

        results = ""
        if errorMessage is None:
            try:
                #The DB merged filenames are hardcoded for now
                votersDB, voterErrors = consolidateVoterDB(voterDBFilenames, "mergedVoterDB.db", masterDBFilename)
                votesDB = consolidateVotesDB(votesDBFilenames, "mergedVotesDB.db")

                offices, results = getElectionResults("mergedVotesDB.db")
                summary = getVotesBreakdown(results, getNumElectionVoters("mergedVoterDB.db"), getNumExpectedVoters(masterDBFilename))
                saveResultsToExcel(offices, results, summary, outputBasename + ".xlsx")

                results = saveResults(offices, results, summary, voterErrors, "summary.txt")
                print(results)
            except Exception as error:
                errorMessage = "The following error was encountered when generating the election summary:\n" + str(type(error))[8:-2] + ": " + str(error)

        if errorMessage is not None:
            errorDialog = Gtk.MessageDialog(parent=self.get_toplevel(),
                modal=True, destroy_with_parent=True,
                message_type=Gtk.MessageType.ERROR,
                buttons=Gtk.ButtonsType.OK,
                text=errorMessage)

            errorDialog.set_title("Failed to generate election summary")
            response = errorDialog.run()
            errorDialog.destroy()
            return

        self.resultsTextView.get_buffer().set_text(results)
        confirmationDialog = Gtk.MessageDialog(parent=self.get_toplevel(),
            modal=True, destroy_with_parent=True,
            message_type=Gtk.MessageType.QUESTION,
            buttons=Gtk.ButtonsType.OK,
            text="The Election summary is saved on" + os.path.abspath("summary.txt")[:-11] + "\n\nsummary.txt contains the full summary, while results.xlsx contains a formatted summary of the votes.\nThe merged Databases are named merged[Voter|Votes].db")

        confirmationDialog.set_title("Election summary generated")
        response = confirmationDialog.run()
        confirmationDialog.destroy()

if __name__ == "__main__":
    win = Gtk.Window()
    widget = ElectionConfigWidget()
    widget.addParty("Sample party (delete)")
    widget.addParty("Independent")
    win.add(widget)
    win.maximize()
    win.show_all()

    win.connect("delete-event", Gtk.main_quit)
    Gtk.main()

