import argparse
import json
import os
import sys

try:
    from pathlib import PurePath
except ImportError:
    PurePath = lambda path: path

try:
    from Core.AVSDB import MasterDB
except ImportError:
    #Probably ran as a script. Add the root saytek-avs folder
    #   this is pretty kludge but will have to do for now
    sys.path.append("..")
    from Core.AVSDB import MasterDB

def sanitize_name(name):
    len_name = len(name)
    out = ""
    for i in range(len_name):
        if name[i].isprintable():
            out += name[i]

    return out

def parseVotersPerSectionFile(filename, directory="."):
    #This function will just turn the newline-delimited candidates name to a dict
    #The checking of valid section format ({grade}-{section}) is delegated to make_masterlist
    #  below as that will be useful for e.g. candidates in the same format but not from a file
    voters = [voter.strip() for voter in open(str(PurePath(directory, filename))).readlines()]
    gradeAndSection = filename[:-4] if filename.endswith(".txt") else filename

    return {gradeAndSection: voters}

def make_masterList(votersPerSection):
        #Accepts a dict in the format:
        #   {Grade-Section: [<Voter names>]}
        #then turns it into a single dict of the format:
        #{"Grade Level 1": {
        #       "Section1": [<Voter names>]
        #       "Section2": [<Voter names>]
        #   },
        #"Grade Level 2": { etc

    masterList = {}
    for gradeAndSection, voterNames in votersPerSection.items():
        gradeAndSection = tuple(map(str.strip, gradeAndSection.split("-", 1)))
        if len(gradeAndSection) < 2:
            print("Error on %s: Section keys must conform to the format {grade}-{section}" % gradeAndSection)
            continue

        grade, section = gradeAndSection
        section = section.title()
        if not grade.isnumeric():
            print("Error on {}: The grade level must be a positive integer.".format(gradeAndSection))
            continue

        if grade not in masterList.keys():
            masterList[grade] = {}

        masterList[grade][section] = []
        for name in voterNames:
            #For now, voter names are in title case and are stripped of leading and trailing whitespace
            name = str(sanitize_name(name)).title().strip()
            if not name:
                continue

            masterList[grade][section].append(name)

    return masterList

def printMasterList(masterList):
    for grade in masterList:
        print(grade)
        for section in masterList[grade]:
            print(" ", section)
            for name in masterList[grade][section]:
                print("   ", name)

            input("Press any key to proceed to the next section")

        print()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Make a masterlist JSON that can be serialized by MasterDB")
    parser.add_argument("voters_files_directory", default=".", nargs="?", help="A folder containing files of newline-delimited voter names with the filenames in {grade}-{section}")
    parser.add_argument("masterlist_file", default="./masterlist.json", nargs="?", help="The file where the output masterlist is saved(default:masterlist.json)")
    parser.add_argument("masterDB_file", default="./masterDB.db", nargs="?", help="Name of MasterDB to be generated(default:masterDB.db)")
    args = parser.parse_args()

    masterlist_folder = str(PurePath(args.voters_files_directory))
    masterListFile = str(PurePath(args.masterlist_file))
    masterDBFile = str(PurePath(args.masterDB_file))

    votersPerSection = {}
    if not os.access(masterlist_folder, os.F_OK | os.R_OK):
        print("The folder supplied", '(' + masterlist_folder + ')', "either does not exist or do not have read permission.")
        exit()

    print()
    for filename in os.listdir(masterlist_folder):
        print(filename)
        gradeAndSection = tuple(map(str.strip, filename.split("-", 1)))
        if len(gradeAndSection) < 2:
            print("The file", filename, "has an invalid filename. Filenames must conform to the format {grade}-{section}", gradeAndSection)
            continue

        votersPerSection.update(parseVotersPerSectionFile(filename, masterlist_folder))


    print("\n\n")
    print("Parsing files:", votersPerSection)
    masterList = make_masterList(votersPerSection)
    file = open(masterListFile, "w")
    json.dump(masterList, file, indent=1, ensure_ascii=False)
    file.close()
    MasterDB(masterDBFile, masterList)
    print("\n\n\nJSON:")
    printMasterList(json.load(open(masterListFile)))
