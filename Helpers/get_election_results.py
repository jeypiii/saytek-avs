#!/usr/bin/env python3
import datetime
import os
import sqlite3
import sys

try:
    import openpyxl
except ImportError:
    pass

try:
    from Core.AVSDB import MasterDB, VoterDB, VotesDB
except ImportError:
    #Probably ran as a script. Add the root saytek-avs folder
    #   this is pretty kludge but will have to do for now
    sys.path.append("..")
    from Core.AVSDB import MasterDB, VoterDB, VotesDB

def consolidateVoterDB(dbFiles, outputFile, masterDBFile=None):
    errors = {error: [] for error in ("Corrupted record", "Unsubmitted ballot", "Unlisted voter", "Extraneous registration", "Extraneous ballot submitted", "No show")}
    masterDB = MasterDB(masterDBFile)
    outputDB = VoterDB(outputFile)
    outputDB.clear()
    outputCon = outputDB.connect()

    recorded = {}
    for dbName in dbFiles:
        con = sqlite3.connect(dbName)
        cur = con.execute("SELECT StudentName, GradeLevel, Section, InTime, OutTime FROM Voters")
        entry = cur.fetchone()
        while entry:
            name, grade, section, inTime, outTime = entry
            outputCon.execute("INSERT INTO Voters VALUES (" + ", ".join("?" * len(entry)) + ")", entry)
            entry = cur.fetchone()

            #Error check 1: Record is corrupted if InTime and OutTime are NULL or any of the other information are NULL
            recordKey = (name, grade, section)
            if ( (inTime is None) and (outTime is None) ) or (None in recordKey):
                #print("Corrupted", recordKey)
                errors["Corrupted record"].append( (dbName, ) + recordKey)
                continue

            #Running record to count duplicate voters
            if recordKey not in recorded:
                recorded[recordKey] = {"Registered": 0, "Submitted ballot": 0}

            recorded[recordKey]["Registered"] += 1
            if outTime is not None:
                recorded[recordKey]["Submitted ballot"] += 1
            else:
                #Error check 2: OutTime is NULL which means the voter's ballot was not submitted
                #print("Unsubmitted", recordKey)
                errors["Unsubmitted ballot"].append( (dbName, name, grade, section, inTime) )
                continue

            slots = masterDB.checkVoter(name, grade, section)
            #Error check 3: Voter not in the MasterDB was erroneously issued a ballot
            if slots == 0:
                #print("Not in MasterDB", recordKey)
                errors["Unlisted voter"].append( (dbName, name, grade, section, inTime, outTime) )
                continue

            #Error check 4: Voter has registered or submitted too many times (reference: masterDB)
            if recorded[recordKey]["Submitted ballot"] > slots:
                #print("Extra submit", recordKey)
                errors["Extraneous ballot submitted"].append( (dbName, name, grade, section, inTime, outTime) )
            elif recorded[recordKey]["Registered"] > slots:
                #print("Extra register", recordKey)
                errors["Extraneous registration"].append( (dbName, name, grade, section, inTime) )

        con.close()

    outputCon.commit()
    outputCon.close()

    #Error check 5: Voters in masterDB but not in any of the voterDBs are classified as Abstainees
    if masterDBFile is not None:
        con = masterDB.connect()
        for gradeLevel in con.execute("SELECT name FROM sqlite_master WHERE type='table'").fetchall():
            gradeLevel = "'" + gradeLevel[0] + "'"
            cur = con.execute("SELECT StudentName, Section FROM " + gradeLevel)
            entry = cur.fetchone()
            while entry:
                name, section = entry
                #gradeLevel as a table name is 'Grade X', including the 's
                recordKey = (name, gradeLevel[7:-1], section)
                if recordKey not in recorded:
                    #print("No show", name, gradeLevel, section)
                    errors["No show"].append(recordKey)

                entry = cur.fetchone()

    return (outputDB, errors)

def consolidateVotesDB(dbFiles, outputFile):
    results = {}
    for dbName in dbFiles:
        print(dbName)
        con = sqlite3.connect(dbName)
        cur = con.cursor()
        entries = cur.execute("SELECT * FROM Candidates").fetchall()
        con.close()
        for entry in entries:
            recordKey = entry[:-1]
            votes = entry[-1]

            if recordKey not in results:
                results[recordKey] = 0

            results[recordKey] += votes

    outputDB = VotesDB(outputFile)
    outputDB.clear()
    con = outputDB.connect()
    arg_placeholders = ",".join( "?" * len(con.execute("PRAGMA table_info('Candidates')").fetchall()) )
    sql = "INSERT INTO Candidates VALUES ({})".format(arg_placeholders)
    for candidateInfo, votes in results.items():
        con.execute(sql, candidateInfo + (votes, ))

    con.commit()

    return outputDB

def getElectionResults(dbFile):
    con = sqlite3.connect(dbFile)
    cur = con.cursor()
    results = cur.execute("SELECT * FROM Candidates").fetchall()
    out = {}
    #A list of offices is used to maintain the oreder of offices
    #  because Python 3.4 dicts do not keep insertion order
    offices = []
    #The following tuple indices hold the respective fields:   0: name, 1: office, 2: party, 3: votes
    con.close()
    for row in results:
        name, office, party, votes = row
        if office not in out:
            out[office] = []
            offices.append(office)

        out[office].append( (name, party, votes) )  #Order of items is sort order

    #Sort candidates in each office alphabetically
    for office in out:
        out[office].sort()

    return (offices, out)

def getNumElectionVoters(voterDBFile):
    voters = 0
    rep_voters = {}

    con = sqlite3.connect(voterDBFile)
    cur = con.execute("SELECT StudentName, GradeLevel, InTime, OutTime FROM Voters")
    entry = cur.fetchone()
    while entry:
        name, grade, inTime, outTime = entry
        entry = cur.fetchone()
        if None in (inTime, outTime):
            continue

        gradeRepKey = str(int(grade) + 1)
        if gradeRepKey not in rep_voters:
            rep_voters[gradeRepKey] = 0

        rep_voters[gradeRepKey] += 1
        voters += 1
    con.close()

    return (voters, rep_voters)

def getNumExpectedVoters(masterDBFile):
    voters = 0
    rep_voters = {}

    masterDB = MasterDB(masterDBFile)
    con = masterDB.connect()

    for gradeLevel in con.execute("SELECT name FROM sqlite_master WHERE type='table'").fetchall():
        grade_voters = con.execute("SELECT COUNT(*) FROM " + "'" + gradeLevel[0] + "'").fetchone()[0]
        grade = str(int(gradeLevel[0][6:]) + 1)
        rep_voters[grade] = grade_voters
        voters += grade_voters

    return (voters, rep_voters)

def getVotesBreakdown(results, electionVoters, expectedVoters):
    voters, rep_voters = electionVoters
    expected_voters, expected_rep_voters = expectedVoters
    summary = {"Total": {"votes": 0, "abstains": 0, "expected": expected_voters},
               "Highest": {"votes": 0, "abstains": 0, "expected": voters}}

    for office in results:
        summary[office] = {"votes": 0, "abstains": 0}
        for candidate, party, votes in results[office]:
            summary[office]["votes"] += votes

        reference = voters
        expected = expected_voters
        if office.startswith("Grade ") and office.endswith(" Representative"):
            grade = office[6:-15]
            reference = rep_voters.get(grade, 0)
            expected = expected_rep_voters.get(grade, 0)

        summary[office]["abstains"] = reference - summary[office]["votes"]
        summary[office]["expected"] = expected

        for key in ("votes", "abstains"):
            summary["Total"][key] += summary[office][key]
            summary["Highest"][key] = max(summary[office][key], summary["Highest"][key])

    return summary

def saveResultsToExcel(offices, results , votesBreakdown, output="winners.xlsx", prependDate=True):
    wb = openpyxl.Workbook()
    sheet = wb.active

    column = 1
    font = openpyxl.styles.Font(bold=True)
    for header, col_width in (("SSG Position", 17), ("Party", 10), ("Name", 35), ("Votes", 8), ("Abstain", 10)):
        sheet.column_dimensions[chr(64+column)].width = col_width
        cell = sheet.cell(row=1, column=column)
        cell.value = header
        cell.font = font
        cell.alignment = openpyxl.styles.Alignment(horizontal="center", vertical="center", wrapText=True)

        column += 1

    row = 2

    total_votes = 0
    total_abstain = 0
    for office in offices:
        if office not in results:
            print(office, "has no recorded results. Check if it is spelled correctly (case sensitive).")
            continue

        highest_votes = max(candidate[2] for candidate in results[office])
        column = 1
        start_row = row

        #Office/Position Name
        cell = sheet.cell(row=row, column=column)
        cell.value = office
        cell.alignment = openpyxl.styles.Alignment(horizontal="center", vertical="center", wrapText=True)
        cell.font = openpyxl.styles.Font(bold=True)

        for candidate, party, votes in results[office]:
            if party.startswith("Independent #") and party[13:].isnumeric():
                party = ""

            total_votes += votes
            if votes == highest_votes:
                fill = openpyxl.styles.PatternFill("solid", fgColor="FFFF00")
                font = openpyxl.styles.Font(bold=True)
            else:
                fill = openpyxl.styles.PatternFill("solid", fgColor="FFFFFF")
                font = openpyxl.styles.Font(bold=False)

            column = 2

            cell = sheet.cell(row=row, column=column)
            cell.value = party
            cell.fill = fill
            cell.font = font;

            column += 1
            cell = sheet.cell(row=row, column=column)
            cell.value = candidate
            cell.fill = fill
            cell.font = font;

            column += 1
            cell = sheet.cell(row=row, column=column)
            cell.value = votes
            cell.fill = fill
            cell.font = font;

            row += 1

        sheet.merge_cells("A%s:A%s" % (start_row, row-1))     #Merge all cells of Office/SSG Position
        #Abstains
        abstain = votesBreakdown[office]["expected"] - votesBreakdown[office]["votes"]
        total_abstain += abstain

        column += 1
        cell = sheet.cell(row=start_row, column=column)
        cell.value = abstain;
        cell.alignment = openpyxl.styles.Alignment(horizontal="center", vertical="center")
        col_letter = chr(64+column)
        sheet.merge_cells(col_letter + str(start_row) + ":" + col_letter + str(row-1))

    cell = sheet.cell(row=row, column=column-2)
    cell.value = "Total"
    cell.alignment = openpyxl.styles.Alignment(horizontal="right")
    cell.font = openpyxl.styles.Font(bold=True)
    sheet.cell(row=row, column=column-1).value = total_votes
    sheet.cell(row=row, column=column).value = total_abstain
    sheet.cell(row=row, column=column).alignment = openpyxl.styles.Alignment(horizontal="center")
    end_cell = chr(64+column) + str(row);
    #sheet.add_table(openpyxl.worksheet.table.Table(displayName="Results", ref="A1:" + end_cell))

    #Add borders
    Side = openpyxl.styles.Side;
    border = openpyxl.styles.Border( top=Side(style='thin'),
             left=Side(style='thin'),                       right=Side(style='thin'),
                                    bottom=Side(style='thin') )

    for row in range(1, row+1):
        for col in range(1, column+1):
            sheet.cell(row=row, column=col).border = border

    sheet.cell(row=row+1, column=1).value = "*Votes and Abstain are only as accurate as the information in the Student database (MasterDB), Registered voter database (VoterDB), and the Votes database (VotesDB)"
    sheet.cell(row=row+1, column=1).alignment = openpyxl.styles.Alignment(horizontal="center", vertical="center", wrapText=True)
    sheet.merge_cells('A' + str(row+1) + ":" + chr(64+column) + str(row+2))

    wb.save((datetime.datetime.now().strftime("%Y_%m_%d ") if prependDate else "") + output)

def saveResults(offices, results, votesBreakdown, errors, filename, prependDate=True):
    out = "Election Summary " + str(datetime.datetime.now()) + "\n\n"
    out += "'Abstain' means the voter submitted their ballot but did not pick a candidate.(COMPUTED from VoterDB and VotesDB)\n"
    out += "'Absent' means the voter was not able vote through the AVS either by not going to voting precinct or not submitting the ballot.(Taken from VoterDB and MasterDB)\n"
    out += "To achieve MAXIMUM ACCURACY, make sure that the VoterDB, VotesDB, and MasterDB supplied are UPDATED and, in the case of multiple servers, PROPERLY MERGED.\n\n"

    for office in offices:
        if office not in results:
            print(office, "has no recorded results. Check if it is spelled correctly (case sensitive).")
            continue

        highest_votes = 0
        longest_identifier_len = 0
        for candidate, party, votes in results[office]:
            highest_votes = max(votes, highest_votes)
            longest_identifier_len = max(len(party) + len(candidate), longest_identifier_len)

        #Workarounds when no masterDB was used/votesBreakdown does not include an office
        if office not in votesBreakdown:
            votesBreakdown[office] = {"votes": 0, "abstains": 0, "expected": 0}
        if votesBreakdown[office]["expected"] == 0:
            votesBreakdown[office]["expected"] = votesBreakdown[office]["votes"] + votesBreakdown[office]["abstains"]

        missing_votes = votesBreakdown[office]["expected"] - votesBreakdown[office]["votes"]
        out += "  " + office + "\r\n"
        for candidate, party, votes in results[office]:
            out += "    " + "[" + party + "] " + candidate + (" " * (longest_identifier_len-len(party)-len(candidate))) + ":"
            out += (" " * (len(str(highest_votes)) - len(str(votes))+1)) + str(votes)
            out += (" ++++" if votes == highest_votes else "") +"\r\n"

        out += "     Total: " + str(votesBreakdown[office]["votes"]) + " Expected:" + str(votesBreakdown[office]["expected"]) + "\r\n";
        out += "     Abstain: " + str(votesBreakdown[office]["abstains"])
        out += " + Absent: " + str(missing_votes - votesBreakdown[office]["abstains"])
        out += " = " + str(missing_votes) + "\r\n"
        out += "\r\n"


    out += "Highest total: " + str(votesBreakdown["Highest"]["votes"]) + "\r\n"
    out += "Total Recorded voters: " + str(votesBreakdown["Highest"]["expected"]) + "\r\n"
    if votesBreakdown["Total"]["expected"] > 0:
        out += " Expected # of voters: " + str(votesBreakdown["Total"]["expected"]) + "\r\n"
        out += "Voter Turnout: " + str(round(votesBreakdown["Highest"]["expected"] / votesBreakdown["Total"]["expected"], 2) * 100) + "%\r\n"
    #out += "Total Abstains: " + str(votesBreakdown["Total"]["abstains"]) + "\r\n"
    out += "Total Votes: " + str(votesBreakdown["Total"]["votes"]) + "\r\n"

    out += "\n\n\n"
    out += "####Errors##############################################\r\n"
    errorDescriptions = (
        ("Corrupted record", "Invalid Voter Record (check for possible VoterDB tampering)"),
        ("Unlisted voter", "Voters that are not in the master list (Check if MasterDB is correct and updated)"),
        ("Extraneous ballot submitted", "Voters who submitted ballot multiple times (Might have used multiple servers)"),
        ("Unsubmitted ballot", "Voters who were not able to submit their ballot"),
        ("Extraneous registration", "Voters who registered multiple times (Might have used multiple servers)"),
        ("No show", "Voters who didn't show up for election")
    )

    for key, description in errorDescriptions:
        entries = errors[key]
        num_entries = len(entries)
        if num_entries == 0:
            continue

        out += "@@@@ (" + str(num_entries) + ") " + description + " @@@@ \r\n"
        for entry in entries:
            out += str(entry) + "\r\n"
        out += "\r\n"
    out += "########################################################\r\n"

    file = open((datetime.datetime.now().strftime("%Y_%m_%d ") if prependDate else "") + filename, "w")
    file.write(out)
    file.close()

    return out

if __name__ == "__main__":
    """
    for office, results in getElectionResults("../votesDB.db").items():
        print(office)
        for candidate, votes in results.items():
            print(candidate, ": ", votes, sep="")

        print()


        results = getElectionResults("../votesDB.db")
        saveResultsToExcel(results, "../winners.xlsx")

        for office in results:
            print(office)
            for candidate in results[office]:
                print(candidate, ": ", results[office][candidate], " votes", sep="")

            print()
    """

    votesFolder = "../dbs/votes"
    voterFolder = "../dbs/voter"
    masterDBFile = "../19-2020.db"
    votersDB, voterErrors = consolidateVoterDB( tuple(os.path.join(voterFolder, file) for file in os.listdir(voterFolder)), "consolidatedVoterDB.db", masterDBFile)
    votesDB = consolidateVotesDB( tuple(os.path.join(votesFolder, file) for file in os.listdir(votesFolder)), "consolidatedVotesDB.db")

    offices, results = getElectionResults("consolidatedVotesDB.db")
    summary = getVotesBreakdown(results, getNumElectionVoters("consolidatedVoterDB.db"), getNumExpectedVoters(masterDBFile))
    saveResultsToExcel(offices, results, summary, "results.xlsx")

    print(tuple(zip(tuple(voterErrors), tuple(map(len, voterErrors.values())))))
    print(saveResults(offices, results, summary, voterErrors, "summary_test.txt"))
