  President
    Joshua D. De Jesus: 686
    Jhon Ivan D. Solomon: 718

  Vice President
    Lawrence Albert J. Bernasor: 756
    Ann Kisha D. Karunungan: 649

  Secretary
    Anne Berlyn L. Blanqueza: 1000
    Jasmine Fiona J. Sanchez: 406

  Auditor
    Ezra Aedrielle V. Villaluz: 947
    Elana Anjela E. Canlas: 459

  PIO
    Aleah P. Borela: 982
    Raya S. Riego de Dios: 421

  Peace Officer
    Gillian Glazee L. Vizconde: 1043
    Timothy Kurth L. Lat: 362

  Treasurer
    Franz Gabriel A. Sacro: 1020
    Ysabel Sophia G. Urrea: 382

  Grade 8 Representative
    Lorise Einjel DL. Rada: 149
    Kyle Emmanuel R. De Matta: 133

  Grade 9 Representative
    Mhica Aviegail Q. Latoza: 116
    Andrea Beanice E. Pador: 153

  Grade 10 Representative
    James D. Guardiario: 135
    Elisha Aedrienne V. Villaluz: 141

  Grade 11 Representative
    Maria Katrina H. Esideño: 152
    Yuan Yancey E. Labay: 70

  Grade 12 Representative
    Estelle Marie B. Simeon: 147
    Maria Andrea S. Dela Cruz: 64

