Saytek-AVS is an Automated Voting System (AVS) for the Santa Rosa Science and Technology High school (StaRSci) Supreme Student Government (SSG) Elections. It supercedes the older AVS system, with the following major improvements:

    - More intuitive UI interface for voters
    - More robust vote counting via checks to ensure that each valid student can only vote once
    - Easier process for updating candidates for election
    - Provides tools for automating the creation of documents required for the official declaration of election results

![Voter info submission page](doc/pictures/new/voter info.png)
![Voter ballot](doc/pictures/new/ballot.png)
![Ballot summary](doc/pictures/new/summary screen.png)
![General configuration](doc/pictures/new/setup config.png)
![Updating list of candidates](doc/pictures/new/ candidates config.png)
![Voting results](doc/pictures/new/election summary.png)
![Auto-generated results document](doc/pictures/new/formatted results.png)
