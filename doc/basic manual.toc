\contentsline {section}{\numberline {1}General Procedures}{3}%
\contentsline {subsection}{\numberline {1.1}Adding permissions}{3}%
\contentsline {subsection}{\numberline {1.2}Sharing files/folders via Network Sharing/Samba}{3}%
\contentsline {subsection}{\numberline {1.3}Opening a Terminal}{5}%
\contentsline {subsection}{\numberline {1.4}Navigating to a directory}{6}%
\contentsline {subsection}{\numberline {1.5}Running a Python program}{7}%
\contentsline {section}{\numberline {2}Setup overview for automated election}{8}%
\contentsline {subsection}{\numberline {2.1}Software}{8}%
\contentsline {subsection}{\numberline {2.2}Computer setup}{8}%
\contentsline {subsection}{\numberline {2.3}Election setup}{9}%
\contentsline {section}{\numberline {3}Voting with the AVS}{12}%
\contentsline {subsection}{\numberline {3.1}Things to check before starting the AVS}{12}%
\contentsline {subsection}{\numberline {3.2}Voting proper}{12}%
\contentsline {section}{\numberline {4}Vote monitoring}{16}%
\contentsline {section}{\numberline {5}Election results and summary}{19}%
\contentsline {section}{\numberline {6}Setup From Scratch}{21}%
\contentsline {subsection}{\numberline {6.1}Python 3.4.4}{21}%
\contentsline {subsubsection}{\numberline {6.1.1}Adding Python to PATH}{21}%
\contentsline {subsection}{\numberline {6.2}Gtk 3/PyGObject for Windows}{23}%
\contentsline {subsection}{\numberline {6.3}Theming Gtk}{26}%
\contentsline {subsubsection}{\numberline {6.3.1}Adding a new Theme}{27}%
\contentsline {subsubsection}{\numberline {6.3.2}Applying a new theme}{28}%
\contentsline {subsection}{\numberline {6.4}Verifying The installation}{29}%
\contentsline {subsection}{\numberline {6.5}Sqlite Browser}{29}%
\contentsline {section}{\numberline {7}Old AVS}{30}%
\contentsline {subsection}{\numberline {7.1}Installing .NET}{30}%
\contentsline {subsection}{\numberline {7.2}Computer Setup}{30}%
\contentsline {subsection}{\numberline {7.3}Setup for the election}{30}%
\contentsline {subsection}{\numberline {7.4}Voting}{33}%
\contentsline {section}{\numberline {8}Troubleshooting}{35}%
\contentsline {section}{\numberline {9}TODO}{36}%
\contentsline {section}{\numberline {10}Modifying the source code/documentation}{38}%
\contentsline {subsection}{\numberline {10.1}Things to consider}{38}%
\contentsline {subsection}{\numberline {10.2}Version control with git}{39}%
\contentsline {subsection}{\numberline {10.3}Developer documentations}{39}%
\contentsline {subsection}{\numberline {10.4}This document}{40}%
